/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/5/23.
//

#include "RequestIntent.hh"
#include "util/Escape.hh"
#include "util/StringFields.hh"

namespace hrb {
using namespace boost::beast::http;

RequestIntent::RequestIntent(std::string_view target, boost::beast::http::verb method)
{
	// Extract the query string:
	// only truncate "target" when "?" is found; keep "target" unchanged if "?" is not found
	// use split_left() because the query string starts from the _first_ '?' according to
	// [RFC 3986](https://tools.ietf.org/html/rfc3986#page-23).
	auto query = target;
	auto[field, sep] = split_left(query, "?");

	if (query.empty() && method == verb::get)
	{
		constexpr std::string_view dot_lib{"/.lib/"};
		if (field.starts_with(dot_lib))
		{
			field.remove_prefix(dot_lib.size());
			m_action = lib;
			m_path   = url_decode(field);
			return ;
		}

		// For directories, default action is view (get HTML to show directory)
		else if (field.ends_with('/'))
			m_action = view;

		// For files, default action is to get content
		else
			m_action = content;
	}
	else if (query.empty() && method == verb::put)
		m_action = upload;

	else if (sep == '?')
	{
		auto [view_opt, content_opt, list_opt, meta_opt, auth_opt] =
			urlform.find_optional(query, "view", "content", "list", "meta", "auth");

		     if (view_opt)      m_action = view;
		else if (meta_opt)      m_action = meta;
		else if (content_opt){  m_action = content;    m_rendition = *content_opt;}
		else if (list_opt)      m_action = list;
		else if (auth_opt)      m_action = (*auth_opt == "login" ? login :
			                               (*auth_opt == "logout" ? logout : none));
		else                    m_action = none;

		target   = field;
	}

	m_path = target.empty() ? "/" : url_decode(target);
}

boost::beast::http::verb RequestIntent::method() const
{
	switch (m_action)
	{
	case view:
	case list:
	case meta:
	case content:   return verb::get;
	case upload:    return verb::put;
	case login:     return verb::post;
	case logout:
	default:        return verb::get;
	}
}

std::string RequestIntent::target() const
{
	assert(!m_path.empty());
	auto path = url_encode(m_path);
	switch (m_action)
	{
	case view:      return m_path.back() == '/' ? path : path + "?view";
	case content:   return m_rendition.empty() ? path : path.append("?content=").append(m_rendition);
	case upload:    return path;
	case meta:      return path + "?meta";
	case list:      return path + "?list";
	case login:     return path + "?auth=login";
	case logout:    return path + "?auth=logout";
	case lib:       return "/.lib/" + path;
	default:        return path;
	}
}

} // end of namespace hrb
