/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 6/3/18.
//

#pragma once

#include "crypto/SessionID.hh"

#include <chrono>
#include <optional>
#include <string>

namespace hrb {

class Cookie;

class UserID
{
public:
	UserID() = default;
	UserID(const SessionID& session, std::string user);
	UserID(const Cookie& cookie, std::string user);

	auto& session() const {return m_session;}
	auto& username() const {return m_name;}

	bool valid() const;

	bool operator==(const UserID& rhs) const;
	bool operator!=(const UserID& rhs) const;

	Cookie cookie() const;
	Cookie set_cookie(std::chrono::seconds session_length = std::chrono::seconds{3600}) const;
	void set_cookie(const Cookie& cookie);

	static std::optional<SessionID> parse_cookie(const Cookie& cookie);

private:
	std::optional<SessionID>    m_session{};
	std::string                 m_name;
};

} // end of namespace hrb
