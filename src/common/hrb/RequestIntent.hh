/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/5/23.
//

#pragma once

#include <boost/beast/http/message.hpp>

namespace hrb {

// Conversion between the intention of the user and the HTTP request.
// It is used by the client to figure out what request to be sent when the user wants to do
// something.
// It is used by the server to figure out what the user wants to do base on the HTTP request it
// receives.
class RequestIntent
{
public:
	enum class Action {
		view,           // For directories, the user wants an HTML page to view the files in
						// that directory.
						// For files, the user wants an HTML page to view the file.
		upload,			// For files, the user wants to create that file and upload its contents.
						// For directory, the user wants to create that directory.
		list,           // User wants the list of files in a directory as a JSON response.
		content,        // User wants to download a rendition of a file.
		meta,           // User wants to get meta-data of a file.
		lib,            // User wants to get a static resource to display the web page, e.g. CSS, javascript
		login,          // User wants to log in. POST only.
		logout,         // User wants to log out. GET only.
		none            // This is an error. No action will be performed.
	};
	using enum Action;

	// For clients, construct a request from an intention.
	explicit RequestIntent(Action action = view, std::string path = "/") :
		m_action{action}, m_path{std::move(path)}
	{
	}

	// For servers, deduce the user's intention from a request
	template <typename Fields>
	explicit RequestIntent(const boost::beast::http::request_header<Fields>& request) :
		RequestIntent{request.target(), request.method()}
	{
	}

	explicit RequestIntent(std::string_view target, boost::beast::http::verb method);

	// About the user's intention
	auto action() const {return m_action;}
	auto& path() const {return m_path;}

	// About the HTTP request to send
	boost::beast::http::verb method() const;
	std::string target() const;
	const std::string& rendition() const {return m_rendition;}
	auto& rendition(std::string rend) {m_rendition = std::move(rend); return *this;}

	bool operator==(const RequestIntent& rhs) const {return m_action == rhs.m_action && m_path == rhs.m_path;}
	bool operator!=(const RequestIntent& rhs) const {return !operator==(rhs);}

private:
	Action              m_action{Action::view};
	std::string         m_path{"/"};
	std::string         m_rendition;
};

} // end of namespace hrb
