/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/5/23.
//

#include "DirectoryListing.hh"
#include "crypto/Blake2.hh"
#include "util/Escape.hh"
#include "util/Image.hh"
#include "util/MMap.hh"
#include "util/Magic.hh"
#include "util/Timestamp.hh"

namespace hrb {

static_assert(sizeof(std::filesystem::perms) <= sizeof(int));

void DirectoryEntry::to_json(nlohmann::json& json, bool use_binary) const
{
	json.emplace("modified", HttpTimePoint{m_modified});
	json.emplace("mime", m_mime);
	if (m_perms != std::filesystem::perms::none)
		json.emplace("perms", static_cast<int>(m_perms));

	if (m_hash)
		json.emplace("hash", m_hash->to_nlohmann_json(use_binary));

	if (m_created)
		json.emplace("created", HttpTimePoint{*m_created});
}

void DirectoryEntry::from_json(const nlohmann::json& json)
{
	m_modified = json.at("modified").get<HttpTimePoint<>>().tp;
	json.at("mime").get_to(m_mime);

	if (json.count("hash") > 0)
		json["hash"].get_to(m_hash.emplace());

	if (json.count("created") > 0)
		m_created = json["created"].get<HttpTimePoint<>>().tp;

	m_perms = (json.count("perms") > 0) ?
		static_cast<std::filesystem::perms>(json["perms"].get<int>()) :
		std::filesystem::perms::none;
}

/// Construct a directory entry without actually reading the file. This means that
/// The blake2 hash is not available. Also, the mime type of the file is guessed base on
/// file extension. To get correct values for all metadata, call full_update().
DirectoryEntry::DirectoryEntry(const std::filesystem::directory_entry& dentry, std::error_code& ec)
{
	if (auto status = dentry.status(ec); !ec)
		from_status(status, dentry.path().extension());

	auto modified = dentry.last_write_time(ec);
	if (!ec)
		m_modified = file_time_to_systime(modified);
}

DirectoryEntry::DirectoryEntry(const std::filesystem::path& path, std::error_code& ec)
{
	if (auto status = std::filesystem::status(path, ec); !ec)
		from_status(status, path.extension());

	auto modified = last_write_time(path, ec);
	if (!ec)
		m_modified = file_time_to_systime(modified);
}

void DirectoryEntry::from_status(const std::filesystem::file_status& status, const std::filesystem::path& extension)
{
	m_perms = status.permissions();
	m_mime = (status.type() == std::filesystem::file_type::directory) ?
		Magic::directory_mime() : Magic::extension_to_mime(extension.string());
}

bool DirectoryEntry::is_directory() const
{
	return m_mime == Magic::directory_mime();
}

DirectoryEntry& DirectoryEntry::set_directory()
{
	m_mime = Magic::directory_mime();
	return *this;
}

void to_json(nlohmann::json& json, const DirectoryListing& input)
{
	// File to show
	if (!input.filename().empty())
		json.emplace("filename", input.filename());

	assert(!input.logical_path().empty());
	json.emplace("logical_path", input.logical_path());

	assert(json.is_object() || json.is_null());

	nlohmann::json file_list;
	for (auto& [filename, entry]: input)
		file_list.emplace(filename, entry);

	json.emplace("elements", file_list);
	assert(json.is_object());
}

void from_json(const nlohmann::json& json, DirectoryListing& output)
{
	output.set_filename(json.value("filename", ""));
	output.set_logical_path(json.value("logical_path", ""));

	for (auto [filename, entry] : json.at("elements").items())
		output.add(filename, entry.get<DirectoryEntry>());
}

DirectoryEntry& DirectoryListing::add(std::string filename, const DirectoryEntry& entry)
{
	auto [iterator, added] = m_entries.try_emplace(std::move(filename), entry);
	return iterator->second;
}

DirectoryListing& DirectoryListing::set_logical_path(std::string path)
{
	m_logical_path = std::move(path);
	if (m_logical_path.empty() || m_logical_path.back() != '/')
		m_logical_path.push_back('/');
	return *this;
}

// Do not use hex string for hash because msgpack is supposed to be binary.
std::vector<std::uint8_t> DirectoryEntry::to_msgpack() const
{
	nlohmann::json msgpack;
	to_json(msgpack, true);
	return nlohmann::json::to_msgpack(msgpack);
}

/// We need a full update on the cache about this file if
/// 1. There is no hash in the cache.
/// 2. Mime type is unknown.
/// 3. File is modified.
/// \param  path    Physical path to the file.
/// \param  ec      File system error encountered.
/// \return true if the entry is changed, false if not.
bool DirectoryEntry::full_update(const std::filesystem::path& path, std::error_code& ec)
{
	// Throw away the hash if the file is modified.
	auto real_mod_time = file_time_to_systime(last_write_time(path, ec));
	if (ec)
		return false;

	auto status = std::filesystem::status(path, ec);
	if (!ec && status.type() == std::filesystem::file_type::directory)
	{
		auto old_mod_time = std::exchange(m_modified, real_mod_time);
		auto old_mime     = std::exchange(m_mime, Magic::directory_mime());
		auto old_created  = std::exchange(m_created, {});
		auto old_perm     = std::exchange(m_perms, status.permissions());
		assert(is_directory());

		return old_mime != m_mime || old_mod_time != m_modified ||
			old_created != m_created || old_perm != m_perms;
	}

	if (!m_hash || m_mime.empty() || real_mod_time != m_modified)
	{
		auto mmap = MMap::open(path, ec);
		if (ec)
			return false;

		// Re-calculate the has if the file is modified.
		if (!m_hash || real_mod_time != m_modified)
		{
			Blake2 hash;
			hash.update(mmap.data(), mmap.size());
			auto new_hash = hash.finalize();

			// Refresh metadata if needed.
			// No need to refresh metadata if file content is the same.
			if (!m_hash || new_hash != *m_hash)
			{
				m_mime.clear();
				m_created = {};
			}

			m_hash = new_hash;
		}

		if (m_mime.empty())
			m_mime = Magic::instance().mime(mmap.data(), mmap.size());

		if (!m_created)
		{
			Image image{mmap.buffer(), m_mime};
			m_created = image.date_time();
		}

		m_perms    = status.permissions();
		m_modified = real_mod_time;
		return true;
	}
	auto old_mod_time = std::exchange(m_modified, real_mod_time);
	return old_mod_time != m_modified;
}

} // end of namespace hrb
