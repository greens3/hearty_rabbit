/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 6/3/18.
//

#include "UserID.hh"
#include "util/Cookie.hh"
#include "util/Escape.hh"

#include <cassert>
#include <iostream>

namespace hrb {

UserID::UserID(const SessionID& session, std::string user) :
	m_session{session}, m_name{std::move(user)}
{
	assert(!m_name.empty());
}

UserID::UserID(const Cookie& cookie, std::string user) :
	m_session{parse_cookie(cookie)},
	m_name{m_session.has_value() ? std::move(user) : std::string{}}
{
}

bool UserID::valid() const
{
	// if cookie is valid, user must not be empty
	// if cookie is invalid, user must be empty
	assert(m_session.has_value() == !m_name.empty() );
	return m_session.has_value();
}

bool UserID::operator==(const UserID& rhs) const
{
	return m_name == rhs.m_name && m_session == rhs.m_session;
}

bool UserID::operator!=(const UserID& rhs) const
{
	return !operator==(rhs);
}

Cookie UserID::cookie() const
{
	Cookie cookie;
	cookie.add("id", valid() ? m_session->to_hex() : "");
	return cookie;
}

Cookie UserID::set_cookie(std::chrono::seconds session_length) const
{
	auto cookie{this->cookie()};
	cookie.add("Path", "/");
	cookie.add("SameSite", "Strict");
	if (valid())
	{
		cookie.add("Secure");
		cookie.add("HttpOnly");
		cookie.add("Max-Age", std::to_string(session_length.count()));
	}
	else
	{
		cookie.add("Expires", "Thu, Jan 01 1970 00:00:00 UTC");
	}
	return cookie;
}

std::optional<SessionID> UserID::parse_cookie(const Cookie& cookie)
{
	return hex_to_array<SessionID{}.size()>(cookie.field("id"));
}

void UserID::set_cookie(const Cookie& cookie)
{
	m_session = parse_cookie(cookie);
}

} // end of namespace hrb
