/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/5/23.
//

#pragma once

#include "util/ByteArray.hh"

#include <nlohmann/json.hpp>

#include <filesystem>
#include <string>
#include <unordered_map>
#include <optional>

namespace hrb {

class DirectoryEntry
{
public:
	using time_point = std::chrono::system_clock::time_point;
	using FileHash = ByteArray<32>;

public:
	DirectoryEntry() = default;
	explicit DirectoryEntry(const std::filesystem::path& path, std::error_code& ec);
	DirectoryEntry(const std::filesystem::directory_entry& dentry, std::error_code& ec);
	DirectoryEntry(
		std::string mime, time_point mod,
		std::filesystem::perms perms, const FileHash& hash,
		time_point created = {}
	) :
		m_mime{std::move(mime)}, m_modified{mod}, m_perms{perms}, m_hash{hash}, m_created{created}
	{
	}

	bool full_update(const std::filesystem::path& path, std::error_code& ec);

	auto& mime() const {return m_mime;}
	auto& set_mime(std::string mime) {m_mime = std::move(mime); return *this;}
	auto modified() const {return m_modified;}
	auto& set_modified(auto modified) {m_modified = modified; return *this;}
	auto created() const {return m_created;}
	auto& set_created(auto created) {m_created = created; return *this;}
	auto& hash() const {return m_hash;}
	auto& set_hash(auto hash) {m_hash = hash; return *this;}
	auto perms() const {return m_perms;}
	auto& set_perms(std::filesystem::perms perms) {m_perms = perms; return *this;}

	std::vector<std::uint8_t> to_msgpack() const;

	template <typename InputType>
	static DirectoryEntry from_msgpack(InputType&& input)
	{
		return nlohmann::json::from_msgpack(std::forward<InputType>(input)).template get<DirectoryEntry>();
	}

	auto tuple() const {return std::make_tuple(m_mime, m_modified, m_hash, m_created);}
	bool is_directory() const;
	DirectoryEntry& set_directory();

	bool operator==(const DirectoryEntry& rhs) const {return tuple() == rhs.tuple();}
	bool operator!=(const DirectoryEntry& rhs) const {return !operator==(rhs);}

	void to_json(nlohmann::json& json, bool use_binary) const;
	void from_json(const nlohmann::json& json);

private:
	void from_status(const std::filesystem::file_status& status, const std::filesystem::path& extension);

private:
	std::string m_mime;
	time_point  m_modified;
	std::filesystem::perms      m_perms{std::filesystem::perms::none};
	std::optional<time_point>   m_created;
	std::optional<FileHash>     m_hash;
};

inline void to_json(nlohmann::json& json, const DirectoryEntry& input)
{
	input.to_json(json, false);
}
inline void from_json(const nlohmann::json& json, DirectoryEntry& output)
{
	output.from_json(json);
}

// Correspond to the "window.dir" JSON in javascript. It is also the same
// JSON returned by list action on directories.
class DirectoryListing
{
public:
	using FileHash = DirectoryEntry::FileHash;
	using time_point = DirectoryEntry::time_point;

	using Entries = std::unordered_map<std::string, DirectoryEntry>;
	using const_iterator = Entries::const_iterator;
	using iterator       = Entries::iterator;

public:
	DirectoryListing() = default;

	auto& set_filename(std::string filename = {}){m_filename = std::move(filename); return *this;}
	auto  set_logical_path(std::string path) -> DirectoryListing&;
	auto& set_entries(Entries entries){m_entries = std::move(entries); return *this;}

	auto perms() const {return m_perms;}
	void set_perms(std::filesystem::perms perms) {m_perms = perms;}

	template <typename ... Args>
	DirectoryEntry& add(std::string filename, Args && ... args)
	{
		auto [iterator, added] = m_entries.try_emplace(
			std::move(filename),
			std::forward<Args>(args)...
		);
		return iterator->second;
	}

	DirectoryEntry& add(std::string filename, const DirectoryEntry& entry);
	auto& at(const std::string& filename) {return m_entries.at(filename);}
	auto contains(const std::string& filename) {return m_entries.contains(filename);}

	auto& filename() const {return m_filename;}
	auto& logical_path() const {return m_logical_path;}

	friend void to_json(nlohmann::json& json, const DirectoryListing& input);
	friend void from_json(const nlohmann::json& json, DirectoryListing& output);

	auto to_json() const {return nlohmann::json(*this);}

	auto begin() const {return m_entries.begin();}
	auto begin()       {return m_entries.begin();}
	auto end() const {return m_entries.end();}
	auto end()       {return m_entries.end();}
	auto find(const std::string& filename) const {return m_entries.find(filename);}
	auto find(const std::string& filename)       {return m_entries.find(filename);}
	auto size() const {return m_entries.size();}
	auto erase(iterator file){return m_entries.erase(file);}
	auto empty() const {return m_entries.empty();}

private:
	// Logical path of the directory of this listing. This is the "parent" directory of
	// that contain the file "m_filename"
	std::string m_logical_path{"/"};

	// Correspond to dir.filename. If this is not empty, the image/file will be opened in
	// the light box. This will be set by the server if the request target is /path/filename?view
	std::string m_filename;

	std::filesystem::perms m_perms{std::filesystem::perms::none};

	// Other entries inside the directory of m_logical_path.
	Entries m_entries;
};

} // end of namespace hrb
