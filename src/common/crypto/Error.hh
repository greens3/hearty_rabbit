/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/12/23.
//

#pragma once

#include <exception>
#include <openssl/err.h>

namespace hrb {

class OpenSSLException : public std::exception
{
public:
	OpenSSLException() = default;
	explicit OpenSSLException(unsigned long err) : m_last_error{err} {}

	[[nodiscard]] const char* what() const noexcept override;

private:
	unsigned long m_last_error{::ERR_get_error()};
};

} // end of namespace
