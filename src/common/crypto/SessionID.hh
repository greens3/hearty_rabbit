/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 9/3/23.
//

#pragma once

#include "util/ByteArray.hh"

namespace hrb {

using SessionID = ByteArray<16>;

}
