/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/28/18.
//

#pragma once

#include "util/ByteArray.hh"
#include <openssl/evp.h>

#include <memory>

namespace hrb {

class Blake2
{
public:
	Blake2();
	Blake2(Blake2&&) = default;
	Blake2(const Blake2& src) : Blake2()
	{
		::EVP_MD_CTX_copy_ex(m_ctx.get(), src.m_ctx.get());
	}
	~Blake2() = default;

	Blake2& operator=(Blake2&&) = default;
	Blake2& operator=(const Blake2& src)
	{
		auto copy{src};
		std::swap(m_ctx, copy.m_ctx);
		return *this;
	}

	static constexpr std::size_t size = 32;

	void update(const void *data, std::size_t len);

	using Hash = ByteArray<size>;
	Hash finalize();
	std::size_t finalize(std::byte *out, std::size_t len);

private:
	struct Deleter {void operator()(::EVP_MD_CTX*);};
	std::unique_ptr<::EVP_MD_CTX, Deleter> m_ctx{::EVP_MD_CTX_new()};
};

static_assert(sizeof(Blake2) == sizeof(void*));

} // end of namespace hrb::evp
