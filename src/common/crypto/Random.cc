/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/27/18.
//

#include "Random.hh"
#include "Error.hh"

#include <cassert>
#include <limits>

#include <openssl/rand.h>

namespace hrb {
void secure_random(void *buf, std::size_t size)
{
	assert(size <= std::numeric_limits<int>::max());
	if (::RAND_priv_bytes(reinterpret_cast<unsigned char*>(buf), static_cast<int>(size)) != 1)
		throw hrb::OpenSSLException();
}

void insecure_random(void *buf, std::size_t size)
{
	assert(size <= std::numeric_limits<int>::max());
	if (::RAND_bytes(reinterpret_cast<unsigned char*>(buf), static_cast<int>(size)) != 1)
		throw hrb::OpenSSLException();
}

} // end of namespace hrb
