/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/28/18.
//

#include "Blake2.hh"
#include "Error.hh"

#include <cstring>
#include <cassert>

namespace hrb {

Blake2::Blake2()
{
	assert(::EVP_MD_get_size(::EVP_blake2s256()) == size);

	if (!::EVP_DigestInit_ex2(m_ctx.get(), ::EVP_blake2s256(), nullptr))
		throw hrb::OpenSSLException();
}

void Blake2::update(const void *data, std::size_t len)
{
	if (!::EVP_DigestUpdate(m_ctx.get(), data, len))
		throw hrb::OpenSSLException();
}

std::size_t Blake2::finalize(std::byte* out, std::size_t len)
{
	// Note that EVP_DigestFinal_ex() does not check the size you pass in.
	// The 3rd argument is for output only.
	unsigned char out_buf[EVP_MAX_MD_SIZE] = {};
	unsigned out_size{};
	if (!::EVP_DigestFinal_ex(m_ctx.get(), out_buf, &out_size))
		throw hrb::OpenSSLException();

	assert(out_size == size);
	auto copied = std::min<std::size_t>(len, out_size);
	std::memcpy(out, out_buf, copied);
	return copied;
}

Blake2::Hash Blake2::finalize()
{
	assert(::EVP_MD_get_size(::EVP_blake2s256()) == size);
	Hash result{};
	unsigned out_size{};
	if (!::EVP_DigestFinal_ex(m_ctx.get(), result.ui8data(), &out_size))
		throw hrb::OpenSSLException();
	assert(out_size == size);
	return result;
}

void Blake2::Deleter::operator()(::EVP_MD_CTX *ctx)
{
	if (ctx)
		::EVP_MD_CTX_free(ctx);
}

namespace {
const std::shared_ptr<void> _{
	// Load error strings before main()
	[]
	{
		::ERR_load_crypto_strings();
		::SSL_load_error_strings();
		return nullptr;
	}(),
	// Free error strings after main()
	[](void*)
	{
		ERR_free_strings();
	}
};
} // end of namespace

[[nodiscard]] const char* OpenSSLException::what() const noexcept
{
	return ::ERR_error_string(m_last_error, nullptr);
}

} // end of namespace hrb
