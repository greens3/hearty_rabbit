/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

#pragma once

#include "RepeatingTuple.hh"

#include <string>
#include <string_view>
#include <tuple>

namespace hrb {

std::string url_encode(std::string_view in);
std::string url_decode(std::string_view in);

std::tuple<std::string_view, char> split_left(std::string_view& in, std::string_view value);
std::tuple<std::string_view, char> split_right(std::string_view& in, std::string_view value);
std::string_view split_front_substring(std::string_view& in, std::string_view substring);

template <std::size_t index, typename ResultTuple>
void parse_token(std::string_view& remain, std::string_view value, ResultTuple& tuple)
{
	static_assert(index < std::tuple_size<ResultTuple>::value);
	static_assert(std::is_same<std::tuple_element_t<index, ResultTuple>, std::string_view>::value);
	std::get<index>(tuple) = std::get<0>(split_left(remain, value));

	if constexpr (index + 1 < std::tuple_size<ResultTuple>::value)
		parse_token<index+1>(remain, value, tuple);
}

template <std::size_t count>
auto tokenize(std::string_view remain, std::string_view value)
{
	static_assert(count > 0);

	typename RepeatingTuple<std::string_view, count>::type result;
	parse_token<0>(remain, value, result);
	return result;
}

} // end of namespace hrb
