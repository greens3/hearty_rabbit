/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 1/Mar/18.
//

#include "Image.hh"

// libexif to read EXIF2 tags
#include <libexif/exif-data.h>
#include <avif/avif.h>

#include <memory>
#include <optional>
#include <chrono>
#include <cassert>

namespace hrb {
using namespace exif;

class EXIF
{
public:
	explicit EXIF(boost::asio::const_buffer jpeg) :
		m_data{::exif_data_new_from_data(static_cast<const unsigned char*>(jpeg.data()), jpeg.size())}
	{
	}

	EXIF(EXIF&&) = default;
	EXIF(const EXIF&) = delete;
	~EXIF() = default;

	EXIF& operator=(EXIF&&) = default;
	EXIF& operator=(const EXIF&) = delete;

	[[nodiscard]] explicit operator bool() const noexcept {return m_data != nullptr;}

	using time_point = std::chrono::system_clock::time_point;
	[[nodiscard]] std::optional<time_point> date_time() const
	{
		if (!m_data)
			return std::nullopt;

		// DateTimeOriginal should be in EXIF IFD
		if (auto entry = ::exif_content_get_entry(m_data->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_ORIGINAL); entry)
			return read_date_tag(entry);

		if (auto entry = ::exif_content_get_entry(m_data->ifd[EXIF_IFD_0], EXIF_TAG_DATE_TIME); entry)
			return read_date_tag(entry);

		if (auto entry = ::exif_content_get_entry(m_data->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_DIGITIZED); entry)
			return read_date_tag(entry);

		return std::nullopt;

	}

	template <typename T>
	bool get_value_from_entry(const ::ExifEntry& entry, T& value) const
	{
		if (entry.size < ::exif_format_get_size(entry.format))
			return false;

		auto endian = ::exif_data_get_byte_order(m_data.get());
		switch (entry.format)
		{
		case EXIF_FORMAT_UNDEFINED:
		case EXIF_FORMAT_ASCII:
			if constexpr (std::is_convertible_v<std::string, T>)
			{
				value.assign(reinterpret_cast<const char*>(entry.data), entry.size);
				return true;
			}
			break;
		case EXIF_FORMAT_BYTE:
			if constexpr (std::is_convertible_v<std::byte, T>)
			{
				value = static_cast<std::byte>(::exif_get_short(entry.data, endian));
				return true;
			}
			break;
		case EXIF_FORMAT_SSHORT:
			if constexpr (std::is_convertible_v<short, T>)
			{
				value = ::exif_get_sshort(entry.data, endian);
				return true;
			}
			break;
		case EXIF_FORMAT_SHORT:
			if constexpr (std::is_convertible_v<unsigned short, T>)
			{
				value = ::exif_get_short(entry.data, endian);
				return true;
			}
			break;
		case EXIF_FORMAT_LONG:
			if constexpr (std::is_convertible_v<std::uint32_t, T>)
			{
				value = ::exif_get_long(entry.data, endian);
				return true;
			}
			break;
		case EXIF_FORMAT_SLONG:
			if constexpr (std::is_convertible_v<std::int32_t, T>)
			{
				value = ::exif_get_slong(entry.data, endian);
				return true;
			}
			break;
		case EXIF_FORMAT_RATIONAL:
			if constexpr (std::is_convertible_v<urational, T>)
			{
				auto rat = ::exif_get_rational(entry.data, endian);
				value = urational{rat.numerator, rat.denominator};
				return true;
			}
			break;
		case EXIF_FORMAT_SRATIONAL:
			if constexpr (std::is_convertible_v<rational, T>)
			{
				auto rat = ::exif_get_srational(entry.data, endian);
				value = rational{rat.numerator, rat.denominator};
				return true;
			}
			break;
		default:
			break;
		}
		return false;
	}

	template <typename T>
	bool get_value(ExifIfd ifd, ExifTag tag, T& value)
	{
		assert(ifd < EXIF_IFD_COUNT);
		if (auto entry = ::exif_content_get_entry(m_data->ifd[ifd], tag); entry)
			return get_value_from_entry<T>(*entry, value);
		return false;
	}

private:
	std::optional<time_point> read_date_tag(::ExifEntry* entry) const
	{
		assert(entry);

		std::string str;
		if (get_value_from_entry(*entry, str))
		{
			struct std::tm result{};
			if (auto p = strptime(str.c_str(), "%Y:%m:%d %H:%M:%S", &result); p)
				return std::chrono::system_clock::from_time_t(::timegm(&result));
		}
		return std::nullopt;
	}


private:
	// Use unique_ptr to ensure the ExifData will be freed.
	struct Unref
	{
		void operator()(::ExifData* data) const
		{
			if (data)
				::exif_data_unref(data);
		}
	};
	std::unique_ptr<::ExifData, Unref>  m_data;
};

Image::Image(boost::asio::const_buffer content, std::string_view mime)
{
	// For JPEG files, the whole file is EXIF format
	if (mime == "image/jpeg")
		from_exif(content);

	// For AVIF, use libavif to locate EXIF inside the file
	else if (mime == "image/avif")
	{
	    std::shared_ptr<::avifDecoder> decoder{::avifDecoderCreate(), [](::avifDecoder* d){::avifDecoderDestroy(d);}};
		::avifDecoderSetIOMemory(decoder.get(), reinterpret_cast<const uint8_t*>(content.data()), content.size());

		// Decode EXIF if it is found inside the file
		if (auto r = ::avifDecoderParse(decoder.get());
			r == AVIF_RESULT_OK && decoder->imageCount > 0 && decoder->image)
			from_exif({decoder->image->exif.data, decoder->image->exif.size});
	}

	// TODO: call png_get_eXIf_1() to get EXIF from PNG files
	// http://www.libpng.org/pub/png/libpng-manual.txt
	else if (mime == "image/png")
	{
	}
}

void Image::from_exif(boost::asio::const_buffer exif_buf)
{
	EXIF exif{exif_buf};
	exif.get_value(EXIF_IFD_EXIF, EXIF_TAG_EXIF_VERSION, m_exif_version);

	m_date_time = exif.date_time().value_or(time_point{});
}

} // end of namespace
