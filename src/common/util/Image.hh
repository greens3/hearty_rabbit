/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 10/30/23.
//

#pragma once

#include <boost/asio/buffer.hpp>
#include <boost/rational.hpp>
#include <chrono>

namespace hrb {
namespace exif {

using rational  = boost::rational<std::int32_t>;
using urational = boost::rational<std::uint32_t>;
}

/// Extract meta-data from images.
/// This class contains meta data of files with "image/*" mime type. It obtains these data
/// from EXIF or the file system itself (e.g. date time). This class will try to deduce the
/// data fields if it is missing. For example, if "DateTimeOriginal" is missing from the EXIF
/// tag of an image, it will be deduced from the file creation time from file system.
class Image
{
public:
	using time_point = std::chrono::system_clock::time_point;

public:
	explicit Image(boost::asio::const_buffer content, std::string_view mime);

	auto date_time() const {return m_date_time;}
	auto& exif_version() const {return m_exif_version;}

private:
	void from_exif(boost::asio::const_buffer exif_buf);

private:
	// Tags from IFD0
	time_point  m_date_time;        ///< From DateTime or EXIF DateTimeOriginal

	// Tags from EXIF IFD
	std::string     m_exif_version;
};

} // end of namespace
