/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 9/2/23.
//

#pragma once

#include <boost/algorithm/hex.hpp>
#include <boost/asio/buffer.hpp>

#include <nlohmann/json.hpp>

#include <fmt/format.h>

#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <string_view>
#include <optional>

static_assert(sizeof(std::byte) == sizeof(std::uint8_t));

namespace hrb {

/// \brief  An std::array instantiation for std::byte with some helper functions.
template <std::size_t N>
class ByteArray : private std::array<std::byte, N>
{
public:
	using std::array<std::byte, N>::array;
	using std::array<std::byte, N>::size;
	using std::array<std::byte, N>::begin;
	using std::array<std::byte, N>::cbegin;
	using std::array<std::byte, N>::end;
	using std::array<std::byte, N>::cend;
	using std::array<std::byte, N>::data;
	using std::array<std::byte, N>::empty;
	using std::array<std::byte, N>::operator=;
	using std::array<std::byte, N>::operator[];

	// Convenient constructors
	constexpr explicit ByteArray(const std::uint8_t (&ucs)[N])
	{
		std::memcpy(data(), ucs, N*sizeof(std::byte));
	}
	explicit ByteArray(const std::array<std::byte, N>& arr) : std::array<std::byte,N>{arr} {}

	auto ui8begin()       {return reinterpret_cast<std::uint8_t*>(data());}
	auto ui8begin() const {return reinterpret_cast<const std::uint8_t*>(data());}
	auto ui8end()       {return ui8begin() + N;}
	auto ui8end() const {return ui8begin() + N;}
	auto ui8data()       {return ui8begin();}
	auto ui8data() const {return ui8begin();}

	auto to_vec() const
	{
		return std::vector<std::uint8_t>(ui8begin(), ui8end());
	}

	auto to_hex() const
	{
		std::string result(N*2, '\0');
		boost::algorithm::hex_lower(ui8begin(), ui8end(), result.begin());
		return result;
	}

	auto to_quoted_hex(char quote = '\"') const
	{
		std::string result(N*2 + 2, quote);
		boost::algorithm::hex_lower(ui8begin(), ui8end(), result.begin()+1);
		return result;
	}

	bool from_hex(std::string_view hex)
	{
		try
		{
			if (hex.size() == size()*2)
			{
				boost::algorithm::unhex(hex.begin(), hex.end(), ui8begin());
				return true;
			}
		}
		catch (boost::algorithm::hex_decode_error&)
		{
		}
		return false;
	}

	nlohmann::json to_binary_json() const
	{
		return nlohmann::json::binary_t(to_vec());
	}
	auto to_nlohmann_json(bool use_binary) const
	{
		return use_binary ? to_binary_json() : nlohmann::json(to_hex());
	}
	void from_nlohmann_json(const nlohmann::json& json)
	{
		if (auto s = json.get_ptr<const std::string*>(); s)
			from_hex(*s);
		else if (auto v = json.get_ptr<const nlohmann::json::binary_t*>(); v)
		{
			if (v->size() == size())
				std::memcpy(data(), v->data(), size());
			else
				throw std::out_of_range(fmt::format("wrong size ({}) in binary JSON, expect {}", v->size(), size()));
		}
		else
			throw std::invalid_argument(fmt::format("incorrect JSON type: {}, expect binary", json.type_name()));
	}

	auto& base() const
	{
		return static_cast<const std::array<std::byte, N>&>(*this);
	}
	auto& base()
	{
		return static_cast<std::array<std::byte, N>&>(*this);
	}

	auto buffer() const
	{
		return boost::asio::const_buffer(data(), size());
	}
	auto buffer()
	{
		return boost::asio::mutable_buffer(data(), size());
	}
};

template <std::size_t N>
bool operator==(const ByteArray<N>& lhs, const ByteArray<N>& rhs)
{
	return lhs.base() == rhs.base();
}

template <std::size_t N>
bool operator!=(const ByteArray<N>& lhs, const ByteArray<N>& rhs)
{
	return lhs.base() != rhs.base();
}

template <std::size_t N>
std::optional<ByteArray<N>> hex_to_array(std::string_view hex)
{
	ByteArray<N> result;
	return result.from_hex(hex) ? std::optional{result} : std::nullopt;
}

template <std::size_t N>
void to_json(nlohmann::json& json, const ByteArray<N>& arr)
{
	json = nlohmann::json(arr.to_hex());
}

template <std::size_t N>
void from_json(const nlohmann::json& json, ByteArray<N>& output)
{
	output.from_nlohmann_json(json);
}

template<std::size_t N>
std::ostream& operator<<(std::ostream& os, const ByteArray<N>& arr)
{
	fmt::format_to(std::ostreambuf_iterator<char>(os.rdbuf()), "{}", arr);
	return os;
}

} // end of namespace hrb

// fmt support
template <std::size_t N>
struct fmt::formatter<hrb::ByteArray<N>>
{
	constexpr auto parse(format_parse_context& ctx) {return ctx.begin();}
	fmt::format_context::iterator format(const hrb::ByteArray<N>& arr, fmt::format_context& ctx) const
	{
		return boost::algorithm::hex_lower(arr.ui8begin(), arr.ui8end(), ctx.out());
	}
};
