/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 5/27/18.
//

#pragma once

#include <nlohmann/json.hpp>

#include <chrono>
#include <filesystem>
#include <fmt/format.h>

namespace hrb {

template <typename Clock=std::chrono::system_clock, typename Duration=std::chrono::system_clock::time_point::duration>
struct HttpTimePoint
{
	std::chrono::time_point<Clock, Duration> tp;
};

template <typename Clock, typename Duration>
HttpTimePoint(std::chrono::time_point<Clock, Duration>) -> HttpTimePoint<Clock, Duration>;

} // end of namespace

// fmt support
template <typename Clock, typename Duration>
struct fmt::formatter<hrb::HttpTimePoint<Clock, Duration>>
{
	constexpr auto parse(format_parse_context& ctx) {return ctx.begin();}
	fmt::format_context::iterator format(const hrb::HttpTimePoint<Clock, Duration>& tp, fmt::format_context& ctx) const
	{
		return fmt::format_to(ctx.out(), "{:%a, %d %b %Y %H:%M:%S} GMT",
			floor<std::chrono::milliseconds>(tp.tp));
	}
};

namespace hrb {
template <typename Clock, typename Duration>
void to_json(nlohmann::json& json, const HttpTimePoint<Clock, Duration>& input)
{
	using namespace std::chrono;
	json = time_point_cast<nanoseconds>(input.tp).time_since_epoch().count();
}

template <typename Clock, typename Duration>
void from_json(const nlohmann::json& json, HttpTimePoint<Clock, Duration>& output)
{
	using namespace std::chrono;
	output.tp = time_point_cast<Duration>(
		time_point<Clock, nanoseconds>{nanoseconds{json.get<system_clock::duration::rep>()}}
	);
}

// In clang/libcxx the default duration of file_clock is different from that of system_clock.
// Therefore, we need a time_point_cast.
inline std::chrono::system_clock::time_point file_time_to_systime(std::filesystem::file_time_type ft)
{
	using namespace std::chrono;
	return time_point_cast<system_clock::time_point::duration>(file_clock::to_sys(ft));
}

} // end of namespace hrb
