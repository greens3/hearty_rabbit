/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/20/18.
//

#pragma once

#include <boost/beast/http/status.hpp>

#include <system_error>

namespace hrb {

enum class Error
{
	ok,
	server_error,
	unknown_error
};

const std::error_category& hrb_error_category();
std::error_code make_error_code(Error err);

// Translation between HTTP status and std::error_code
boost::beast::http::status error_to_status(
	std::error_code ec,
	boost::beast::http::status success = boost::beast::http::status::ok
);
std::error_code status_to_error(boost::beast::http::status status);
std::error_code status_to_error(std::error_code ec, boost::beast::http::status status);

} // end of namespace hrb

namespace std
{
	template <> struct is_error_code_enum<hrb::Error> : true_type {};
}
