/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/20/18.
//

#include "Error.hh"

namespace hrb {

const std::error_category& hrb_error_category()
{
	struct Cat : std::error_category
	{
		Cat() = default;
		[[nodiscard]] const char *name() const noexcept override { return "hrb"; }

		[[nodiscard]] std::string message(int ev) const override
		{
			using enum Error;
			switch (static_cast<Error>(ev))
			{
				case ok:             return "no error";
				case server_error:   return "server error";
				default:             return "unknown error " + std::to_string(ev);
			}
		}
	};
	static const Cat cat;
	return cat;
}

std::error_code make_error_code(Error err)
{
	return {static_cast<int>(err), hrb_error_category()};
}

boost::beast::http::status error_to_status(std::error_code ec, boost::beast::http::status success)
{
	using enum boost::beast::http::status;
	assert(to_status_class(success) == boost::beast::http::status_class::successful);

	if (!ec)
		return success;

	if (ec.category() == std::generic_category() || ec.category() == std::system_category())
	{
		using enum std::errc;
		switch (static_cast<std::errc>(ec.value()))
		{
		case invalid_argument:              return bad_request;
		case no_such_file_or_directory:     return not_found;
		case permission_denied:             return forbidden;
		default:                            return internal_server_error;
		}
	}
	else if (ec.category() == hrb_error_category())
	{
		using enum Error;
		switch (static_cast<Error>(ec.value()))
		{
		case server_error:
		case unknown_error:
		default:                            return internal_server_error;
		}
	}
	else
		return internal_server_error;
}

std::error_code status_to_error(boost::beast::http::status status)
{
	using enum boost::beast::http::status;
	using enum std::errc;
	switch (status)
	{
	case ok:            return {};
	case not_found:     return make_error_code(no_such_file_or_directory);
	case forbidden:     return make_error_code(permission_denied);
	case bad_request:   return make_error_code(invalid_argument);
	case internal_server_error:
	default:            return make_error_code(Error::server_error);
	}
}

std::error_code status_to_error(std::error_code ec, boost::beast::http::status status)
{
	if (!ec)
		ec = status_to_error(status);
	return ec;
}

} // end of namespace
