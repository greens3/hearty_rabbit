/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/20/18.
//

#include "Magic.hh"

#include <fmt/core.h>

#include <boost/algorithm/string/case_conv.hpp>

#include <stdexcept>
#include <unordered_map>

namespace hrb {

Magic::Magic() : m_cookie{::magic_open(MAGIC_MIME_TYPE)}
{
	if (!m_cookie || ::magic_load(m_cookie, nullptr) == -1)
		throw std::runtime_error{::magic_error(m_cookie)};
}

Magic::~Magic()
{
	::magic_close(m_cookie);
}

std::string_view Magic::mime(boost::asio::const_buffer buf) const
{
	return mime(buf.data(), buf.size());
}

std::string_view Magic::mime(const void *buffer, std::size_t size) const
{
	auto r = ::magic_buffer(m_cookie, buffer, size);
	return r ? std::string_view{r} : std::string_view{};
}

std::string_view Magic::mime(boost::beast::file_posix::native_handle_type fd) const
{
	auto r = ::magic_descriptor(m_cookie, fd);
	return r ? std::string_view{r} : std::string_view{};
}

std::string_view Magic::mime(const std::filesystem::path& path) const
{
	auto r  = ::magic_file(m_cookie, path.string().c_str());
	return r ? std::string_view{r} : std::string_view{};
}

const Magic& Magic::instance()
{
	thread_local Magic inst;
	return inst;
}

const std::string& Magic::directory_mime()
{
	static const std::string mime{"inode/directory; charset=binary"};
	return mime;
}

const std::string& Magic::unknown_mime()
{
	static const std::string mime{"application/octet-stream"};
	return mime;
}

const std::string& Magic::extension_to_mime(const std::string& ext)
{
	// Build extension -> mime type mapping
	static const std::unordered_map<std::string, std::string> ext_to_mime
	{
		{".jpg",    "image/jpeg"},
		{".png",    "image/png"},
		{".jpeg",   "image/jpeg"},
		{".avif",   "image/avif"},
		{".webp",   "image/webp"},
		{".css",    "text/css"},
		{".js",     "application/javascript"},
	};
	auto mit = ext_to_mime.find(boost::algorithm::to_lower_copy(ext));
	return mit != ext_to_mime.end() ? mit->second : unknown_mime();
}

} // end of namespace
