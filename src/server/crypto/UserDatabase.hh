/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 9/1/23.
//

#pragma once

#include "crypto/SessionID.hh"
#include "Password.hh"

#include <fmt/format.h>
#include <nlohmann/json.hpp>

#include <boost/asio/any_completion_handler.hpp>

#include <cstdint>
#include <chrono>
#include <filesystem>
#include <string_view>

namespace sanali {
class Connection;
class CommandBuilder;
}

namespace hrb {
constexpr std::size_t min_iteration = 5000;
constexpr std::string_view default_hash_algorithm{"sha512"};

class Configuration;
class UserID;

class ShadowFile
{
public:
	using Hash = Password::Key;
	using Salt = ByteArray<32>;

	struct Entry
	{
		std::string username;   // lower case
		Salt        salt;
		Hash        password_hash;
		std::size_t iteration{min_iteration};
		std::string hash_algorithm{default_hash_algorithm};
	};
	static Entry parse_entry(std::string_view line);

	struct Order
	{
		bool operator()(const Entry& e1, const Entry& e2) const
		{
			return e1.username < e2.username;
		}
	};

public:
	ShadowFile() = default;

	// Both will change last-modified time, so they are not const
	void load(const std::filesystem::path& file, std::error_code& ec);
	void save(const std::filesystem::path& file, std::error_code& ec);

	auto begin() const {return m_en.begin();}
	auto end() const {return m_en.end();}
	auto size() const {return m_en.size();}

	void append(Entry entry){m_en.push_back(std::move(entry));}
	Entry& new_user(std::string username_mixed_case, const Password& password);

	std::vector<Entry>::iterator find(std::string username);

	auto last_modified() const {return m_last_mod;}

private:
	std::vector<Entry>  m_en;
	std::chrono::system_clock::time_point m_last_mod{};
};

inline bool operator==(const ShadowFile::Entry& e1, const ShadowFile::Entry& e2)
{
	return
		std::make_tuple(e1.username, e1.salt, e1.password_hash, e1.iteration, e1.hash_algorithm) ==
		std::make_tuple(e2.username, e2.salt, e2.password_hash, e2.iteration, e2.hash_algorithm);
}
void to_json(nlohmann::json& json, ShadowFile::Entry& input);
void from_json(const nlohmann::json& json, ShadowFile::Entry& output);

class UserDatabase
{
public:
	using time_point = std::chrono::system_clock::time_point;
	using SessionCallback = boost::asio::any_completion_handler<void(UserID&&, std::error_code)>;

public:
	UserDatabase(const Configuration& config, sanali::Connection& redis) :
		m_config{config}, m_redis{redis} {}

	void add_user(std::string username_mixed_case, const Password& password, std::error_code& ec);
	void verify_user(
		std::string username_mixed_case, const Password& password,
		SessionCallback&& comp
	);
	void verify_session(const SessionID& session_id, SessionCallback&& comp);
	void renew_session(UserID&& auth, SessionCallback&& comp);
	void destroy_session(const SessionID& session_id, boost::asio::any_completion_handler<void(std::error_code)> comp);

private:
	void create_session(std::string username, SessionCallback&& comp);

	template <typename Complete>
	void load_shadow_if_outdated(Complete&& comp);

	template <typename Complete>
	void load_shadow_to_redis(Complete&& comp);

private:
	const Configuration&    m_config;
	sanali::Connection&     m_redis;
};

} // end of namespace hrb

// fmt support
template <>
struct fmt::formatter<hrb::ShadowFile::Entry>
{
	constexpr auto parse(format_parse_context& ctx) {return ctx.begin();}
	fmt::format_context::iterator format(const hrb::ShadowFile::Entry& en, fmt::format_context& ctx) const;
};

template <>
struct fmt::formatter<hrb::ShadowFile>
{
	constexpr auto parse(format_parse_context& ctx) {return ctx.begin();}
	fmt::format_context::iterator format(const hrb::ShadowFile& file, fmt::format_context& ctx) const;
};
