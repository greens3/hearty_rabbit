/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 9/1/23.
//

#include "UserDatabase.hh"

#include "hrb/UserID.hh"
#include "crypto/Random.hh"
#include "util/Configuration.hh"
#include "util/Cookie.hh"
#include "util/Escape.hh"
#include "util/Log.hh"
#include "util/Timestamp.hh"
#include "util/Error.hh"

#include <Sanali.hh>

#include <boost/algorithm/string.hpp>

#include <fmt/os.h>

#include <fstream>
#include <stdexcept>

namespace hrb {

ShadowFile::Entry ShadowFile::parse_entry(std::string_view line)
{
	auto [username, salt, hash, iteration, algorithm] = tokenize<5>(line, ":\n");
	if (username.empty())
		throw std::runtime_error("Cannot parse user (empty line?)");

	ShadowFile::Entry en{boost::algorithm::to_lower_copy(std::string{username})};

	if (!en.salt.from_hex(salt))
		throw std::runtime_error{fmt::format("Invalid salt ({}) for user {}", salt, username)};

	if (!en.password_hash.from_hex(hash))
		throw std::runtime_error{fmt::format("Invalid password hash for user {}", username)};

	auto [ptr, ec] = std::from_chars(iteration.begin(), iteration.end(), en.iteration);
	if (ec != std::errc{})
		throw std::runtime_error{fmt::format("Invalid iteration count {} for user {}", iteration, username)};

	en.hash_algorithm = algorithm;
	return en;
}

void ShadowFile::load(const std::filesystem::path& file, std::error_code& ec) try
{
	std::vector<Entry> entries;
	std::ifstream input;
	input.exceptions(std::ios::badbit);
	input.open(file);

	char line[1024];
	while (input.getline(line, sizeof(line)))
		try
		{
			entries.push_back(parse_entry(line));
		}
		catch (std::exception& e)
		{
			Log(LOG_WARNING, "Ignoring shadow file entry {} due to exception ({})", line, e.what());
		}

	// Sort and unique to remove duplicated users
	std::sort(entries.begin(), entries.end(), Order{});
	entries.erase(std::unique(entries.begin(), entries.end()), entries.end());

	// If the read failed because of EOF, that means we successfully read the whole file.
	if (input.eof())
		m_en.swap(entries);

	m_last_mod = file_time_to_systime(last_write_time(file));
	ec.clear();
}
catch (std::system_error& e)
{
	ec = e.code();
}

void ShadowFile::save(const std::filesystem::path& file, std::error_code& ec) try
{
	std::ofstream dest;
	dest.exceptions(std::ios::failbit);
	dest.open(file);
	if (dest)
		fmt::format_to(std::ostreambuf_iterator<char>{dest.rdbuf()}, "{}", *this);
	dest.close();

	m_last_mod = file_time_to_systime(last_write_time(file));
	ec.clear();
}
catch (std::system_error& e)
{
	ec = e.code();
}

void to_json(nlohmann::json& json, const ShadowFile::Entry& input)
{
	// Skip username because it would be the hash key
	json.emplace("salt", input.salt.to_binary_json());
	json.emplace("password_hash", input.password_hash.to_binary_json());
	json.emplace("iteration", input.iteration);
	json.emplace("hash_algorithm", input.hash_algorithm);
}

void from_json(const nlohmann::json& json, ShadowFile::Entry& output)
{
	// Skip username because it would be the hash key
	json.at("salt").get_to(output.salt);
	json.at("password_hash").get_to(output.password_hash);
	json.at("iteration").get_to(output.iteration);
	json.at("hash_algorithm").get_to(output.hash_algorithm);
}

ShadowFile::Entry& ShadowFile::new_user(std::string username, const Password& password)
{
	boost::algorithm::to_lower(username);
	auto [first, last] = std::equal_range(m_en.begin(), m_en.end(), ShadowFile::Entry{username}, Order{});
	if (first == last)
		first = m_en.insert(first, ShadowFile::Entry{username});
	assert(first != m_en.end());
	assert(std::is_sorted(m_en.begin(), m_en.end(), Order{}));

	// Convert username to lower case to ensure case-insensitive comparison.
	auto& new_entry = *first;

	new_entry.salt = insecure_random<ShadowFile::Salt>();
	assert(new_entry.hash_algorithm == default_hash_algorithm);
	assert(new_entry.iteration == min_iteration);

	new_entry.password_hash = password.derive_key(
		boost::asio::buffer(new_entry.salt.base()),
		new_entry.iteration, new_entry.hash_algorithm
	);
	return new_entry;
}

std::vector<ShadowFile::Entry>::iterator ShadowFile::find(std::string username)
{
	assert(std::is_sorted(m_en.begin(), m_en.end(), Order{}));

	boost::algorithm::to_lower(username);
	auto [first, last] = std::equal_range(m_en.begin(), m_en.end(), ShadowFile::Entry{username}, Order{});
	return first == last ? m_en.end() : first;
}

template<typename Complete>
void UserDatabase::load_shadow_if_outdated(Complete&& comp)
{
	m_redis.command([*this, comp=std::forward<Complete>(comp)](auto&& resp, std::error_code ec) mutable
	{
		if (ec || resp.is_error())
		{
			Log(LOG_WARNING, "Error when getting last-mod time of user entries from redis: {} {}", resp, ec);
			return comp(ec);
		}

		// It is not an error if the shadow file is absent. We just treat it as empty.
		auto last_mod_of_file = file_time_to_systime(last_write_time(m_config.shadow_file(), ec));
		if (ec)
		{
			last_mod_of_file = {};
			ec.clear();
		}

		// It is also not an error if the last mod in redis is absent (i.e. nil).
		time_point last_mod_in_redis{time_point::duration{resp.is_int() ? resp.to_int() : 0}};

		if (last_mod_in_redis != last_mod_of_file)
			return load_shadow_to_redis(std::forward<Complete>(comp));
		else
			return comp(ec);
	}, "GET {}user-db:last-mod", m_config.redis_prefix());
}

template <typename Complete>
void UserDatabase::load_shadow_to_redis(Complete&& comp)
{
	// Start a transaction. Abort if someone else modified the last-modified time.
	// We must wait until the "WATCH" command to finish before read the file.
	m_redis.command([*this, comp=std::forward<Complete>(comp)](auto&& resp, std::error_code ec) mutable
	{
		if (ec || resp.is_error())
		{
			Log(LOG_WARNING, "Error when WATCHing user database update: {} {}", resp, ec);
			return comp(ec);
		}

		ShadowFile shadow;
		shadow.load(m_config.shadow_file(), ec);
		if (ec)
			return comp(ec);

		auto log_error = [](auto&& resp, auto ec)
		{
			if (ec || resp.is_error())
				Log(LOG_WARNING, "Error in queued command: {} {}", resp, ec);
		};

		m_redis.queue_command(log_error, "MULTI");
		m_redis.queue_command(log_error, "SET {}users:last-mod {}",
			m_config.redis_prefix(), shadow.last_modified()
		);
		m_redis.queue_command(log_error, "DEL {}users", m_config.redis_prefix());

		sanali::CommandBuilder hset{"HSET {}users", m_config.redis_prefix()};
		for (auto& en : shadow)
			hset.append("{}", en.username).
				append("{}", boost::asio::buffer(nlohmann::json::to_msgpack(nlohmann::json(en))));
		m_redis.queue_command(log_error, std::move(hset));

		m_redis.command([comp=std::forward<Complete>(comp), *this](auto&& resp, auto ec) mutable
		{
			if (ec || resp.is_error())
			{
				Log(LOG_WARNING, "Error loading user database to redis: {} {}", resp, ec);
			}

			// Transaction discarded: some other thread has updated the user records in redis.
			// We need to start over.
			else if (!ec && resp.is_nil())
			{
				return load_shadow_to_redis(std::forward<Complete>(comp));
			}
			return comp(ec);
		}, "EXEC");
	}, "WATCH {}users:last-mod", m_config.redis_prefix());
}

void UserDatabase::add_user(std::string username_mixed_case, const Password& password, std::error_code& ec)
{
	ShadowFile shadow;

	// It's not a fatal error if we can't load the user database file.
	// We just treat it as empty.
	// "ec" will be overwritten by the outcome of save() later. The error from load() will be ignored.
	shadow.load(m_config.shadow_file(), ec);
	if (ec)
		Log(LOG_WARNING, "Cannot open password database at {}: {} ({})", m_config.shadow_file(), ec, ec.message());

	shadow.new_user(std::move(username_mixed_case), password);

	shadow.save(m_config.shadow_file(), ec);
	if (ec)
		Log(LOG_WARNING, "Cannot save password database at {}: {} ({})", m_config.shadow_file(), ec, ec.message());
}

void UserDatabase::verify_user(
	std::string username_mixed_case,
	const Password& password,
	SessionCallback&& comp
)
{
	auto username{std::move(username_mixed_case)};
	boost::algorithm::to_lower(username);

	load_shadow_if_outdated([username, password, comp=std::move(comp), *this](std::error_code ec) mutable
	{
		if (ec)
			return comp(UserID{}, ec);

		m_redis.command([username, password, comp=std::move(comp), *this](auto&& resp, auto ec) mutable
		{
			if (ec || resp.is_error())
			{
				Log(LOG_WARNING, "Error getting user entry from redis: {} {}", resp, ec);
				if (!ec)
					ec = Error::server_error;
				return comp(UserID{}, ec);
			}
			if (resp.is_nil())
			{
				Log(LOG_INFO, "User not found in database: {}", username);
				return comp(UserID{}, make_error_code(std::errc::permission_denied));
			}
			try
			{
				auto en = nlohmann::json::from_msgpack(std::move(resp.as_string())).template get<ShadowFile::Entry>();
				auto pkey = password.derive_key(en.salt.buffer(), en.iteration, en.hash_algorithm);
				if (pkey.size() != en.password_hash.size() ||
					::CRYPTO_memcmp(pkey.data(), en.password_hash.data(), pkey.size()) != 0)
				{
					Log(LOG_INFO, "User login incorrect: {}", username);
					return comp(UserID{}, make_error_code(std::errc::permission_denied));
				}

				return create_session(std::move(username), std::move(comp));
			}
			catch (std::exception& e)
			{
				Log(LOG_WARNING, "Exception caught when decoded user entry ({}) from redis: {}", username, e.what());
				return comp(UserID{}, make_error_code(Error::server_error));
			}
			catch (...)
			{
				return comp(UserID{}, make_error_code(Error::unknown_error));
			}

		}, "HGET {}users {}", m_config.redis_prefix(), username);
	});
}

void UserDatabase::create_session(std::string username, SessionCallback&& comp)
{
	boost::algorithm::to_lower(username);

	// Generate session ID and store it in database
	// Session ID must be generated by cryptographically secure random number generator.
	UserID auth{secure_random<SessionID>(), std::move(username)};
	m_redis.command(
		[auth, comp=std::move(comp)](auto&& resp, std::error_code ec) mutable
		{
			if (ec || resp.is_error())
			{
				Log(LOG_WARNING, "Error creating user session in redis: {} {}", resp, ec);
				if (!ec)
					ec = Error::server_error;
				return comp(UserID{}, ec);
			}

			return comp(std::move(auth), ec);
		},

		"SET {}session:{} _{} EX {}",
		m_config.redis_prefix(), auth.session()->buffer(),
		auth.username(), m_config.session_length().count()
	);
}

void UserDatabase::verify_session(const SessionID& session_id, UserDatabase::SessionCallback&& comp)
{
	static const char lua[] = "return {redis.call('GET', KEYS[1]), redis.call('TTL', KEYS[1])}";

	m_redis.command(
		[comp=std::move(comp), session_id, *this](auto&& resp, auto&& ec) mutable
		{
			if (ec || resp.is_error())
			{
				Log(LOG_WARNING, "Error verifying user session in redis: {} {}", resp, ec);
				if (!ec)
					ec = Error::server_error;
				return comp(UserID{}, ec);
			}

			auto [user, ttl] = resp.template as_tuple<2>(ec);
			if (ec || !user.is_string() || user.as_string().size() <= 1 || !ttl.is_int() || ttl.as_int() < 0)
				return comp(UserID{}, make_error_code(std::errc::permission_denied));

			UserID auth{session_id, user.as_string().substr(1)};
			if (ttl.to_int() < m_config.session_length().count()/2)
			{
				Log(LOG_NOTICE, "{} seconds before session timeout. Renewing session", ttl.as_int());
				return renew_session(std::move(auth), std::move(comp));
			}
			else
			{
				return comp(std::move(auth), ec);
			}
		},
		"EVAL {} 1 {}session:{}", lua, m_config.redis_prefix(), session_id.buffer()
	);
}

void UserDatabase::renew_session(UserID&& auth, UserDatabase::SessionCallback&& comp)
{
	assert(auth.valid());
	auto new_session_id = secure_random<SessionID>();

	// Check if the old session is already renewed and renew it atomically.

	// If the value of the old session key start with an underscore
	// that means it has not been renewed.

	// When renewing, expire the old session cookie in 30 seconds.
	// This is to avoid session error for the pending requests in the
	// pipeline. These requests should still be using the old session.
	static const char lua[] = R"__(
		if string.sub(redis.call('GET', KEYS[1]), 1, 1) == '_'
		then
			redis.call('SET', KEYS[1], '#' .. ARGV[1], 'EX', 30)
			redis.call('SET', KEYS[2], '_' .. ARGV[1], 'EX', ARGV[2])
			return 1
		else
			return 0
		end
	)__";
	m_redis.command(
		[comp=std::move(comp), new_session_id, auth](auto&& reply, auto ec) mutable
		{
			// if error occurs or session already renewed, use the old session
			if (ec || reply.to_int() != 1)
			{
				Log(LOG_NOTICE,
					"user {} session already renewed: {} ({}) {}", auth.username(), ec, ec.message(), reply
				);
				return comp(std::move(auth), ec);
			}
			else
			{
				Log(LOG_NOTICE, "user {} session renewed successfully", auth.username());
				return comp(UserID{new_session_id, auth.username()}, ec);
			}
		},
		"EVAL {} 2 {}session:{} {}session:{} {} {}",
		lua,
		m_config.redis_prefix(), // KEYS[1]: old session cookie
		auth.session()->buffer(),
		m_config.redis_prefix(), // KEYS[2]: new session cookie
		new_session_id.buffer(),
		auth.username(),         // ARGV[1]: username
		m_config.session_length().count()   // ARGV[2]: session length
	);

}

void UserDatabase::destroy_session(
	const SessionID& session_id,
	boost::asio::any_completion_handler<void(std::error_code)> comp
)
{
	m_redis.command([comp=std::move(comp), session_id](auto&& reply, auto ec) mutable
		{
			if (ec || reply.to_int() != 1)
			{
				Log(LOG_NOTICE,
					"Session {} is not a valid session: {} ({}) {}", session_id, ec, ec.message(), reply
				);
				if (!ec)
					ec = make_error_code(std::errc::invalid_argument);
			}

			comp(ec);
		},
		"DEL {}session:{}", m_config.redis_prefix(), session_id.buffer()
	);
}

} // end of namespace hrb

auto fmt::formatter<hrb::ShadowFile::Entry>::format(const hrb::ShadowFile::Entry& en, fmt::format_context& ctx) const ->
fmt::format_context::iterator
{
	return fmt::format_to(
		ctx.out(), "{}:{}:{}:{}:{}", en.username, en.salt, en.password_hash, en.iteration, en.hash_algorithm
	);
}

auto fmt::formatter<hrb::ShadowFile>::format(const hrb::ShadowFile& file, fmt::format_context& ctx) const ->
fmt::format_context::iterator
{
	auto out = ctx.out();
	for (auto& en : file)
		out = fmt::format_to(out, "{}\n", en);
	return out;
}
