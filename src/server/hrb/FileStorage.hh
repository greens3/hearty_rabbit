/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 8/7/23.
//

#pragma once

#include <filesystem>

#include <boost/asio/any_completion_handler.hpp>

namespace sanali {
class Connection;
class CommandBuilder;
}

namespace hrb {

class Configuration;
class DirectoryEntry;
class DirectoryListing;

/// This class present all disk operations on the files that hearty rabbit is serving.
/// It integrates the base directory and the blob cache layer. In addition, this class
/// does not process file permissions. It does obtain the file permissions from the
/// underlying file-system backend, but it does not determine if a specific
/// user is allowed to access the file.
class FileStorage
{
public:
	using ListingCallback = boost::asio::any_completion_handler<void(DirectoryListing&&, std::error_code)>;
	using EntryCallback   = boost::asio::any_completion_handler<void(DirectoryEntry&&, std::error_code)>;
	using PathEntryCallback  = boost::asio::any_completion_handler<
	    void(std::filesystem::path, DirectoryEntry&&, std::error_code)
	>;

public:
	FileStorage(const Configuration& config, sanali::Connection& redis) :
		m_config{config}, m_redis{redis}
	{
	}

	// If "logical_path" points to a file, the DirectoryListing returned will
	// contain a list of files in its parent directory.
	void get_listing(std::string_view logical_path, ListingCallback&& comp) const;

	void get_entry(std::string_view logical_path, std::string rendition, PathEntryCallback&& comp) const;

	void generate_renditions(
		const std::filesystem::path& physical_path,
		std::vector<std::string> renditions,
		boost::asio::any_completion_handler<void(std::error_code)> comp
	) const;

private:
	struct PhysicalPath : public std::filesystem::path{using path::path;};
	struct LogicalPath  : public std::filesystem::path{using path::path;};
	static_assert(!std::is_convertible_v<PhysicalPath, LogicalPath>);

	PhysicalPath to_physical(std::string_view logical_path) const;
	LogicalPath to_logical(const PhysicalPath& physical_path) const;

	using time_point = std::chrono::system_clock::time_point;
	void cache_directory(
		DirectoryListing&& listing,
		time_point max_mod,
		ListingCallback&& comp
	) const;
	void fetch_listing_from_redis(const LogicalPath& path, ListingCallback&& comp) const;
	void fetch_entry_from_redis(const LogicalPath& logical_path, EntryCallback&& comp) const;
	void lookup_rendition(
		PhysicalPath path,
		const std::string& rendition,
		DirectoryEntry&& dir_entry,
		PathEntryCallback&& comp
	) const;
	PhysicalPath try_refresh_dir_entry(
		const LogicalPath& logical_path,
		DirectoryEntry& dir_entry,
		std::error_code& ec
	) const;
	sanali::CommandBuilder sync_listing(
		DirectoryListing& listing,
		const PhysicalPath& path
	) const;

private:
	const Configuration&    m_config;
	sanali::Connection&     m_redis;
};

} // end of namespace hrb
