/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 8/7/23.
//

#include "FileStorage.hh"
#include "RenditionCache.hh"

#include "hrb/DirectoryListing.hh"
#include "util/Configuration.hh"
#include "util/Escape.hh"
#include "util/Log.hh"
#include "util/Timestamp.hh"

#include <Sanali.hh>

namespace hrb {

// Shortcut
using time_point = DirectoryListing::time_point;

FileStorage::PhysicalPath FileStorage::to_physical(std::string_view logical_path) const
{
	assert(m_config.base_path().is_absolute());
	auto relative_path{(std::filesystem::path{"/"} / logical_path).lexically_normal().relative_path()};
	return PhysicalPath{(m_config.base_path()/relative_path).lexically_normal()};
}

FileStorage::LogicalPath FileStorage::to_logical(const PhysicalPath& physical_path) const
{
	assert(m_config.base_path().is_absolute());
	assert(physical_path.is_absolute());
	return LogicalPath{(LogicalPath{"/"} / physical_path.lexically_relative(m_config.base_path())).lexically_normal()};
}

void FileStorage::generate_renditions(
	const std::filesystem::path& physical_path,
	std::vector<std::string> renditions,
	boost::asio::any_completion_handler<void(std::error_code)> comp
) const
{
	auto logical_path = to_logical(PhysicalPath{physical_path});

	// Cannot generate rendition for directories. It makes no sense to pass this function a path
	// referring to a directory.
	// Note that even if a file with the corresponding name exists, e.g. "/some-file" exists,
	// calling this function with "/some-file/" will still fail.
	if (!physical_path.has_filename() || !logical_path.has_filename())
		return comp(std::make_error_code(std::errc::is_a_directory));

	fetch_entry_from_redis(
		logical_path,
		[comp = std::move(comp), renditions = std::move(renditions), *this, logical_path]
		(DirectoryEntry&& dir_entry, auto ec) mutable
		{
			if (ec)
				return std::move(comp)(ec);

			auto physical_path = try_refresh_dir_entry(logical_path, dir_entry, ec);
			if (ec)
				return std::move(comp)(ec);

			assert(dir_entry.is_directory() || dir_entry.hash());
			if (dir_entry.is_directory())
				return std::move(comp)(std::make_error_code(std::errc::is_a_directory));

			if (auto rend_setting = m_config.renditions(dir_entry.mime()); rend_setting)
			{
				auto hex = dir_entry.hash()->to_hex();
				auto blob_dir = m_config.blob_path() / hex.substr(0, 2) / hex;

				assert(blob_dir.is_absolute());
				RenditionCache blob_file{blob_dir, physical_path};

				for (auto& rend: renditions)
					blob_file.rendition(rend, *rend_setting, m_config.haar_path(), ec);
			}
			return comp(ec);
		}
	);}

void FileStorage::fetch_entry_from_redis(const LogicalPath& logical_path, EntryCallback&& comp) const
{
	// Make no sense to fetch the directory entry of the root directory from its "parent"
	assert(logical_path != logical_path.parent_path());
	assert(logical_path.is_absolute());
	assert(logical_path.has_filename());

	// Try to get it from cache
	m_redis.command([comp=std::move(comp), logical_path](auto&& resp, std::error_code ec) mutable
	{
		DirectoryEntry dir_entry;
		if (resp.is_string() && !ec)
		{
			// msgpack decode error is not a big deal.
			// try_refresh_dir_entry() will fix that.
			try
			{
				nlohmann::json::from_msgpack(std::move(resp.as_string())).get_to(dir_entry);
			}
			catch (std::exception& e)
			{
				Log(LOG_WARNING, "Invalid cache entry {}: {}", logical_path.string(), e.what());
			}
		}
		return std::move(comp)(std::move(dir_entry), ec);
	}, "HGET {}dir:{} {}",
	m_config.redis_prefix(), logical_path.parent_path().string(), logical_path.filename().string());
}

void FileStorage::get_entry(std::string_view logical_path, std::string rendition, PathEntryCallback&& comp) const
{
	LogicalPath log_path{logical_path};
	assert(log_path.root_name().empty());

	// DirectoryEntries are stored in the redis hash of its parent directory. Since the root directory
	// has no parent, we do not store the DirectoryEntry of the root directory.
	if (log_path == log_path.root_directory())
		return std::move(comp)(
			to_physical(logical_path), std::move(DirectoryEntry{}.set_directory()),
			std::make_error_code(std::errc::not_supported)
		);

	// It's possible to get a DirectoryEntry from a directory.
	if (!log_path.has_filename())
		log_path = LogicalPath{log_path.parent_path()};
	assert(!log_path.empty());
	assert(log_path.has_filename());

	fetch_entry_from_redis(
		log_path,
		[comp = std::move(comp), rendition = std::move(rendition), *this, log_path]
		(DirectoryEntry&& dir_entry, auto ec) mutable
		{
			if (ec)
				return std::move(comp)(std::filesystem::path{}, std::move(dir_entry), ec);

			auto physical_path = try_refresh_dir_entry(log_path, dir_entry, ec);
			if (ec)
				return std::move(comp)(physical_path, DirectoryEntry{}, ec);

			// No need to lookup rendition for subdirectories
			if (dir_entry.is_directory())
				return std::move(comp)(physical_path, std::move(dir_entry), ec);

			assert(dir_entry.hash().has_value());
			return lookup_rendition(physical_path, rendition, std::move(dir_entry), std::move(comp));
		}
	);
}

void FileStorage::lookup_rendition(
	PhysicalPath        physical_path,
	const std::string&  rendition,
	DirectoryEntry&&    dir_entry,
	PathEntryCallback&& comp
) const
{
	assert(!dir_entry.is_directory());
	assert(dir_entry.hash().has_value());
	assert(*dir_entry.hash() != DirectoryEntry::FileHash{});

	std::error_code ec;

	// Special handling for master rendition
	if (rendition == "master")
	{
		// dir_entry.mime() and physical_path are referring to the real file, i.e. master rendition.
		// No need for further processing.
	}
	else if (auto rend_setting = m_config.renditions(dir_entry.mime()); rend_setting)
	{
		auto hex = dir_entry.hash()->to_hex();
		auto blob_dir = m_config.blob_path() / hex.substr(0, 2) / hex;

		assert(blob_dir.is_absolute());
		RenditionCache blob_file{blob_dir, physical_path};

		// if blob_file.rendition() return empty optional, "ec" will have the reason of the error.
		if (auto rend_result = blob_file.rendition(rendition, *rend_setting, m_config.haar_path(), ec); rend_result)
		{
			assert(!ec);
			dir_entry.set_mime(rend_result->setting.mime);
			physical_path = PhysicalPath{std::move(rend_result->result)};
		}
	}
	return comp(std::move(physical_path), std::move(dir_entry), ec);
}

/// Retrieve a listing of entries for a directory.
/// This is actually quite complicated. There are a few cases:
///     1.  \a logical_path refers to a file, e.g. "/foo/bar". The returned DirectoryListing
///         will contain a listing of the parent directory (i.e. "/foo/") and the filename
///         field inside the DirectoryListing will be the file (i.e. "bar")
///     2.  \a logical_path refers to a directory, e.g. "/foo/bar/". The returned DirectoryListing
///         will contain a listing of this directory. The filename field of the listing will be empty.
///			If "/foo/bar" is a file instead of a directory, get_listing() will fail with error "not_a_directory".
///     3.  \a logical_path refers to a directory, but it does not have an ending directory separator.
///         It's the same case as above, except for the "logical_path" field inside the returned
///         DirectoryListing will have the ending directory separator appended.
void FileStorage::get_listing(std::string_view logical_path, ListingCallback&& comp) const
{
	fetch_listing_from_redis(LogicalPath{std::filesystem::path{logical_path}.lexically_normal()}, std::move(comp));
}

void FileStorage::cache_directory(DirectoryListing&& listing, time_point max_mod, ListingCallback&& comp) const
{
	auto log_error = [](auto&& resp, auto ec)
	{
		if (ec)
			Log(LOG_WARNING, "Error in queued command: {} {}", resp, ec);
	};

	auto delete_cache = [*this, log_error, logical_path=listing.logical_path()]
	{
		m_redis.queue_command(log_error, "MULTI");
		m_redis.queue_command(log_error, "DEL {}dir:{}",     m_config.redis_prefix(), logical_path);
		m_redis.queue_command(log_error, "DEL {}max:mod:{}", m_config.redis_prefix(), logical_path);
		m_redis.command(log_error, "EXEC");
	};

	auto check_listing_before_return = [*this](DirectoryListing&& listing, ListingCallback&& comp)
	{
		// If the listing has a filename, we need to check if the filename is valid
		if (!listing.filename().empty())
		{
			auto entry = listing.find(listing.filename());

			// The entry referred by filename does not exist.
			// Remove the filename from listing and return error.
			if (entry == listing.end())
				return std::move(comp)(
					std::move(listing.set_filename()),
					make_error_code(std::errc::no_such_file_or_directory)
				);

			// The entry exists, but it refers to a directory instead of a file,
			// we need to start over.
			if (entry->second.is_directory())
				return fetch_listing_from_redis(
					LogicalPath{listing.logical_path()+listing.filename()+'/'},
					std::move(comp)
				);
		}

		// Everything seems fine, we can return the listing.
		std::move(comp)(std::move(listing), std::error_code{});
	};

	PhysicalPath physical_path{to_physical(listing.logical_path())};

	std::error_code ec;
	auto dir_status = status(physical_path, ec);
	if (ec)
	{
		Log(LOG_INFO, "Directory {} does not exist ({}: {}). Deleting cache.", physical_path.string(), ec, ec.message());
		delete_cache();
		return std::move(comp)(std::move(listing), ec);
	}

	if (!is_directory(dir_status))
	{
		Log(LOG_INFO, "Directory {} becomes a regular file ({}: {}). Deleting cache.", physical_path.string(), ec, ec.message());
		delete_cache();
		return std::move(comp)(std::move(listing), make_error_code(std::errc::not_a_directory));
	}

	auto modified = file_time_to_systime(last_write_time(physical_path));
	if (modified == max_mod)
	{
		Log(
			LOG_INFO, "Cache hit for dir '{}': {} files. (Modified: {})",
			listing.logical_path(), listing.size(), modified
		);
		return check_listing_before_return(std::move(listing), std::move(comp));
	}

	Log(LOG_INFO, "Cache outdated for dir '{}'. Need to re-generate listing.", listing.logical_path());

	LogicalPath logical_path{listing.logical_path()};
	assert(logical_path == to_logical(physical_path));

	auto transaction = [
		*this, listing=std::move(listing), comp=std::move(comp), modified, physical_path, log_error,
		check_listing_before_return
	](auto&& resp, auto ec) mutable
	{
		if (ec)
		{
			Log(
				LOG_WARNING, "Error when WATCHing directory modification time: {} {} ({})",
				listing.logical_path(), resp, ec
			);
			return comp(std::move(listing), ec);
		}

		auto& prefix = m_config.redis_prefix();
		auto& logical_path = listing.logical_path();

		// Start a transaction.
		// Using transaction instead of LUA script to update directory entries.
		// LUA script will fail if the directory has lots of files (5000+).
		m_redis.queue_command(log_error, "MULTI");
		m_redis.queue_command(log_error, "DEL {}dir:{}", prefix, logical_path);
		m_redis.queue_command(
			log_error, "SET {}max:mod:{} {}", prefix, logical_path,
			modified.time_since_epoch().count()
		);

		// If the directory has no entries, the HSET command will have not enough arguments.
		auto hset = sync_listing(listing, physical_path);
		if (!listing.empty())
			m_redis.queue_command(log_error, std::move(hset));

		auto on_exec = [
			comp=std::move(comp), listing=std::move(listing), physical_path, *this, check_listing_before_return
		](auto&& resp, auto ec) mutable
		{
			if (ec)
			{
				Log(LOG_WARNING, "Error caching directory listing: {} {} ({})", resp, ec, ec.message());
				return std::move(comp)(std::move(listing), ec);
			}
			// Transaction discarded: some other thread has updated the directory cache during
			// our transaction. We need to start over again. Hopefully we will get a cache hit
			// next time.
			else if (!ec && resp.is_nil())
			{
				// The DirectoryEntries in "listing" are outdated, so throw them away.
				listing.set_entries({});
				return fetch_listing_from_redis(
					LogicalPath{listing.logical_path()+'/'+listing.filename()}, std::move(comp));
			}

			else
				return check_listing_before_return(std::move(listing), std::move(comp));
		};
		m_redis.command(std::move(on_exec), "EXEC");
	};

	// Discard the upcoming transaction if the modification time or directory entries changed. That means some
	// other threads have updated the cache before we finish.
	// WATCH commands must be issued before starting transaction (i.e. EXEC)
	m_redis.command(
		std::move(transaction),
		"WATCH {}max:mod:{} {}dir:{}",
		m_config.redis_prefix(), logical_path.string(), m_config.redis_prefix(), logical_path.string()
	);
}

/// Synchronized a DirectoryListing from Redis cache with the actual directory in the file-system.
/// \param  listing     A DirectoryListing that may contain some outdated entries. It will be
///                     updated to reflect the actual content of the directory, meaning that
///                     outdated entries will be removed and missing entries will be added.
/// \param  path   Path to the physical directory in the file-system.
sanali::CommandBuilder FileStorage::sync_listing(DirectoryListing& listing, const PhysicalPath& path) const
{
	DirectoryListing::Entries updated_entries;
	sanali::CommandBuilder hset{"HSET {}dir:{}", m_config.redis_prefix(), listing.logical_path()};
	for (auto&& entry: std::filesystem::directory_iterator{path})
	{
		auto filename = entry.path().filename().generic_string();
		auto cache = listing.find(filename);

		// Try to find a corresponding cache entry
		if (cache != listing.end() && file_time_to_systime(entry.last_write_time()) <= cache->second.modified())
			std::tie(cache, std::ignore) = updated_entries.insert(std::move(*cache));
		else
		{
			std::error_code den_error;
			if (DirectoryEntry e{entry, den_error}; !den_error)
				std::tie(cache, std::ignore) = updated_entries.try_emplace(filename, std::move(e));

			// Discard the entry
			else
				cache = updated_entries.end();
		}

		// Now "cache" is an iterator to "updated_entries", not "listing"
		if (cache != updated_entries.end())
			hset.append("{}", filename).append("{}", boost::asio::buffer(cache->second.to_msgpack()));
	}

	Log(LOG_INFO, "Cache entry generated with {} files", updated_entries.size());
	listing.set_entries(std::move(updated_entries));

	return hset;
}

void FileStorage::fetch_listing_from_redis(const LogicalPath& path, ListingCallback&& comp) const
{
	assert(path == path.lexically_normal());

	DirectoryListing result;
	if (path.has_filename())
	{
		result.set_filename(path.filename().string());
		result.set_logical_path(path.parent_path().string());
	}
	else
		result.set_logical_path(path);

	assert(!LogicalPath{result.logical_path()}.has_filename());
	assert(!result.logical_path().empty());
	assert(result.logical_path().back() == '/');

	m_redis.command(
		[comp=std::move(comp), result, *this](auto&& resp, std::error_code ec) mutable
		{
			if (ec && ec != sanali::Error::command_error)
			{
				Log(LOG_WARNING, "Redis command error: {} {} ({})", resp, ec, ec.message());
				return comp(std::move(result), ec);
			}

			// This is not an error if the result of the script is not as expected.
			// Just regenerate the whole cache entry.
			if (!resp.is_array())
			{
				Log(LOG_WARNING, "Ignoring invalid cache for dir {}: {}", result.logical_path(), resp);
				return cache_directory(std::move(result), {}, std::move(comp));
			}

			auto& array = resp.as_array();

			// Cache entry is invalid. It should at least have the max modification time
			// in the array.
			if (array.empty())
				return cache_directory(std::move(result), {}, std::move(comp));

			// Max modification time is at the end of the array.
			// Ignore conversion error: to_int() will return 0 when error.
			time_point::duration max_mod{array.back().to_int()};
			array.pop_back();

			// The rest of the array contains the directory entries
			for (auto&& kv : resp.kv_pairs())
				try
				{
					result.add(kv.key(), DirectoryEntry::from_msgpack(std::move(kv.value().as_string())));
				}
				catch (std::exception& e)
				{
					Log(
						LOG_WARNING, "Exception caught when decoding directory cache '{}{}' from redis: {}",
						result.logical_path(), kv.key(), e.what()
					);
				}

			// Next step:
			return cache_directory(std::move(result), time_point{max_mod}, std::move(comp));
		},
		"EVAL {} 2 {}max:mod:{} {}dir:{}",
		R"___(
		local result = redis.call("HGETALL", KEYS[2])
		table.insert(result, redis.call("GET", KEYS[1]))
		return result
		)___",
		m_config.redis_prefix(), result.logical_path(),
		m_config.redis_prefix(), result.logical_path()
	);
}

FileStorage::PhysicalPath FileStorage::try_refresh_dir_entry(
	const LogicalPath& logical_path,
	DirectoryEntry& dir_entry,
	std::error_code& ec
) const
{
	auto physical_path = to_physical(logical_path.string());

	assert(!logical_path.empty());
	assert(logical_path == logical_path.lexically_normal());
	assert(!logical_path.filename().empty());

	// There is no need to store the DirectoryEntry of the root directory
	// to redis. It has no parent_path() anyway.
	if (dir_entry.full_update(physical_path, ec) && !ec)
	{
		m_redis.command(
			[physical_path](auto&& resp, std::error_code ec) mutable
			{
				if (ec || resp.is_error())
					Log(
						LOG_WARNING,
						"Error updating directory entry in redis for file {}: '{}' {}",
						physical_path.string(), resp, ec
					);
			},
			"HSET {}dir:{} {} {}",
			m_config.redis_prefix(),
			logical_path.parent_path().string(),
			logical_path.filename().string(),
			boost::asio::buffer(dir_entry.to_msgpack())
		);
	}

	if (ec)
		Log(LOG_WARNING, "Error occurs when updating directory entry: {} ({})", ec, ec.message());

	return physical_path;
}

} // end of namespace hrb
