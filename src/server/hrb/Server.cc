
/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/7/18.
//

#include "Server.hh"

#include "RequestHandler.hh"
#include "net/Listener.hh"

#include "crypto/Password.hh"
#include "crypto/UserDatabase.hh"

#include "util/Configuration.hh"
#include "util/Error.hh"
#include "util/Exception.hh"

#include "Sanali.hh"

#include <boost/asio/deadline_timer.hpp>
#include <boost/exception/errinfo_api_function.hpp>
#include <boost/exception/info.hpp>

namespace hrb {

Server::Server(const Configuration& cfg) :
	m_cfg{cfg},
	m_ioc{static_cast<int>(std::max(1UL, cfg.thread_count()))},
	m_lib{cfg.web_root()},
	m_sync_strand{boost::asio::make_strand(m_ioc.get_executor())},
	m_sync{m_cfg, m_sync_strand},
	m_watcher{m_sync_strand, cfg.base_path(), m_sync}
{
}

void Server::listen()
{
	m_ssl.set_options(
		boost::asio::ssl::context::default_workarounds |
		boost::asio::ssl::context::no_sslv2
	);
	m_ssl.use_certificate_chain_file(m_cfg.cert_chain().string());
	m_ssl.use_private_key_file(m_cfg.private_key().string(), boost::asio::ssl::context::pem);

	auto factory = [this](boost::asio::any_io_executor executor)
	{
		return RequestHandler{
			m_lib,
			m_cfg.connect_to_redis(std::move(executor)),
			m_cfg
		};
	};

	// Create and launch a listening port for HTTP and HTTPS
	std::make_shared<Listener<RequestHandler>>(m_ioc, factory, nullptr, m_cfg.web_server())->run();
	std::make_shared<Listener<RequestHandler>>(m_ioc, factory, &m_ssl, m_cfg.web_server())->run();
}

void Server::drop_privileges() const
{
	// drop privileges if run as root
	if (::getuid() == 0)
	{
		// must set group ID before setting user ID, otherwise we have no
		// privilege to set group ID
		if (::setgid(m_cfg.group_id()) != 0)
			BOOST_THROW_EXCEPTION(hrb::SystemError()
				<< ErrorCode(std::error_code(errno, std::system_category()))
				<< boost::errinfo_api_function("setgid")
			);

		if (::setuid(m_cfg.user_id()) != 0)
			BOOST_THROW_EXCEPTION(hrb::SystemError()
				<< ErrorCode(std::error_code(errno, std::system_category()))
				<< boost::errinfo_api_function("setuid")
			);
	}

	if (::getuid() == 0)
		throw std::runtime_error("cannot run as root");
}

void Server::add_user(std::string username, const Password& password, std::error_code& ec)
{
	// Not used
	auto redis = sanali::connect(m_ioc, m_cfg.redis_host(), m_cfg.redis_port());

	UserDatabase user_db{m_cfg, *redis};
	user_db.add_user(std::move(username), password, ec);
}

} // end of namespace
