/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/2/23.
//

#pragma once

#include "crypto/SessionID.hh"
#include "net/Request.hh"
#include "hrb/RequestIntent.hh"
#include "hrb/UserID.hh"

#include <boost/asio/any_completion_handler.hpp>

#include <cassert>
#include <functional>
#include <memory>
#include <optional>

namespace sanali {class Connection;}
namespace hrb {

class WebResources;
class Configuration;
class DirectoryEntry;

// This class is responsible to handle all requests of hearty rabbit. Internally, the
// requests will be dispatched to smaller handlers according to their intent (i.e.
// RequestIntent).
class RequestHandler
{
public:
	RequestHandler(
		const WebResources& lib,
		std::shared_ptr<sanali::Connection> db,
		const Configuration& cfg
	) : m_lib{lib}, m_redis{std::move(db)}, m_cfg{cfg}
	{
	}

	void on_header(
		const RequestHeader& header,
		boost::asio::any_completion_handler<void(std::error_code)> callback
	);

	template <class Request, class Send>
	void on_body(Request&& req, Send&& send)
	{
		if (req.method() != m_intent.method())
			return send(bad_request("unsupported request method", req.version()));

		auto finalize_and_send = [this, send=std::forward<Send>(send), session=session_id(req)](auto&& response) mutable
		{
			finalize_response_before_send(session, response);
			send(std::forward<decltype(response)>(response));
		};

		switch(m_intent.action())
		{
		using enum RequestIntent::Action;
		case login:
			if constexpr (requires{handle_login(req, send);})
				return handle_login(std::forward<Request>(req), std::move(finalize_and_send));
			break;

		case logout:
			if constexpr (requires{handle_logout(req, send);})
				return handle_logout(std::forward<Request>(req), std::move(finalize_and_send));
			break;
		case view:
			if constexpr (requires{handle_view(req, send);})
				return handle_view(std::forward<Request>(req), std::move(finalize_and_send));
			break;
		case list:
			if constexpr (requires{handle_list(req, send);})
				return handle_list(std::forward<Request>(req), std::move(finalize_and_send));
			break;
		case content:
		case meta:
			if constexpr (requires{handle_lookup(req, send);})
				return handle_lookup(std::forward<Request>(req), std::move(finalize_and_send));
			break;
		case lib:
			if constexpr (requires{handle_lib(req, send);})
				return handle_lib(std::forward<Request>(req), std::move(finalize_and_send));
			break;
		default:
			break;
		}

		return finalize_and_send(bad_request("unsupported request method", req.version()));
	}

	// For UT verification
	auto& user() const {return m_auth;}

private:
	void handle_login(const StringRequest& req, EmptyResponseSender send);
	void handle_logout(const EmptyRequest& req, EmptyResponseSender send);
	void handle_view(const EmptyRequest& req, SplitBufferResponseSender&& send);
	void handle_list(const EmptyRequest& req, StringResponseSender&& send);
	void handle_lib(const EmptyRequest& req, SplitBufferResponseSender&& send);

	using LookupSender = boost::asio::any_completion_handler<
		void(http::response<http::empty_body>&&),
		void(http::response<http::string_body>&&),
		void(http::response<http::file_body>&&)
	>;
	void handle_lookup(const EmptyRequest& req, LookupSender send);
	static void send_file(
		const std::filesystem::path& physical_file,
		const std::string& filename,
		const DirectoryEntry& dir_entry,
		const std::string& etag_from_request,
		unsigned version,
		LookupSender&& send
	);

	template <typename Response>
	static void set_cache_control(const DirectoryEntry& dir_entry, Response& response);

	static std::optional<SessionID> session_id(const RequestHeader& header);

	// Called just before the response message to be sent.
	// Perform some common tasks for all responses, i.e. set session cookie.
	void finalize_response_before_send(const std::optional<SessionID>& session, ResponseHeader& response);

private:
	const WebResources&                 m_lib;
	std::shared_ptr<sanali::Connection> m_redis;
	const Configuration&                m_cfg;
	RequestIntent                       m_intent;
	UserID	                            m_auth;
};

} // end of namespace hrb
