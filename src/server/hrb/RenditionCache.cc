/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 3/3/18.
//

#include "RenditionCache.hh"
#include "UploadFile.hh"

// HeartyRabbit headers
#include "image/ImageContent.hh"
#include "util/Log.hh"
#include "util/MMap.hh"
#include "util/RenditionSetting.hh"

// OpenCV for calculating phash
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

namespace hrb {

namespace {

cv::Mat square_crop(const cv::Mat& image, const std::filesystem::path& haar_path)
{
	try
	{
		ImageContent content{image, haar_path};
		return image(content.square_crop()).clone();
	}
	catch (cv::Exception& e)
	{
		Log(LOG_WARNING, "OpenCV Exception caught during square cropping: {}", e.msg.c_str());
	}
	catch (std::exception& e)
	{
		Log(LOG_WARNING, "Exception caught during square cropping: {}", e.what());
	}
	catch (...)
	{
		Log(LOG_WARNING, "Unknown exception caught during square cropping.");
	}

	// fall back: crop at center
	auto window_length = std::min(image.cols, image.rows);
	cv::Rect roi = (image.cols > image.rows) ?
		cv::Rect{image.cols / 2 - window_length/2, 0, window_length, window_length} :
		cv::Rect{0, image.rows / 2 - window_length/2, window_length, window_length} ;
	return image(roi).clone();
}

} // end of local namespace

/// \brief Construct a RenditionCache object from a file
/// \param  dir     Path to the rendition directory, which contains all renditions of the same file
/// \param  master  Path to the master rendition (i.e. the real file)
RenditionCache::RenditionCache(std::filesystem::path dir, std::filesystem::path master) :
	m_dir{std::move(dir)}, m_master{std::move(master)}
{
//	assert(m_dir.is_absolute());
	assert(!m_dir.empty());
	assert(!is_directory(m_master));
}

std::optional<RenditionCache::Rendition> RenditionCache::rendition(
	const std::string& rendition,
	const RenditionSetting& cfg,
	const std::filesystem::path& haar_path,
	std::error_code& ec
) const
{
	auto& setting = cfg.find(rendition);
	if (!setting)
	{
		ec = make_error_code(std::errc::no_such_file_or_directory);
		return std::nullopt;
	}

	create_directories(m_dir, ec);
	auto rend_path = m_dir / setting.filename;

	// generate the rendition if it doesn't exist
	if (!exists(rend_path, ec) || ec || file_size(rend_path, ec) == 0 || ec)
		generate_image_rendition(setting, rend_path, haar_path, ec);

	if (!exists(rend_path, ec) || ec || file_size(rend_path, ec) == 0 || ec)
	{
		Log(LOG_WARNING,
			"Rendition '{}' for file {} cannot be generated. Using master rendition instead.",
			setting.filename, m_master
		);
		rend_path = m_master;
	}

	return Rendition{setting, rend_path};
}

void RenditionCache::generate_image_rendition(
	const ImageRenditionSetting& cfg, const std::filesystem::path& dest,
	const fs::path& haar_path, std::error_code& ec
) const
{
	if (auto jpeg = cv::imread(m_master, cv::IMREAD_ANYCOLOR); !jpeg.empty())
	{
		auto h_ratio = cfg.dim.width() / static_cast<double>(jpeg.cols);
		auto v_ratio = cfg.dim.height() / static_cast<double>(jpeg.rows);
		auto ratio = cfg.square_crop ? std::max(h_ratio, v_ratio) : std::min(h_ratio, v_ratio);

		cv::Mat out;
		if (ratio < 1.0)
			cv::resize(jpeg, out, {}, ratio, ratio, cv::INTER_CUBIC);
		else
			out = jpeg;

		if (cfg.square_crop)
			out = square_crop(out, haar_path);

		// Select different quality flag according to file extension.
		const auto ext = dest.extension();
		const auto quality_flag =
			(ext == ".jpeg" || ext == ".jpg") ? cv::IMWRITE_JPEG_QUALITY :
			(ext == ".avif") ? cv::IMWRITE_AVIF_QUALITY :
			(ext == ".webp") ? cv::IMWRITE_WEBP_QUALITY : 0;

		// TODO: check errors
		if (cv::imwrite(dest, out, {
			quality_flag, cfg.quality,
			cv::IMWRITE_JPEG_PROGRESSIVE, 1,
			cv::IMWRITE_JPEG_OPTIMIZE, 1
		}))
			Log(LOG_INFO,
				"Generated rendition {}x{} (actual {}x{}) for file {} at {}",
				cfg.dim.width(), cfg.dim.height(),
				out.cols, out.rows,
				m_master.filename(), dest
			);
		else
			Log(LOG_ERR,
				"Cannot generate rendition {}x{} for file {} at {}",
				cfg.dim.width(), cfg.dim.height(),
				m_master.filename(), dest
			);
	}
	else
	{
		Log(LOG_WARNING, "Cannot open file {} to generate renditions", m_master);
	}
}

} // end of namespace hrb
