/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/7/18.
//

#pragma once

#include "WebResources.hh"
#include "StorageSync.hh"

#include "util/DirectoryWatcher.hh"

#include <boost/asio/io_context.hpp>
#include <boost/asio/ssl/context.hpp>

#include <system_error>
#include <functional>

namespace sanali {class Connection;}
namespace hrb {

class Configuration;
class Password;

/// The main application logic of hearty rabbit.
/// This is the class that handles HTTP requests from and produce response to clients. The unit test
/// this class calls handle_https().
class Server
{
public:
	explicit Server(const Configuration& cfg);
	void listen();

	void run()  {m_ioc.run();}
	void stop() {m_ioc.stop();}
	void full_sync() {m_sync.start();}

	// Administrative commands and configurations
	void add_user(std::string username, const Password& password, std::error_code& ec);

	void drop_privileges() const;

private:
	const Configuration&        m_cfg;
	boost::asio::ssl::context   m_ssl{boost::asio::ssl::context::sslv23};
	boost::asio::io_context     m_ioc;
	WebResources                m_lib;

	boost::asio::strand<boost::asio::any_io_executor>   m_sync_strand;
	StorageSync                 m_sync;
	DirectoryWatcher            m_watcher;
};

} // end of namespace
