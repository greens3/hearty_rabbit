/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 11/5/23.
//

#pragma once

#include "util/DirectoryEvents.hh"

#include <deque>
#include <memory>

namespace sanali {class Connection;}

namespace hrb {

class Configuration;

class StorageSync : public DirectoryEvents
{
public:
	explicit StorageSync(const Configuration& config, boost::asio::any_io_executor ioc) :
		m_config{config}, m_strand{std::move(ioc)}
	{
	}

	// All paths are relative path to the root of the watcher.
	void on_directory_added(std::filesystem::path&& dir) override;
	void on_file_added(std::filesystem::path&& path) override;
	void on_file_write_close(std::filesystem::path&& path) override;
	void on_file_removed(std::filesystem::path&& path) override;
	void on_directory_removed(std::filesystem::path&& dir) override;

	auto& get_executor() const {return m_strand;}
	void start();
	void add(std::filesystem::path path);

private:
	void sync_dir(std::shared_ptr<sanali::Connection> redis, std::filesystem::recursive_directory_iterator dir);
	void sync_file(std::shared_ptr<sanali::Connection> redis);
	void kick_off(std::shared_ptr<sanali::Connection> redis = {});

private:
	const Configuration&            m_config;
	boost::asio::any_io_executor    m_strand;

	std::deque<std::filesystem::path>   m_to_sync;
};

} // end of namespace hrb
