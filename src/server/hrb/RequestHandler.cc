/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 8/2/23.
//

#include "RequestHandler.hh"
#include "FileStorage.hh"

#include "crypto/UserDatabase.hh"
#include "hrb/DirectoryListing.hh"
#include "hrb/WebResources.hh"
#include "util/Log.hh"
#include "util/Configuration.hh"
#include "util/StringFields.hh"
#include "util/Cookie.hh"
#include "util/Error.hh"
#include "util/Timestamp.hh"

namespace hrb {

void RequestHandler::on_header(
	const RequestHeader& header,
	boost::asio::any_completion_handler<void(std::error_code)> callback
)
{
	Log(LOG_INFO, "{}: {}", header.method_string(), header.target());
	m_intent = RequestIntent{header};

	auto session = session_id(header);
	if (!session)
	{
		return callback(std::error_code{});
	}

	// No need to verify session again if we already verified the cookie before in
	// earlier request of the same TCP connection.
	if (m_auth.valid() && *session == m_auth.session())
	{
		Log(LOG_DEBUG, "User '{}' already authenticated before. Proceeding request.", m_auth.username());
		return callback(std::error_code{});
	}

	Log(LOG_DEBUG, "Verifying user with session ID in cookie");
	UserDatabase{m_cfg, *m_redis}.verify_session(
		*session,
		[this, callback=std::move(callback), session=*session](UserID&& auth, std::error_code ec) mutable
		{
			if (auth.valid() && !ec)
				Log(LOG_INFO, "User '{}' verified.", auth.username());
			else if (ec == std::errc::permission_denied)
				Log(LOG_WARNING, "Invalid session ID sent from client: {}", session);
			else
				Log(LOG_WARNING, "User verification error: {} ({})].", ec, ec.message());

			m_auth = std::move(auth);
			return callback(std::error_code{});
		}
	);
}

void RequestHandler::handle_login(const StringRequest& req, EmptyResponseSender send)
{
	assert(req.method() == http::verb::post);

	auto&& body = req.body();
	if (req[http::field::content_type] != "application/x-www-form-urlencoded")
		return send(http::response<http::empty_body>{http::status::bad_request, req.version()});

	auto [username, password] = urlform.find(body, "username", "password");
	UserDatabase{m_cfg, *m_redis}.verify_user(
		std::string{username}, Password{password},
		[version=req.version(), send=std::move(send), this](auto&& session, std::error_code ec) mutable
		{
			// Regardless of success or not, need to store the login result.
			m_auth = std::forward<decltype(session)>(session);

			// For some reason, login uses 204 to indicate success
			http::response<http::empty_body> res{error_to_status(ec, http::status::no_content), version};
			if (!ec)
				res.set(http::field::set_cookie, m_auth.set_cookie(m_cfg.session_length()).str());

			res.set(http::field::cache_control, "no-cache, no-store, must-revalidate");
			send(std::move(res));
		}
	);
}

void RequestHandler::handle_logout(const EmptyRequest& req, EmptyResponseSender send)
{
	if (!m_auth.valid())
		return send(http::response<http::empty_body>{http::status::bad_request, req.version()});

	assert(m_auth.session());
	UserDatabase{m_cfg, *m_redis}.destroy_session(
		*m_auth.session(),
		[version=req.version(), send=std::move(send), this](auto ec) mutable
		{
			m_auth = UserID{};

			http::response<http::empty_body> res{error_to_status(ec), version};
			res.set(http::field::set_cookie, m_auth.set_cookie().str());
			res.set(http::field::cache_control, "no-cache, no-store, must-revalidate");
			send(std::move(res));
		}
	);
}

void RequestHandler::finalize_response_before_send(const std::optional<SessionID>& session, ResponseHeader& response)
{
	// Actual session ID (m_actual_user) is different from the session ID in request.
	// There should be two cases: 1) session is just renewed or, 2) user has logged out.
	if (session && *session != m_auth.session())
	{
		Log(LOG_INFO, "Session ID updated for user '{}'", m_auth.username());
		response.set(http::field::set_cookie, m_auth.set_cookie(m_cfg.session_length()).str());
	}
}

namespace {
bool has_permission(const UserID& auth, std::filesystem::perms perm)
{
	using enum std::filesystem::perms;
	return auth.valid() || (perm & others_read) != none;
}

void filter_directory_listing(DirectoryListing& dir, const UserID& auth)
{
	for (auto it = dir.begin(); it != dir.end();)
	{
		if (has_permission(auth, it->second.perms()))
		{
			// For security reason, we don't want to send the permission of the file to the client.
			it->second.set_perms(std::filesystem::perms::none);
			++it;
		}

		// Do not include files that the user is not permitted to read in the listing.
		else
			dir.erase(it++);
	}
}
} // end of local namespace

void RequestHandler::handle_view(const EmptyRequest& req, SplitBufferResponseSender&& send)
{
	FileStorage{m_cfg, *m_redis}.get_listing(
		m_intent.path(),
		[send = std::move(send), version = req.version(), this]
			(DirectoryListing&& dir, std::error_code ec) mutable
		{
			assert(send);
			filter_directory_listing(dir, m_auth);
			auto json = dir.to_json();
			json.emplace("username", m_auth.username());
			return send(m_lib.inject(error_to_status(ec), json.dump(), {}, version));
		}
	);
}

void RequestHandler::handle_list(const EmptyRequest& req, StringResponseSender&& send)
{
	FileStorage{m_cfg, *m_redis}.get_listing(
		m_intent.path(),
		[send=std::move(send), version=req.version(), this](DirectoryListing&& dir, std::error_code ec) mutable
		{
			assert(send);
			filter_directory_listing(dir, m_auth);

			auto json = dir.to_json();
			json.emplace("username", m_auth.username());

			http::response<http::string_body> res{
				std::piecewise_construct,
				std::make_tuple(json.dump()),
				std::make_tuple(error_to_status(ec), version)
			};
			res.set(http::field::content_type, "application/json");
			return send(std::move(res));
		}
	);
}


template <typename Response>
void RequestHandler::set_cache_control(const DirectoryEntry& dir_entry, Response& response)
{
	response.set(http::field::cache_control, "private, max-age=0, must-revalidate");
	response.set(http::field::last_modified, fmt::to_string(HttpTimePoint{dir_entry.modified()}));
}

void RequestHandler::handle_lookup(const EmptyRequest& req, LookupSender send)
{
	FileStorage{m_cfg, *m_redis}.get_entry(
		m_intent.path(), m_intent.rendition(),
		[
			etag = std::string{req[http::field::if_none_match]},
			send = std::move(send), version = req.version(), this
		](auto&& rendition, auto&& dir_entry, std::error_code ec) mutable
		{
			assert(send);
			auto logical_path = m_intent.path();

			if (dir_entry.is_directory())
			{
				http::response<http::empty_body> res{http::status::moved_permanently, version};
				res.set(http::field::location, RequestIntent{m_intent.action(), logical_path + "/"}.target());
				return send(std::move(res));
			}

			if (ec)
			{
				Log(
					LOG_WARNING, "Lookup error for {} [rendition: {}] [error: {} ({})]",
					logical_path, rendition, ec, ec.message()
				);
				return send(http::response<http::empty_body>{error_to_status(ec), version});
			}

			// Send 404 Not-found if the user has no permission to read the file.
			// Not using 403 because it will disclose that the file exists.
			if (!has_permission(m_auth, dir_entry.perms()))
				return send(http::response<http::empty_body>{http::status::not_found, version});

			// Meta-data request is much simpler. Just convert the dir_entry to JSON.
			if (m_intent.action() == RequestIntent::Action::meta)
			{
				http::response<http::string_body> response{
					std::piecewise_construct,
					std::make_tuple(nlohmann::json(dir_entry).dump()),
					std::make_tuple(error_to_status(ec), version)
				};
				set_cache_control(dir_entry, response);
				response.set(http::field::content_type, "application/json");
				return send(std::move(response));
			}
			else if (m_intent.action() == RequestIntent::Action::content)
			{
				return send_file(
					rendition,
					std::filesystem::path{logical_path}.filename().generic_string(),
					dir_entry, etag, version, std::move(send)
				);
			}
			else
				return send(bad_request("invalid handler", version));
		}
	);
}

void RequestHandler::send_file(
	const std::filesystem::path& physical_file,
	const std::string& filename,
	const DirectoryEntry& dir_entry,
	const std::string& etag_from_request,
	unsigned version,
	LookupSender&& send
)
{
	// No need to send the file if the client already has the latest copy, i.e. their ETag is the
	// same as the file hash.
	auto& file_hash = dir_entry.hash();
	if (!etag_from_request.empty() && file_hash && etag_from_request == file_hash->to_quoted_hex())
	{
		http::response<http::empty_body> res{http::status::not_modified, version};
		set_cache_control(dir_entry, res);
		res.set(http::field::etag, etag_from_request);
		return send(std::move(res));
	}

	http::file_body::value_type file_body;
	boost::system::error_code bec;
	file_body.open(physical_file.c_str(), boost::beast::file_mode::scan, bec);
	if (bec)
	{
		Log(LOG_WARNING, "Cannot open file {}: {} ({})", physical_file, bec, bec.message());
		return send(http::response<http::empty_body>{error_to_status(bec), version});
	}

	http::response<http::file_body> response{
		std::piecewise_construct,
		std::make_tuple(std::move(file_body)),
		std::make_tuple(http::status::ok, version)
	};
	response.set(http::field::content_type, dir_entry.mime());
	response.content_length(response.body().size());
	response.set(http::field::content_disposition, fmt::format("inline; filename=\"{}\"", filename));

	if (dir_entry.hash().has_value())
		response.set(http::field::etag, dir_entry.hash()->to_quoted_hex());
	set_cache_control(dir_entry, response);

	return send(std::move(response));
}

void RequestHandler::handle_lib(const EmptyRequest& req, SplitBufferResponseSender&& send)
{
	assert(send);
	return send(m_lib.find_static(m_intent.path(), req[http::field::if_none_match], req.version()));
}

std::optional<SessionID> RequestHandler::session_id(const RequestHeader& header)
{
	return UserID::parse_cookie(Cookie{header[http::field::cookie]});
}

} // end of namespace hrb
