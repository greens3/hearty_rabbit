/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 11/5/23.
//

#include "StorageSync.hh"
#include "FileStorage.hh"

#include "util/Configuration.hh"
#include "util/Log.hh"

#include "Sanali.hh"

namespace hrb {

static const std::vector<std::string> renditions{"thumbnail.avif", "2048x2048.avif"};

void StorageSync::start()
{
	// Perform a full sync on the base path
	add(m_config.base_path());
}

void StorageSync::add(std::filesystem::path path)
{
	m_to_sync.push_back(std::move(path));
	if (m_to_sync.size() == 1)
		kick_off();
}

void StorageSync::kick_off(std::shared_ptr<sanali::Connection> redis)
{
	assert(!m_to_sync.empty());
	if (!redis)
		redis = m_config.connect_to_redis(m_strand);

	assert(redis);
	std::error_code ec;
	if (is_directory(m_to_sync.front(), ec) && !ec)
		sync_dir(std::move(redis), std::filesystem::recursive_directory_iterator{m_to_sync.front()});
	else if (!ec)
		sync_file(std::move(redis));
}

void StorageSync::on_directory_added(std::filesystem::path&& dir)
{
	Log(LOG_INFO, "Directory {} is added to storage.\n", dir);
	add(std::move(dir));
}

void StorageSync::on_file_added(std::filesystem::path&& path)
{
	Log(LOG_INFO, "File {} is added to storage\n", path);
	add(std::move(path));
}

void StorageSync::on_file_write_close(std::filesystem::path&& path)
{
	Log(LOG_INFO, "File {} is modified\n", path);
	add(std::move(path));
}

void StorageSync::on_file_removed(std::filesystem::path&& path)
{
	Log(LOG_INFO, "File {} is deleted\n", path);
	add(path.parent_path());
}

void StorageSync::on_directory_removed(std::filesystem::path&& dir)
{
	Log(LOG_INFO, "Directory {} is deleted\n", dir);
	add(dir.parent_path());
}

void StorageSync::sync_dir(std::shared_ptr<sanali::Connection> redis, std::filesystem::recursive_directory_iterator dir)
{
	assert(redis);
	assert(!m_to_sync.empty());
	assert(redis->get_executor() == m_strand);

	dispatch(m_strand, [this, redis, dir]
	{
		assert(!m_to_sync.empty());
		if (dir != std::filesystem::recursive_directory_iterator{})
		{
			auto next = [this, redis, dir](auto ec) mutable
			{
				assert(redis->inflight_count() > 0);

				++dir;
				sync_dir(std::move(redis), dir);
			};

			Log(LOG_DEBUG, "Generating rendition for {}", dir->path());
			FileStorage{m_config, *redis}.generate_renditions(dir->path(), renditions, std::move(next));
		}
		else
		{
			Log(LOG_INFO, "Completely generated all renditions at {}", m_to_sync.front());
			m_to_sync.pop_front();

			if (!m_to_sync.empty())
				kick_off(redis);
		}
	});
}

void StorageSync::sync_file(std::shared_ptr<sanali::Connection> redis)
{
	assert(redis);
	assert(!m_to_sync.empty());
	assert(redis->get_executor() == m_strand);

	dispatch(m_strand, [this, redis=std::move(redis)]()
	{
		assert(!m_to_sync.empty());

		auto next = [this, redis](auto ec) mutable
		{
			assert(!m_to_sync.empty());
			m_to_sync.pop_front();

			if (!m_to_sync.empty())
				kick_off(std::move(redis));
		};

		Log(LOG_DEBUG, "Generating rendition for {}", m_to_sync.front());
		FileStorage{m_config, *redis}.generate_renditions(m_to_sync.front(), renditions, std::move(next));
	});
}

} // end of namespace hrb
