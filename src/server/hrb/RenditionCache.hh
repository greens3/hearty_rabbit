/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 3/3/18.
//

#pragma once

#include "util/Size2D.hh"
#include "util/MMap.hh"

#include <filesystem>
#include <system_error>
#include <chrono>

namespace hrb {

class RenditionSetting;
class ImageRenditionSetting;
class UploadFile;

/// \brief  Rendition Cache layer of Hearty Rabbit
/// The _Rendition Cache_ is stores different rendition of a resource. When a user requests
/// a resource, Hearty Rabbit generates a _rendition_ of that resource and send it to the user.
/// A rendition of a resource is a smaller version of the resource but with the same content.
/// For images, renditions are copies of the original with reduced resolution.
///
/// By default, Hearty Rabbit defines two renditions: master and default. The _master_
/// rendition is the original file. The default rendition is the one to be used when the
/// user does not specify any. Other renditions, for example, the
/// "2048x2048.jpeg" rendition resizes the images into 2048x2048 pixels and compress them
/// with JPEG.
///
/// Each rendition has a name, which is specified by the user when they request a file. The
/// parameters used to generate renditions are defined in configuration.
///
/// All renditions of the same file will be put in the same directory. This class manages
/// that directory. Files in these rendition directories are named by the name of the rendition.
class RenditionCache
{
public:
	using time_point = std::chrono::system_clock::time_point;

public:
	RenditionCache() = default;
	RenditionCache(std::filesystem::path dir, std::filesystem::path master);

	// If the rendition does not exist, and it's a valid one, it will be generated dynamically
	struct Rendition
	{
		const ImageRenditionSetting&    setting;
		std::filesystem::path           result;
	};
	std::optional<Rendition> rendition(
		const std::string& rendition,
		const RenditionSetting& cfg,
		const std::filesystem::path& haar_path,
		std::error_code& ec
	) const;

private:
	void generate_image_rendition(
		const ImageRenditionSetting& cfg,
		const std::filesystem::path& dest,
		const std::filesystem::path& haar_path,
		std::error_code& ec
	) const;

private:
	std::filesystem::path   m_dir;			//!< The directory in file system that stores all renditions of the file
	std::filesystem::path   m_master;       //!< The master rendition of the file
};

} // end of namespace hrb
