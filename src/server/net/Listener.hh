/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/1/18.
//

#pragma once

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/asio/any_io_executor.hpp>

#include <optional>
#include <memory>
#include <functional>

namespace hrb {

class WebServerConfig;

// Accepts incoming connections and launches the sessions
template <typename RequestHandlerType>
class Listener : public std::enable_shared_from_this<Listener<RequestHandlerType>>
{
public:
	Listener(
		boost::asio::io_context &ioc,
		std::function<RequestHandlerType(boost::asio::any_io_executor)> context_factory,
		boost::asio::ssl::context *ssl_ctx,
		const WebServerConfig& cfg
	);

	void run();

private:
	void do_accept();
	void on_accept(boost::system::error_code ec, boost::asio::ip::tcp::socket socket);

private:
	boost::asio::io_context&        m_ioc;
	boost::asio::ip::tcp::acceptor  m_acceptor;
	boost::asio::ssl::context       *m_ssl_ctx{};

	std::function<RequestHandlerType(boost::asio::any_io_executor)> m_factory;
	const WebServerConfig&          m_cfg;

	// stats
	std::size_t m_session_count{};
};

} // end of hrb namespace
