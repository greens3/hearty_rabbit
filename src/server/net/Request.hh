/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/14/18.
//

#pragma once

#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/buffer_body.hpp>
#include <boost/beast/http/empty_body.hpp>
#include <boost/beast/http/file_body.hpp>
#include <boost/beast/http/parser.hpp>

#include <boost/asio/any_completion_handler.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <variant>

namespace hrb {

class SplitBuffers;
class UploadRequestBody;

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>

using EndPoint = boost::asio::ip::tcp::endpoint;

using RequestHeader  = http::header<true,  http::fields>;
using ResponseHeader = http::header<false, http::fields>;

using HeaderRequest = http::request<http::buffer_body>;
using StringRequest = http::request<http::string_body>;
using EmptyRequest  = http::request<http::empty_body>;
using UploadRequest = http::request<UploadRequestBody>;

using HeaderRequestParser 	= http::request_parser<http::buffer_body>;
using StringRequestParser 	= http::request_parser<http::string_body>;
using EmptyRequestParser 	= http::request_parser<http::empty_body>;
using UploadRequestParser   = http::request_parser<UploadRequestBody>;

http::response<http::string_body> bad_request(std::string_view why, unsigned version);

using EmptyResponseSender       = boost::asio::any_completion_handler<void(http::response<http::empty_body>&&)>;
using StringResponseSender      = boost::asio::any_completion_handler<void(http::response<http::string_body>&&)>;
using SplitBufferResponseSender = boost::asio::any_completion_handler<void(http::response<SplitBuffers>&&)>;
using FileResponseSender        = boost::asio::any_completion_handler<void(http::response<http::file_body>&&)>;

} // end of namespace
