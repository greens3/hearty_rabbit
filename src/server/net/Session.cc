/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/1/18.
//

#include "Session.hh"

#include "net/SplitBuffers.hh"
#include "util/Cookie.hh"
#include "util/Error.hh"
#include "util/Log.hh"

#include <boost/asio/dispatch.hpp>
#include <boost/beast/version.hpp>

#include "config.hh"

#include "Sanali.hh"

namespace hrb {

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>

template <typename RequestHandlerType>
Session<RequestHandlerType>::Session(
	RequestHandlerType handler,
	boost::asio::ip::tcp::socket socket,
	boost::asio::ssl::context&  ssl_ctx,
	std::size_t                 nth,
	std::size_t                 upload_size_limit
) :
	m_stream{std::move(socket), ssl_ctx},
	m_handler{std::move(handler)},
	m_nth_session{nth},
	m_upload_size_limit{upload_size_limit}
{
}

// Start the asynchronous operation
template <typename RequestHandlerType>
void Session<RequestHandlerType>::run()
{
	boost::asio::dispatch(m_stream.get_executor(), [self=this->shared_from_this()]{self->on_run();});
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::on_run()
{
	// Set the timeout.
	boost::beast::get_lowest_layer(m_stream).expires_after(std::chrono::seconds{30});

	// Perform the SSL handshake
	m_stream.async_handshake(
		boost::asio::ssl::stream_base::server,
		[self = this->shared_from_this()](auto ec){self->on_handshake(ec);}
	);
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::on_handshake(boost::system::error_code ec)
{
	if (ec)
		Log(LOG_WARNING, "handshake error: {}", ec);

	do_read();
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::do_read()
{
	// Destroy and re-construct the parser for a new HTTP transaction
	m_parser.emplace();

//	m_handler.emplace(m_factory());
	m_parser->body_limit(m_upload_size_limit);

	// Read the header of a request
	async_read_header(
		m_stream, m_buffer, *m_parser,
		[self=this->shared_from_this()](auto ec, auto bytes) {self->on_read_header(ec, bytes);}
	);
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::on_read_header(boost::system::error_code ec, std::size_t)
{
	if (ec)
		return handle_read_error(ec);
	// Get the HTTP header from the partially parsed request message from the parser.
	// The body of the request message has not parsed yet.
	auto&& header = m_parser->get();
	m_keep_alive = header.keep_alive();

	if (!validate_request(header))
		return;

	auto on_header = [self = this->shared_from_this(), this, method = header.method()](std::error_code ec)
	{
		init_request_body(method, ec);
		if (ec)
		{
			Log(
				LOG_WARNING,
				"Error occurs when initializing request parser: {} ({}). "
				"Request body will be ignored.",
				ec.message(), ec
			);
			assert(std::holds_alternative<EmptyRequestParser>(m_body));
		}

		// Call async_read() using the chosen parser to read and parse the request body.
		std::visit([this, self](auto&& parser)
		{
			async_read(m_stream, m_buffer, parser, [self](auto ec, auto bytes)
			{
				self->on_read(ec, bytes);
			});
		}, m_body);
	};


	m_handler.on_header(header, std::move(on_header));
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::on_read(boost::system::error_code ec, std::size_t)
{
	// This means they closed the connection
	if (ec)
		return handle_read_error(ec);
	else
	{
		std::visit([this, self=this->shared_from_this()](auto&& parser)
		{
			m_handler.on_body(
				parser.release(), [self](auto&& response)
				{
					self->send_response(std::forward<decltype(response)>(response));
				}
			);
		}, m_body);
	}
	m_nth_transaction++;
}

// This function produces an HTTP response for the given
// request. The type of the response object depends on the
// contents of the request, so the interface requires the
// caller to pass a generic lambda for receiving the response.
template <typename RequestHandlerType>
template<class Request>
bool Session<RequestHandlerType>::validate_request(const Request& req)
{
	boost::system::error_code ec;
	auto endpoint = m_stream.next_layer().socket().remote_endpoint(ec);
	if (ec)
		Log(LOG_WARNING, "remote_endpoint() error: {} {}", ec, ec.message());

	// Request path must be absolute and not contain "..".
	if (req.target().empty() ||
	    req.target()[0] != '/' ||
	    req.target().find("..") != boost::beast::string_view::npos)
	{
		send_response(bad_request("Illegal request-target", req.version()));
		return false;
	}
	return true;
}

template <typename RequestHandlerType>
template <class Response>
void Session<RequestHandlerType>::send_response(Response&& response)
{
	static const auto version =
		fmt::format("{} HeartyRabbit/{}", BOOST_BEAST_VERSION_STRING, hrb::constants::version());

	// The lifetime of the message has to extend
	// for the duration of the async operation, so
	// we use a shared_ptr to manage it.
	auto sp = std::make_shared<std::remove_reference_t<Response>>(std::forward<Response>(response));
	sp->set(http::field::server, version);
	sp->keep_alive(m_keep_alive);
	sp->prepare_payload();

	async_write(
		m_stream, *sp,
		[this, self=this->shared_from_this(), sp](auto&& ec, auto bytes)
		{
			// This means we should close the connection, usually because
			// the response indicated the "Connection: close" semantic.
			if (sp->need_eof())
				return do_close();

			// Read another request
			if (!ec)
				do_read();
		}
	);
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::handle_read_error(boost::system::error_code ec, std::string_view where)
{
	// This means they closed the connection
	if (ec == boost::beast::http::error::end_of_stream)
		return do_close();

	if (ec)
	{
		Log(LOG_WARNING, "Read error {} ({}) occurred at {}", ec, ec.message(), where);
		return send_response(http::response<http::empty_body>{http::status::bad_request, 11});
	}
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::do_close()
{
	// Send a TCP shutdown
	m_stream.async_shutdown(
		[self=this->shared_from_this()](auto ec){self->on_shutdown(ec);}
	);
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::on_shutdown(boost::system::error_code)
{
	// At this point the connection is closed gracefully
	Log(LOG_INFO, "Shutting down session with {} transactions", m_nth_transaction);
}

template <typename RequestHandlerType>
void Session<RequestHandlerType>::init_request_body(http::verb method, std::error_code& ec)
{
	using enum http::verb;
	if (!ec && method == post)
		m_body.emplace<StringRequestParser>(std::move(*m_parser));

	else if (!ec && method == put)
		m_body.emplace<UploadRequestParser>(std::move(*m_parser));
//		m_handler->prepare_upload(parser.get().body(), ec);

	else
		m_body.emplace<EmptyRequestParser>(std::move(*m_parser));
}

} // end of namespace

#include "hrb/RequestHandler.hh"
template class hrb::Session<hrb::RequestHandler>;
