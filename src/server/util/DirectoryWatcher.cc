/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/14/23.
//

#include "DirectoryWatcher.hh"
#include "DirectoryEvents.hh"

#include <boost/functional/hash.hpp>
#include <boost/asio/bind_executor.hpp>

#include <cassert>
#include <system_error>

namespace hrb {

#if __has_include (<sys/inotify.h>)
namespace inotify {

constexpr auto inotify_event_size = sizeof(inotify_event) + NAME_MAX + 1;
constexpr auto inotify_flags = IN_CREATE | IN_DELETE | IN_CLOSE_WRITE | IN_MOVED_TO | IN_MOVED_FROM;

DirectoryWatcher::DirectoryWatcher(
	const boost::asio::any_io_executor& ioc,
	const std::filesystem::path& root,
	DirectoryEvents& events
) :
	m_inotify{ioc, inotify_init1(IN_NONBLOCK)},
	m_buffer{1024 * inotify_event_size},
	m_events{events}
{
	assert(m_inotify.native_handle() > 0);

	auto wd = ::inotify_add_watch(m_inotify.native_handle(), root.c_str(), inotify_flags);
	if (wd == -1)
		throw std::system_error(errno, std::system_category());
	m_cond.emplace(wd, -1, root.generic_string());
}

DirectoryWatcher::~DirectoryWatcher()
{
	// Do not allow exception to escape from a destructor.
	try
	{
		stop();
	}
	catch (...)
	{
	}
}

void DirectoryWatcher::erase(int parent, const std::string& name)
{
	auto it = m_cond.get<FromParent>().find(std::make_tuple(parent, name));
	if (it != m_cond.get<FromParent>().end())
	{
		::inotify_rm_watch(m_inotify.native_handle(), it->watch_descriptor);
		m_cond.get<FromParent>().erase(it);
	}
}

void DirectoryWatcher::stop()
{
	clear();

	boost::system::error_code ec{};
	m_inotify.close(ec);
}

void DirectoryWatcher::start()
{
	m_inotify.async_read_some(
		boost::asio::buffer(m_buffer),
		[this](auto error, std::size_t bytes_read)
		{
			for (std::size_t i = 0; i < bytes_read;)
			{
				auto event = reinterpret_cast<inotify_event *>(&m_buffer[i]);
				process_event(*event);
				i += sizeof(inotify_event) + event->len;
			}

			// keep going
			if (!error && bytes_read > 0)
				start();
		}
	);
}

void DirectoryWatcher::process_event(const inotify_event& event) try
{
	if (event.len == 0)
		return;

	std::string event_name{event.name, ::strnlen(event.name, event.len)};
	auto current_dir = get(event.wd);
	auto new_path = current_dir / event_name;

	if (event.mask & IN_CREATE || event.mask & IN_MOVED_TO)
	{
		if (event.mask & IN_ISDIR)
		{
			insert(event.wd, current_dir, std::move(event_name));
			m_events.on_directory_added(std::move(new_path));
		}
		else
			m_events.on_file_added(std::move(new_path));
	}
	else if (event.mask & IN_DELETE || event.mask & IN_MOVED_FROM)
	{
		if (event.mask & IN_ISDIR)
		{
			erase(event.wd, event_name);
			m_events.on_directory_removed(std::move(new_path));
		}
		else
			m_events.on_file_removed(std::move(new_path));
	}
	else if (event.mask & IN_CLOSE_WRITE && !(event.mask & IN_ISDIR))
	{
		m_events.on_file_write_close(std::move(new_path));
	}
}
catch (std::exception&)
{
}

std::filesystem::path DirectoryWatcher::get(int self) const
{
	auto it = m_cond.get<ByDescriptor>().find(self);
	if (it == m_cond.get<ByDescriptor>().end())
		throw std::runtime_error(fmt::format("cannot find directory from watch descriptor {}", self));

	assert(it->watch_descriptor == self);
	return it->parent_descriptor == -1 ?
		std::filesystem::path{it->name} :
		this->get(it->parent_descriptor) / it->name;
}

void DirectoryWatcher::clear()
{
	for (auto& dir: m_cond)
		::inotify_rm_watch(m_inotify.native_handle(), dir.watch_descriptor);
	m_cond.clear();
}

void DirectoryWatcher::insert(
	int parent,
	const std::filesystem::path& parent_path,
	std::string name
)
{
	// inotify_add_watch() may fail if the newly created directory is already gone
	if (auto wd = ::inotify_add_watch(m_inotify.native_handle(), (parent_path / name).c_str(), inotify_flags);
		wd > -1)
		m_cond.emplace(wd, parent, std::move(name));
}

} // end of namespace inotify
#endif

} // end of namespace hrb
