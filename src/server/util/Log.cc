/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/8/18.
//

#include "Log.hh"

#ifndef __APPLE__
	#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
	#endif
#endif

#include <cstring>
#include <unistd.h>

#include <fmt/color.h>

namespace hrb::detail {

// By default log everything
std::atomic_int LogBase::m_priority{LOG_DEBUG};

bool is_stdout_tty()
{
	static const bool result = ::isatty(STDOUT_FILENO);
	return result;
}

auto priority_to_string(int priority)
{
	constexpr std::string_view names[] =
	{
		"<emerg>",
		"<alert>",
		"<crit>",
		"<error>",
		"<warning>",
		"<notice>",
		"<info>",
		"<debug>",
		""
	};
	constexpr std::size_t count = std::size(names);
	return names[priority < count ? priority : count-1];
}

auto priority_style(int priority)
{
	constexpr fmt::text_style styles[] =
	{
		fmt::fg(fmt::color::orange_red),
		fmt::fg(fmt::color::orange_red) | fmt::bg(fmt::color::dark_slate_gray),
		fmt::fg(fmt::color::orange_red),
		fmt::fg(fmt::color::orange_red),
		fmt::fg(fmt::color::yellow),
		{},
		{},
		fmt::fg(fmt::color::dark_gray),
		{}
	};
	constexpr std::size_t count = std::size(styles);
	return styles[priority < count ? priority : count-1];
}

void log_header(int priority, fmt::memory_buffer& line)
{
	using namespace std::chrono;
	auto now = system_clock::now();
	auto sub_seconds = time_point_cast<microseconds>(now) - time_point_cast<seconds>(now);

	fmt::format_to(
		std::back_inserter(line),
		"{:9} {:%b %d %H:%M:%S}.{:<6} ",
		fmt::styled(
			priority_to_string(priority),
			is_stdout_tty() ? priority_style(priority) : fmt::text_style{}
		),
		now,
		sub_seconds.count()
	);
}

void detail_log(int priority, fmt::memory_buffer&& line, std::size_t syslog_start_position)
{
	// Write to stdout if it is a terminal, otherwise use syslog()
	// See https://www.freedesktop.org/software/systemd/man/systemd.exec.html#StandardOutput=
	if (!is_stdout_tty())
	{
		// See the printf() man page for details about _precision_
		// https://man7.org/linux/man-pages/man3/printf.3.html
		::syslog(
			priority, "%.*s",
			static_cast<int>(line.size() - syslog_start_position),
			line.data() + syslog_start_position
		);
	}
	else
	{
		line.push_back('\n');
		::write(STDOUT_FILENO, line.data(), line.size());
	}
}

std::string_view gnu_base_name(const char* path)
{
#ifdef _GNU_SOURCE
	return ::basename(path);
#else
	// From https://stackoverflow.com/questions/5802191/use-gnu-versions-of-basename-and-dirname-in-c-source
	auto base = std::strrchr(path, '/');
	return base ? base+1 : path;
#endif
}

void format_hex_line(fmt::memory_buffer& buf, const char* line, std::size_t size)
{
	assert(size <= 16);
	for (std::size_t i = 0; i < 16; ++i)
	{
		if (i > 0 && i % 8 == 0)
			buf.push_back(' ');

		if (i < size)
			fmt::format_to(std::back_inserter(buf), "{:02x} ", static_cast<int>(line[i]));
		else
			fmt::format_to(std::back_inserter(buf), "   ");
	}
	buf.push_back('\t');
	for (std::size_t i = 0; i < 16; ++i)
	{
		if (i < size)
			fmt::format_to(std::back_inserter(buf), "{:c}", std::isprint(line[i]) ? line[i] : '.');
		else
			buf.push_back(' ');
	}
	buf.push_back('\n');
}

} // end of namespace hrb::detail

namespace hrb {

std::string format_hex(std::string_view str)
{
	fmt::memory_buffer buf;
	for (std::size_t i = 0; i + 16 < str.size(); i+= 16)
		detail::format_hex_line(buf, &str[i], std::min<std::size_t>(16, str.size()-i));

	if (auto remind = str.size() % 16; remind > 0)
		detail::format_hex_line(buf, &str[str.size()-remind], remind);
	return std::string{buf.data(), buf.size()};
}

} // end of namespace hrb
