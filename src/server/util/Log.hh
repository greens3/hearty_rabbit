/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/7/18.
//

#pragma once

#include <fmt/ostream.h>
#include <fmt/std.h>
#include <fmt/format.h>
#include <fmt/chrono.h>

#include <boost/system/error_code.hpp>
#include <boost/beast/core/string.hpp>

#include <syslog.h>
#include <system_error>
#include <source_location>
#include <chrono>

namespace hrb {

namespace detail {
void detail_log(int priority, fmt::memory_buffer&& line, std::size_t syslog_start_position);

std::string_view gnu_base_name(const char* path);

void log_header(int priority, fmt::memory_buffer& line);

template <typename... Args>
void log_impl(
	const std::source_location& location,
	int priority,
	fmt::format_string<Args...> format_string,
	Args && ... args
)
{
	fmt::memory_buffer buf;
	log_header(priority, buf);

	// If we use syslog(), we don't need to put the priority and timestamp
	// because syslog() will provide it.
	auto syslog_start_position = buf.size();

	fmt::format_to(std::back_inserter(buf), format_string, std::forward<Args>(args)...);
	fmt::format_to(
		std::back_inserter(buf),
		" ({}:{})",
		gnu_base_name(location.file_name()),        // only the filename part is needed
		location.line()
	);
	return detail::detail_log(priority, std::move(buf), syslog_start_position);
}

struct LogBase
{
	static void set_priority(int priority)
	{
		m_priority = priority;
	}

protected:
	static std::atomic_int m_priority;
};
} // end of namespace detail

// Credits: https://stackoverflow.com/questions/57547273/how-to-use-source-location-in-a-variadic-template-function

template <typename... Args>
struct Log : private detail::LogBase
{
	Log(
		int priority,
		fmt::format_string<Args...> format_string,
		Args && ... args,
		const std::source_location& location = std::source_location::current()
	)
	{
		if (priority <= m_priority)
			detail::log_impl(location, priority, format_string, std::forward<Args>(args)...);
	}
};

template <typename... Args>
Log(int, fmt::format_string<Args...>, Args && ...) -> Log<Args...>;

std::string format_hex(std::string_view str);

} // end of namespace

namespace fmt {

template <>
struct formatter<boost::system::error_code> : public formatter<std::error_code>
{
	template <typename FormatContext>
	auto format(const boost::system::error_code& ec, FormatContext& ctx)
	{
		return fmt::format_to(ctx.out(), "{}", std::error_code{ec});
	}
};

template <>
struct formatter<boost::core::string_view> : public formatter<std::string_view>
{
	template <typename FormatContext>
	auto format(const boost::core::string_view& s, FormatContext& ctx)
	{
		return fmt::format_to(ctx.out(), "{}", std::string_view{s.data(), s.size()});
	}
};

} // end of namespace fmt