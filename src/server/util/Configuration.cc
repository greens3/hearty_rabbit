/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/6/18.
//

#include "Configuration.hh"

#include "WebServerConfig.hh"
#include "config.hh"

#include <boost/program_options.hpp>
#include <boost/exception/info.hpp>

#include <cassert>
#include <fstream>

#include "Log.hh"

namespace po = boost::program_options;
namespace ip = boost::asio::ip;

namespace hrb {
namespace {

ip::tcp::endpoint parse_endpoint(const nlohmann::json& json)
{
	return {
		ip::make_address(json["address"].get<std::string>()),
		json["port"].get<unsigned short>()
	};
}

} // end of local namespace

Configuration::Configuration() : m_web_server{std::make_unique<WebServerConfig>()}
{
}
Configuration::~Configuration() = default;

Configuration::Configuration(int argc, const char* const* argv, const char* env) : Configuration{}
{
	assert(m_web_server);

	using namespace std::literals;
	m_desc.add_options()
		("help", "produce help message")
		("add-user", po::value<std::string>()->value_name("username"), "add a new user given a user name")
		(
			"cfg", po::value<std::string>()->default_value(
				env ? std::string{env} : std::string{hrb::constants::config_filename()}
			)->value_name("path"), "Configuration file. Use environment variable HEARTY_RABBIT_CONFIG to set default path."
		);

	if (argc > 0)
	{
		store(po::parse_command_line(argc, argv, m_desc), m_args);
		po::notify(m_args);

	}

	// no need for other options when --help is specified
	if (!help())
		load_config(
			m_args.count("cfg") > 0 ? m_args["cfg"].as<std::string>() : env
		);
}

Configuration::Configuration(const std::filesystem::path& file) : Configuration{}
{
	load_config(file);
}

void Configuration::usage(std::ostream& out) const
{
	out << m_desc;
}

void Configuration::load_config(const std::filesystem::path& config_file)
{
	assert(m_web_server);
	try
	{
		std::ifstream stream;
		stream.open(config_file, std::ios::in);
		if (!stream)
			BOOST_THROW_EXCEPTION(FileError() << ErrorCode({errno, std::system_category()}));

		// Ignore comments in config file.
		auto json = nlohmann::json::parse(stream, nullptr, true, true);
		using jptr = nlohmann::json::json_pointer;

		// Paths are relative to the configuration file
		m_cert_chain = (config_file.parent_path() / json.at(jptr{"/cert_chain"})).lexically_normal();
		m_private_key = (config_file.parent_path() / json.at(jptr{"/private_key"})).lexically_normal();
		m_root = weakly_canonical(config_file.parent_path() / json.at(jptr{"/web_root"}));
		m_base_path = weakly_canonical(config_file.parent_path() / json.at(jptr{"/base_path"}));
		m_blob_path = weakly_canonical(config_file.parent_path() / json.at(jptr{"/blob_path"}));
		m_haar_path = (
			config_file.parent_path() /
				json.value(jptr{"/haar_path"}, std::string{constants::haarcascades_path()})
		).lexically_normal();
		m_redis_prefix = json.value(jptr{"/redis_prefix"}, "");
		m_log_priority = parse_priority(json.value(jptr{"/log_priority"}, "warning"));

		m_web_server->set_server_name(json.at(jptr{"/server_name"}));
		m_thread_count = json.value(jptr{"/thread_count"}, m_thread_count);
		m_web_server->set_upload_limit(static_cast<std::size_t>(
			json.value(
				jptr{"/upload_limit_mb"},
				static_cast<double>(m_web_server->upload_limit()) / 1024 / 1024) * 1024 * 1024
		));
		if (json.find("rendition") != json.end())
		{
			load_rendition_config(json["rendition"]);
		}
		m_shadow = weakly_canonical(config_file.parent_path() / json.at(jptr{"/shadow_file"}));
		m_session_length = std::chrono::seconds{json.value(jptr{"/session_length_in_sec"}, 3600L)};

		m_user_id = json.value(jptr{"/uid"}, m_user_id);
		m_group_id = json.value(jptr{"/gid"}, m_group_id);
		if (m_user_id == 0 || m_group_id == 0)
			BOOST_THROW_EXCEPTION(InvalidUserOrGroup());

		m_web_server->set_listen_http(parse_endpoint(json.at(jptr{"/http"})));
		m_web_server->set_listen_https(parse_endpoint(json.at(jptr{"/https"})));

		if (auto redis = json.value(jptr{"/redis"}, nlohmann::json::object_t{}); !redis.empty())
		{
			m_redis_host = redis["host"];

			auto& port = redis["port"];
			m_redis_port = port.is_number() ? std::to_string(port.get<int>()) : port.get<std::string>();
		}
	}
	catch (nlohmann::json::exception& e)
	{
		throw;
	}
	catch (Exception& e)
	{
		e << Path{config_file};
		throw;
	}
	catch (std::exception& e)
	{
		throw boost::enable_error_info(e) << Path{config_file};
	}
}

void Configuration::load_rendition_config(const nlohmann::json& config)
{
	for (auto&& mime: config.items())
	{
		auto [setting, added] = m_rendition.try_emplace(mime.key(), RenditionSetting{});
		for (auto&& rend: mime.value().items())
		{
			if (rend.value().is_string())
				setting->second.alias(rend.key(), rend.value());
			else
			{
				auto width = rend.value().value("width", 0);
				auto height = rend.value().value("height", 0);
				auto quality = rend.value().value("quality", 70);
				auto square_crop = rend.value().value("square_crop", false);
				auto rend_mime = rend.value().value("mime", "");

				if (width > 0 && height > 0)
				{
					if (!setting->second.add(
						rend.key(), {width, height}, std::move(rend_mime), quality, square_crop
					))
					{

					}
				}
			}
		}

		if (setting->second.rendition_count() == 0)
			BOOST_THROW_EXCEPTION(FileError() << Message{"No rendition for mime type " + setting->first});
	}
}

std::string WebServerConfig::https_root() const
{
	using namespace std::literals;
	return "https://" + m_server_name
		+ (listen_https().port() == 443 ? ""s : (":"s + std::to_string(listen_https().port())));
}

void Configuration::change_listen_ports(std::uint16_t https, std::uint16_t http)
{
	auto listen_https = m_web_server->listen_https();
	auto listen_http  = m_web_server->listen_http();
	listen_https.port(https);
	listen_http.port(http);
	m_web_server->set_listen_https(listen_https);
	m_web_server->set_listen_http(listen_http);
}

const RenditionSetting* Configuration::renditions(const std::string& mime) const
{
	auto it = m_rendition.find(mime);
	return it != m_rendition.end() ? &it->second : nullptr;
}

int Configuration::parse_priority(const std::string& text)
{
	if (text == "error")
		return LOG_ERR;
	else if (text == "critical")
		return LOG_CRIT;
	else if (text == "notice")
		return LOG_NOTICE;
	else if (text == "info")
		return LOG_INFO;
	else if (text == "debug")
		return LOG_DEBUG;
	else
		return LOG_WARNING;
}

std::shared_ptr<sanali::Connection> Configuration::connect_to_redis(boost::asio::any_io_executor ioc) const
{
	auto redis = sanali::connect(std::move(ioc), redis_host(), redis_port());
	redis->set_logger([](const std::string& msg)
	{
		Log(LOG_WARNING, "Error from Sanali: {}", msg);
	});
	return redis;
}

} // end of namespace
