/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/14/23.
//

#pragma once

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>

#include <boost/asio/any_io_executor.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/strand.hpp>

#include <compare>
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

#if __has_include (<sys/inotify.h>)
#include <sys/inotify.h>
#endif

namespace hrb {

class DirectoryEvents;

namespace bmi = boost::multi_index;

#if __has_include (<sys/inotify.h>)
inline namespace inotify {

class DirectoryWatcher
{
public:
	explicit DirectoryWatcher(
		const boost::asio::any_io_executor& ioc,
		const std::filesystem::path& root,
		DirectoryEvents& events
	);
	DirectoryWatcher(const DirectoryWatcher&) = delete;
	DirectoryWatcher& operator=(const DirectoryWatcher&) = delete;

	~DirectoryWatcher();

	void start();
	void stop();

	[[nodiscard]] auto size() const
	{
		return m_cond.size();
	}

private:
	void insert(int parent, const std::filesystem::path& parent_path, std::string name);

	void erase(int parent, const std::string& name);

	void clear();

	[[nodiscard]] std::filesystem::path get(int self) const;

	// The design of the inotify data structure is based on:
	// https://gist.github.com/pkrnjevic/6016356
	struct DirectoryEntry
	{
		// The inotify watch descriptor for this directory. It is returned by
		// inotify_add_watch(). It is also used as an identifier of this directory
		// when referred by its children.
		int watch_descriptor{-1};

		// The watch_descriptor of the parent directory. It is used to look up
		// it's parent up to the base (i.e. root of the watched tree). The root
		// directory will parent_descriptor=-1.
		int parent_descriptor{-1};

		// The name of this directory.
		std::string name;
	};

private:
	void process_event(const inotify_event& event);

	// tags
	struct ByDescriptor	{};
	struct FromParent {};

	// Can't use multi_index without using.
	using EntryContainer = boost::multi_index_container<
		DirectoryEntry,
		bmi::indexed_by<

			// Support lookup by descriptor
			bmi::hashed_unique<
				bmi::tag<ByDescriptor>,
				bmi::member<DirectoryEntry, int, &DirectoryEntry::watch_descriptor>
			>,

			// Support lookup by both parent descriptor and name
			bmi::hashed_unique<
				bmi::tag<FromParent>,
				bmi::composite_key<
					DirectoryEntry,
					bmi::member<DirectoryEntry, int, &DirectoryEntry::parent_descriptor>,
					bmi::member<DirectoryEntry, std::string, &DirectoryEntry::name>
				>
			>
		>
	>;
	EntryContainer m_cond;

	boost::asio::posix::stream_descriptor m_inotify;

	std::vector<std::byte> m_buffer;

	DirectoryEvents& m_events;
};

} // end of namespace inotify
#else

inline namespace mock {

class DirectoryWatcher
{
public:
	explicit DirectoryWatcher(
		const boost::asio::any_io_executor&,
		const std::filesystem::path&,
		DirectoryEvents&
	) {}
	DirectoryWatcher(const DirectoryWatcher&) = delete;
	DirectoryWatcher& operator=(const DirectoryWatcher&) = delete;

	void start() {}
	void stop() {}

	[[nodiscard]] std::size_t size() const
	{
		return 0;
	}
};
} // end of namespace macos

#endif

} // end of namespace hrb