//
// Created by root on 12/15/23.
//

#pragma once

#include <filesystem>

namespace hrb {

class DirectoryEvents
{
public:
	virtual ~DirectoryEvents() = default;

	// All paths are relative path to the root of the watcher.

	/// Called when a directory is created in the watched path, or an existing directory
	/// was moved to the watched path.
	virtual void on_directory_added(std::filesystem::path&& dir) = 0;

	/// Called when a file is created in the watched path, or an existing file
	/// was moved to the watched path.
	virtual void on_file_added(std::filesystem::path&& path) = 0;

	virtual void on_file_write_close(std::filesystem::path&& path) = 0;

	/// Called when a directory is deleted in the watched path, or it is moved from the
	/// watched path to somewhere else.
	virtual void on_file_removed(std::filesystem::path&& path) = 0;

	virtual void on_directory_removed(std::filesystem::path&& dir) = 0;
};

} // end of namespace
