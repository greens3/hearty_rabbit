/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 10/11/23.
//

#include "RenditionSetting.hh"

namespace hrb {

const ImageRenditionSetting& RenditionSetting::lookup_alias(const std::string& alias) const
{
	if (auto ait = m_alias.find(alias); ait != m_alias.end())
		if (auto it = m_renditions.find(ait->second); it != m_renditions.end())
			return *it;
	return not_found();
}

const ImageRenditionSetting& RenditionSetting::find(const std::string& name) const
{
	if (auto it = m_renditions.find(name); it != m_renditions.end())
		return *it;

	return lookup_alias(name);
}

bool RenditionSetting::valid(std::string_view rend) const
{
	return m_renditions.find(std::string{rend}) != m_renditions.end();
}

bool RenditionSetting::add(std::string name, Size2D dim, std::string mime, int quality, bool square_crop)
{
	if (name.empty())
		return false;

	// The first one should be the default
	if (!m_alias.contains({}))
		m_alias.insert_or_assign({}, name);

	return m_renditions.emplace(std::move(name), dim, quality, square_crop, std::move(mime)).second;
}

const ImageRenditionSetting& RenditionSetting::not_found()
{
	static const ImageRenditionSetting s{};
	return s;
}

} // end of namespace hrb
