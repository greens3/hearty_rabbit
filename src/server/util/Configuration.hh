/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/6/18.
//

#pragma once

#include "RenditionSetting.hh"
#include "Exception.hh"

#include "Sanali.hh"

#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/exception/error_info.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/any_io_executor.hpp>

#include <nlohmann/json.hpp>

#include <chrono>
#include <filesystem>
#include <memory>

#include <iosfwd>
#include <syslog.h>

namespace hrb {

class WebServerConfig;

/// \brief  Parsing command line options and configuration file
class Configuration
{
public:
	struct Error : virtual Exception {};
	struct FileError : virtual Error {};
	struct InvalidUserOrGroup: virtual Error {};
	using Path      = boost::error_info<struct tag_path,    std::filesystem::path>;
	using Message   = boost::error_info<struct tag_message, std::string>;
	using Offset    = boost::error_info<struct tag_offset,  std::size_t>;
	using ErrorCode = boost::error_info<struct tag_error_code,  std::error_code>;

public:
	Configuration();
	Configuration(int argc, const char *const *argv, const char *env);
	explicit Configuration(const std::filesystem::path& file);
	~Configuration();

	auto& web_server() const {return *m_web_server;}

	std::string_view redis_host() const {return m_redis_host;}
	std::string_view redis_port() const {return m_redis_port;}
	void change_redis_host(std::string host)
	{
		m_redis_host = std::move(host);
	}
	void change_redis_port(std::string port)
	{
		m_redis_port = std::move(port);
	}

	auto& cert_chain() const {return m_cert_chain;}
	auto& private_key() const {return m_private_key;}
	auto& web_root() const {return m_root;}
	auto& base_path() const {return m_base_path;}
	auto& blob_path() const {return m_blob_path;}
	auto& haar_path() const {return m_haar_path;}
	auto& redis_prefix() const {return m_redis_prefix;}
	void redis_prefix(std::string val) {m_redis_prefix = std::move(val);}

	std::shared_ptr<sanali::Connection> connect_to_redis(boost::asio::any_io_executor ioc) const;

	std::size_t thread_count() const {return m_thread_count;}
	uid_t user_id() const {return m_user_id;}
	gid_t group_id() const {return m_group_id;}
	const RenditionSetting* renditions(const std::string& mime) const;
	std::chrono::seconds session_length() const {return m_session_length;}
	int log_priority() const {return m_log_priority;}

	auto& shadow_file() const {return m_shadow;}
	void shadow_file(std::filesystem::path shadow) {m_shadow = std::move(shadow);}

	bool help() const {return m_args.count("help") > 0;}

	template <typename AddUser>
	bool add_user(AddUser&& func) const
	{
		return m_args.count("add-user") > 0 && (func(m_args["add-user"].as<std::string>()), true);
	}

	void usage(std::ostream& out) const;

	// for unit tests
	void base_path(std::filesystem::path path) {m_base_path = std::move(path);}
	void blob_path(std::filesystem::path path) {m_blob_path = std::move(path);}
	void change_listen_ports(std::uint16_t https, std::uint16_t http);

private:
	void load_config(const std::filesystem::path& config_file);
	void load_rendition_config(const nlohmann::json& config);
	static int parse_priority(const std::string& text);

private:
	boost::program_options::options_description m_desc{"Allowed options"};
	boost::program_options::variables_map       m_args;

	std::unique_ptr<WebServerConfig> m_web_server;
	std::string m_redis_host{"localhost"};
	std::string m_redis_port{"6379"};

	std::filesystem::path m_cert_chain, m_private_key;
	std::filesystem::path m_root, m_base_path, m_blob_path, m_haar_path;
	std::string m_redis_prefix;
	std::size_t m_thread_count{1};

	std::unordered_map<std::string, RenditionSetting> m_rendition;
	uid_t m_user_id{65535};
	gid_t m_group_id{65535};

	int m_log_priority{LOG_WARNING};

	std::chrono::seconds m_session_length{3600};
	std::filesystem::path m_shadow;
};

} // end of namespace
