/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by root on 10/11/23.
//

#pragma once

#include "Size2D.hh"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>

#include <nlohmann/json.hpp>

#include <string>

namespace hrb {

/// Describe how to generate a rendition from a master image.
struct ImageRenditionSetting
{
	///	Filename of the generated rendition image file.
	std::string filename;

	/// Dimension (i.e. width and height) or the generated rendition image.
	/// The generated rendition image will always have the same aspect ratio as the original image, but
	/// resized down so that each of the dimension is smaller than or equal to dim.
	Size2D  dim;

	/// Quality setting when converting the input master image.
	/// For JPEG and AVIF, this is a number between 0-100. Larger value will give better quality.
	int     quality{70};

	///	If square_crop is true, the generated rendition will be cropped into a square.
	bool    square_crop{false};

	///	Mime type of the rendition image.
	/// This field basically chose the decoder of the image. If it is "image/avif", then the AVIF encoder
	/// will be used.
	std::string mime;

	explicit operator bool() const
	{
		return !filename.empty();
	}
};

class RenditionSetting
{
public:
	RenditionSetting() = default;

	const ImageRenditionSetting& find(const std::string& name) const;
	auto rendition_count() const {return m_renditions.size();}

	bool valid(std::string_view rend) const;

	bool add(std::string name, Size2D dim, std::string mime, int quality=70, bool square_crop=false);
	void alias(std::string rend, std::string real_rend)
	{
		m_alias.insert_or_assign(std::move(rend), std::move(real_rend));
	}

private:
	const ImageRenditionSetting& lookup_alias(const std::string& alias = "") const;
	static const ImageRenditionSetting& not_found();

private:
	boost::multi_index_container<
	    ImageRenditionSetting,
		boost::multi_index::indexed_by<
		    boost::multi_index::hashed_unique<
		        boost::multi_index::member<ImageRenditionSetting, std::string, &ImageRenditionSetting::filename>
			>
		>
	> m_renditions;

	std::unordered_map<std::string, std::string> m_alias;
};

} // end of namespace hrb
