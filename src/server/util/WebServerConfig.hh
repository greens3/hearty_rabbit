//
// Created by nestal on 11/28/23.
//

#pragma once

#include <boost/asio/ip/tcp.hpp>

#include <chrono>
#include <cstddef>
#include <string>

namespace hrb {

class WebServerConfig
{
public:
	WebServerConfig() = default;

	auto upload_limit() const {return m_upload_limit;}
	auto& set_upload_limit(std::size_t v) {m_upload_limit = v; return *this;}

	std::string https_root() const;
	auto& set_server_name(std::string s) {m_server_name = std::move(s); return *this;}
	auto& server_name() const {return m_server_name;}

	auto listen_http() const { return m_listen_http;}
	auto listen_https() const { return m_listen_https;}
	auto& set_listen_http(boost::asio::ip::tcp::endpoint p)  {m_listen_http  = std::move(p); return *this;}
	auto& set_listen_https(boost::asio::ip::tcp::endpoint p) {m_listen_https = std::move(p); return *this;}

private:
	boost::asio::ip::tcp::endpoint m_listen_http, m_listen_https;
	std::size_t m_upload_limit{10 * 1024 * 1024};
	std::string m_server_name;
};

} // end of namespace

