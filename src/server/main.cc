/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

#include "crypto/Password.hh"
#include "util/Configuration.hh"
#include "util/Exception.hh"
#include "util/Log.hh"
#include "image/ImageContent.hh"
#include "hrb/Server.hh"
#include "net/Session.hh"

#include "config.hh"

#include <boost/exception/diagnostic_information.hpp>
#include <openssl/evp.h>

#include <cstdlib>
#include <regex>
#include <thread>
#include <iostream>

namespace hrb {

void run(const Configuration& cfg)
{
	auto const threads = std::max(1UL, cfg.thread_count());

	Server server{cfg};
	server.listen();
	server.full_sync();

	// make sure we load the certificates and listen before dropping root privileges
	server.drop_privileges();

	// Run the I/O service on the requested number of threads
	std::vector<std::thread> v;
	v.reserve(threads - 1);
	for (auto i = threads - 1; i > 0; --i)
		v.emplace_back([&server]{server.run();});

	server.run();
}

void StartServer(const Configuration& cfg)
{
	if (cfg.add_user([&cfg](auto&& username)
	{
		std::cout << "Please input password of the new user " << username << ":\n";
		std::string password;
		if (std::getline(std::cin, password))
		{
			Server srv{cfg};
			std::error_code ec;
			srv.add_user(username, Password{std::string_view{password}}, ec);
			std::cout << "result = " << ec << " " << ec.message() << std::endl;
		}
	})) {return;}

	else
	{
		// Print starting log before setting priority, so that the starting log
		// will always be printed regardless of configuration.
		Log(LOG_NOTICE, "hearty_rabbit (version {}) starting", constants::version());
		detail::LogBase::set_priority(cfg.log_priority());

		run(cfg);
		return;
	}
}

} // end of namespace

int main(int argc, char *argv[])
{
	using namespace hrb;
	try
	{
		OpenSSL_add_all_digests();
		Configuration cfg{argc, argv, std::getenv("HEARTY_RABBIT_CONFIG")};
		if (cfg.help())
		{
			cfg.usage(std::cout);
			std::cout << "\n";
			return EXIT_SUCCESS;
		}

		// check if HAAR model path in configuration is valid
		ImageContent::check_models(cfg.haar_path());

		StartServer(cfg);
		return EXIT_SUCCESS;
	}
	catch (Exception& e)
	{
		Log(LOG_CRIT, "Uncaught boost::exception: {}", boost::diagnostic_information(e));
		return EXIT_FAILURE;
	}
	catch (std::exception& e)
	{
		Log(LOG_CRIT, "Uncaught std::exception: {}", e.what());
		return EXIT_FAILURE;
	}
	catch (...)
	{
		Log(LOG_CRIT, "Uncaught unknown exception");
		return EXIT_FAILURE;
	}
}
