/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 6/3/18.
//

#include "HRBClient.hh"

#include "GenericHTTPRequest.hh"
#include "util/Error.hh"

#include "hrb/RequestIntent.hh"
#include "hrb/DirectoryListing.hh"

#include <fmt/format.h>

namespace hrb {
namespace {
template <typename ResponseType>
std::error_code get_error_from_response(const ResponseType& response)
{
	// If server sends 301 redirect, that means the "file" we tried to download is
	// actually a directory.
	if (response.result() == http::status::moved_permanently)
	{
		if (auto location = response.at(http::field::location); !location.empty())
		{
			RequestIntent intent{location, http::verb::get};
			return make_error_code(
				intent.path().back() == '/' ? std::errc::is_a_directory : std::errc::not_a_directory
			);
		}
	}
	return status_to_error(response.result());
}

template <typename ResultType>
class JSONResponseHandler
{
public:
	using Comp = HRBClient::Callback<void(ResultType&&, std::error_code)>;

	explicit JSONResponseHandler(Comp comp) : m_comp{std::move(comp)}
	{
	}

	template <typename ResponseType>
	void operator()(std::error_code ec, ResponseType&& response)
	{
		assert(m_comp);

		ResultType result;
		auto overall_ec = (ec ? ec : get_error_from_response(response));
		if (!overall_ec)
			try
			{
				auto json = nlohmann::json::parse(response.body());
				json.get_to(result);
			}
			catch (nlohmann::json::parse_error& e)
			{
				overall_ec = Error::server_error;
			}

		return m_comp(std::move(result), overall_ec);
	}

private:
	Comp m_comp;
};

}

HRBClient::HRBClient(
	boost::asio::io_context& ioc,
	boost::asio::ssl::context& ctx,
	std::string_view host,
	std::string_view port
) :
	m_ioc{ioc}, m_ssl{ctx}, m_host{host}, m_port{port}
{
}

void HRBClient::login(std::string username, std::string_view password, Callback<void(std::error_code)> comp)
{
	RequestIntent intent{RequestIntent::login};

	// Launch the asynchronous operation
	auto req = std::make_shared<GenericHTTPRequest<http::string_body, http::string_body>>(m_ioc, m_ssl);
	req->init(m_host, m_port, intent.target(), intent.method());
	req->request().set(http::field::content_type, "application/x-www-form-urlencoded");
	req->request().body() = fmt::format("username={}&password={}", username, password);

	req->on_load(
		[this, username, comp=std::move(comp)](auto ec, auto&& response) mutable
		{
			if (response.result() == http::status::no_content)
				m_user = UserID{Cookie{response.at(http::field::set_cookie)}, username};
			else
				ec = make_error_code(std::errc::permission_denied);

			comp(ec);
		}
	);
	req->run();
}

void HRBClient::logout(Callback<void(std::error_code)> comp)
{
	RequestIntent intent{RequestIntent::logout};
	auto req = std::make_shared<GenericHTTPRequest<http::empty_body, http::empty_body>>(m_ioc, m_ssl);
	req->init(m_host, m_port, intent.target(), intent.method());

	// If the user is invalid, then we don't have to logout(). But we will try anyway.
	if (m_user.valid())
		req->request().set(http::field::cookie, m_user.cookie().str());

	req->on_load(
		[comp=std::move(comp)](auto ec, auto&& response) mutable
		{
			return comp(status_to_error(ec, response.result()));
		}
	);
	req->run();
}

void HRBClient::list(std::string directory_path, Callback<void(DirectoryListing&&, std::error_code)> comp)
{
	RequestIntent intent{RequestIntent::list, std::move(directory_path)};

	auto req = std::make_shared<GenericHTTPRequest<http::empty_body, http::string_body>>(m_ioc, m_ssl);
	req->init(m_host, m_port, intent.target(), intent.method());
	if (m_user.valid())
		req->request().set(http::field::cookie, m_user.cookie().str());

	req->on_load(JSONResponseHandler<DirectoryListing>{std::move(comp)});
	req->run();
}

void HRBClient::get_metadata(std::string path, Callback<void(DirectoryEntry&&, std::error_code)> comp)
{
	RequestIntent intent{RequestIntent::meta, std::move(path)};

	auto req = std::make_shared<GenericHTTPRequest<http::empty_body, http::string_body>>(m_ioc, m_ssl);
	req->init(m_host, m_port, intent.target(), intent.method());
	if (m_user.valid())
		req->request().set(http::field::cookie, m_user.cookie().str());
	req->on_load(JSONResponseHandler<DirectoryEntry>{std::move(comp)});
	req->run();
}

void HRBClient::download_file(
	std::string file_path, std::string rendition, const std::filesystem::path& dest,
    HRBClient::Callback<void(std::error_code)> comp
)
{
	RequestIntent intent{RequestIntent::content, std::move(file_path)};
	intent.rendition(std::move(rendition));

	auto req = std::make_shared<GenericHTTPRequest<http::empty_body, http::file_body>>(m_ioc, m_ssl);
	req->init(m_host, m_port, intent.target(), intent.method());

	// Open the destination file for writing.
	boost::system::error_code ec;
	req->response().body().open(dest.c_str(), boost::beast::file_mode::write, ec);
	if (ec)
		return comp(std::error_code{ec.value(), ec.category()});

	if (m_user.valid())
		req->request().set(http::field::cookie, m_user.cookie().str());
	req->on_load(
		[comp=std::move(comp), dest](auto ec, auto& response) mutable
		{
			if (!ec)
				ec = get_error_from_response(response);

			// If the server returns an error, the "downloaded" file will be empty and we should remove it.
			std::error_code size_ec;
			if (ec && file_size(dest, size_ec) == 0 && !size_ec)
				remove(dest, size_ec);
			return comp(ec);
		}
	);
	req->run();
}

} // end of namespace
