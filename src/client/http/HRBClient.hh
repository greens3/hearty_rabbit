/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 6/3/18.
//

#pragma once

#include "util/Cookie.hh"
#include "hrb/UserID.hh"

#include <boost/beast/http/verb.hpp>
#include <boost/beast/http/fields.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/any_completion_handler.hpp>

namespace hrb {

class DirectoryEntry;
class DirectoryListing;

/// C++ Client for Hearty Rabbit
class HRBClient
{
public:
	template <typename... Signatures>
	using Callback = boost::asio::any_completion_handler<Signatures...>;

public:
	HRBClient(boost::asio::io_context& ioc, boost::asio::ssl::context& ctx, std::string_view host, std::string_view port);

	void login(std::string username, std::string_view password, Callback<void(std::error_code)> comp);
	void logout(Callback<void(std::error_code)> comp);

	void list(std::string directory_path, Callback<void(DirectoryListing&&, std::error_code)> comp);
	void get_metadata(std::string directory_path, Callback<void(DirectoryEntry&&, std::error_code)> comp);
	void download_file(
		std::string file_path,
		std::string rendition,
		const std::filesystem::path& dest,
		Callback<void(std::error_code)> comp
	);

	auto& user() const {return m_user;}

private:
	// connection to the server
	boost::asio::io_context&    m_ioc;
	boost::asio::ssl::context&  m_ssl;

	// configurations
	std::string m_host;
	std::string m_port;

	// authenticated user
	UserID  m_user;

	// outstanding and pending requests
//	RequestScheduler m_outstanding;
};

}
