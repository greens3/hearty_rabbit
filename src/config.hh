/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/5/18.
//

#pragma once

#include <string_view>

namespace hrb::constants {

std::string_view config_filename();
std::string_view version();
std::string_view haarcascades_path();

} // end of namespace
