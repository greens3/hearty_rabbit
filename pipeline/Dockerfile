FROM debian:bookworm
MAINTAINER [Nestal Wan <me@nestal.net>]

RUN apt-get -y update && \
	apt-get -y install build-essential cmake libtcmalloc-minimal4 git curl zip unzip tar wget pkg-config ninja-build \
        libssl-dev libunwind-dev autoconf libtool libexif-dev libmagic-dev libavif-dev libpng-dev libjpeg-dev

# Install vcpkg
RUN git clone https://github.com/Microsoft/vcpkg.git && \
    ./vcpkg/bootstrap-vcpkg.sh -disableMetrics

# The opencv vcpkg is too large to be built by vcpkg. The gitlab runner will also run out of space.
RUN wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip \
	&& unzip opencv.zip \
	&& cd opencv-4.x \
	&& cmake -B build -S . \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/opt \
		-DBUILD_SHARED_LIBS=OFF \
		-DBUILD_LIST=core,imgproc,imgcodecs,objdetect \
		-DBUILD_opencv_legacy=OFF \
		-DBUILD_JASPER=OFF \
		-DWITH_WEBP=ON \
		-DWITH_AVIF=ON \
		-DBUILD_TBB=ON \
    	-DWITH_OPENMP=ON \
		-DWITH_FFMPEG=OFF \
		-DWITH_GSTREAMER=OFF \
		-DWITH_GTK=OFF \
		-DWITH_IPP=OFF \
		-DWITH_JASPER=OFF \
		-DWITH_OPENEXR=OFF \
    	-DWITH_PROTOBUF=OFF \
    	-DWITH_ADE=OFF \
    	-DBUILD_opencv_gapi=OFF \
        -DWITH_EIGEN=OFF \
	&& cmake --build build -- -j8 install \
	&& rm -rf build
