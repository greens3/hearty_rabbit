# HeartyRabbit

It's a web-base image album developed using [boost/beast](https://github.com/boostorg/beast) and
[Redis](https://redis.io/).

# Configuration

HeartyRabbit uses a JSON configuration file. By default, it is in `/etc/hearty_rabbit/hearty_rabbit.json`.
It can be specified by the `HEART_RABBIT_CONFIG` environment variable or using command line options `--cfg`.

The configuration file specifies the location of other files used by HeartyRabbit. If you use relative
paths for these configuration files, they will be relative to the location of the configuration file,
not the current directory of the HeartyRabbit process.

-   `web_root`: the directory that contains the HTML, CSS and javascript files served by HeartyRabbit
    as a web server. This directory is typically provided by the docker image, so there is little
    reason to change this. By default, it points to `../../lib`. Relative to
    `/etc/hearty_rabbit/hearty_rabbit.json`, it actually resolves to `/lib,` which is the actual
    directory inside the docker image that contains the [lib](lib) directory of the source.
    This directory must be readable by the user that runs HeartyRabbit (by default UID 65535).
-   `base_path`: the directory that store images. All files under this directory will be published by
    Hearty Rabbit. Only read access is required.
-   `blob_path`: the directory of the rendition cache. In Hearty Rabbit, a rendition is a down-sampled
    version of an image, for example, thumbnails. It may be in a different image format than the original
    one to save space. Hearty Rabbit needs both read and write access to this directory.
-   `cert_chain` and `private_key`: both files are used for SSL/TLS to support HTTPS. Normally
    they are provided by a certificate authority. For testing purpose the
    [HeartyRabbit source](etc/hearty_rabbit) include a self-signed certificate for
    automated testing. Please do not use them for production.
-   `redis`: Optional. IP address and port number of the Redis server. The default setting
	 is `127.0.0.1/6379`. 
-   `https`: Local IP address and port number HeartyRabbit listens to for HTTPS.
	 Normally it should be `0.0.0.0/443`. For testing we use port `4433` just in
	 case that HeartyRabbit is not run by root.
-   `http`: Local IP address and port number for HTTP. Normally it should be
	`0.0.0.0/80`. HeartyRabbit will redirect all requests to HTTPS.
-   `uid`/`gid`: Optional. Effective user ID and group ID of the HeartyRabbit
	process after it drops privileges. As mentioned above, `base_path` must be readable by this user.
    Currently only numerical values are accepted.
	User names and group names are not. Default value is `65535`. 

# Build Environment

In order maintain a stable build environment to product predictable software
builds, HeartyRabbit uses docker containers to build. The `bookworm-vcpkg-dev` image 
contain a full snapshot of the build environment. It can be found in
[gitlab](https://gitlab.com/greens3/hearty_rabbit/container_registry/5726645).

The docker `bookworm-vcpkg-dev` image can be built by the dockerfile
[`pipeline/Dockerfile`](pipeline/Dockerfile).

Run

	docker build -t bookworm-vcpkg-dev pipeline
	
in the source directory to build the docker image.

The `bookworm-vcpkg-dev` is a big image base on Debian 12 (Bookworm). The complete development
tool-chain, e.g. `build-essential`, `cmake` and `vcpkg` have been installed to
make sure HeartyRabbit can be built successfully.

## Details About the Build Environment

The [bookworm-vcpkg-dev](https://gitlab.com/greens3/hearty_rabbit/container_registry/5726645)
image is base on [Debian 12](https://hub.docker.com/_/debian/). See the [Dockerfile](pipeline/Dockerfile)
for a list of Debian package installed. In addition, [OpenCV](https://opencv.org/) is built by `Dockerfile` too.
The OpenCV version provided by Debian Bookworm does not support AVIF, so we need a custom-built version.

In addition, HeartyRabbit also requires [Boost libraries](http://boost.org),
[CMake](https://cmake.org), 
[nlohmann JSON](https://github.com/nlohmann/json), [Catch2](https://github.com/catchorg/Catch2) and [fmt](https://fmt.dev/)
to build. These are provided by `vcpkg`. 
