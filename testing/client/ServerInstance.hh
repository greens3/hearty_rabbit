/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 6/5/18.
//

#pragma once

#include "common/CommonFixture.hh"

#include <memory>
#include <string>

namespace hrb::test {

class ServerEnvironment;
class ServerInstance
{
public:
	ServerInstance();
	~ServerInstance();

	void run();

	static std::string listen_https_port();
	static ServerEnvironment& env();

private:
	struct Impl;
	std::unique_ptr<Impl> m_impl;
};

class ClientFixture : public CommonFixture
{
protected:
	static std::string file_hash(const std::string& filename);
};

} // end of namespace hrb
