/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
	This file is subject to the terms and conditions of the GNU General Public
	License.  See the file COPYING in the main directory of the hearty_rabbit
	distribution for more details.
*/

//
// Created by nestal on 6/5/18.
//

#include "ServerInstance.hh"
#include "common/ServerEnvironment.hh"

#include "hrb/Server.hh"
#include "crypto/Password.hh"
#include "util/FS.hh"
#include "util/WebServerConfig.hh"

#include <cassert>
#include <thread>

namespace hrb::test {

struct ServerInstance::Impl
{
	Impl()
	{
		env().copy_lena();
		std::error_code ec;
		Server{env().config()}.add_user("sumsum", Password{"bearbear"}, ec);
		m_srv.listen();
	}
	~Impl()
	{
		assert(m_thread.joinable());
		m_srv.stop();
		m_thread.join();
	}
	Server m_srv{env().config()};
	std::thread m_thread;
};

ServerInstance::ServerInstance() : m_impl{std::make_unique<Impl>()} {}
ServerInstance::~ServerInstance() = default;

void ServerInstance::run()
{
	m_impl->m_thread = std::thread([this]{m_impl->m_srv.run();});
}

std::string ServerInstance::listen_https_port()
{
	return std::to_string(env().config().web_server().listen_https().port());
}

ServerEnvironment& ServerInstance::env()
{
	static ServerEnvironment inst;
	return inst;
}

std::string ClientFixture::file_hash(const std::string& filename)
{
	return ServerEnvironment::file_hash(filename);
}

} // end of namespace hrb
