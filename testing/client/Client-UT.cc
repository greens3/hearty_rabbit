/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 6/3/18.
//

#include <catch2/catch_all.hpp>

#include "ServerInstance.hh"
#include "common/ServerEnvironment.hh"

#include "http/HRBClient.hh"
#include "hrb/DirectoryListing.hh"
#include "util/Error.hh"
#include "util/Escape.hh"

using namespace hrb;
using namespace std::chrono_literals;

TEST_CASE_METHOD(test::ClientFixture, "simple client test", "[normal]")
{
	boost::asio::ssl::context ctx{boost::asio::ssl::context::sslv23_client};

	HRBClient subject{ioc, ctx, "localhost", test::ServerInstance::listen_https_port()};

	SECTION("login correct")
	{
		bool checked{false};
		subject.login("sumsum", "bearbear", [&checked](auto err)
		{
			checked = true;
			REQUIRE_FALSE(err);
		});
		REQUIRE(RunFor(checked));
		REQUIRE(subject.user().valid());
		REQUIRE(subject.user().username() == "sumsum");

		subject.list("/", [&checked](auto dir, auto ec)
		{
			checked = true;
			REQUIRE_FALSE(ec);
			REQUIRE(dir.contains("mspaint.jpg"));

			auto [mime, mod, hash, created] = dir.at("mspaint.jpg").tuple();
			REQUIRE(mime == "image/jpeg");

			REQUIRE(dir.at("lena2.avif").mime() == "image/avif");
			REQUIRE(dir.at("lena3.webp").mime() == "image/webp");
			REQUIRE(dir.at("two_lena.avif").mime() == "image/avif");
			REQUIRE(dir.contains("black.jpg"));
		});
		REQUIRE(RunFor(checked));

		subject.get_metadata("/black.jpg", [&checked](auto dir_entry, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(dir_entry.mime() == "image/avif");
			REQUIRE(dir_entry.hash().has_value());
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// Downloading default rendition
		remove(std::filesystem::path{"/tmp/back.jpg"});
		subject.download_file("/black.jpg", "", "/tmp/back.jpg", [&checked](auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(std::filesystem::status("/tmp/back.jpg").type() == std::filesystem::file_type::regular);
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// Downloading master rendition
		remove(std::filesystem::path{"/tmp/back.jpg"});
		subject.download_file("/black.jpg", "master", "/tmp/back.jpg", [&checked](auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(std::filesystem::status("/tmp/back.jpg").type() == std::filesystem::file_type::regular);
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// Create a subdirectory and try to list it (without a slash at the end)
		create_directory(test::ServerInstance::env().config().base_path() / "new_folder");
		subject.list("/new_folder", [&checked](auto dir, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(dir.empty());
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// Download a directory. It won't work.
		subject.download_file("/new_folder", "master", "/tmp/folder", [&checked](auto ec)
		{
			REQUIRE(ec == std::errc::is_a_directory);
			REQUIRE_FALSE(exists(std::filesystem::status("/tmp/folder")));
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// Getting meta-data for a directory won't work either.
		subject.get_metadata("/new_folder", [&checked](auto entry, auto ec)
		{
			REQUIRE(ec == std::errc::is_a_directory);
			REQUIRE(entry == DirectoryEntry{});
			checked = true;
		});
		REQUIRE(RunFor(checked));

		// List a file works, unlike downloading a directory. The list of the parent directory of the
		// file will be returned. Also, the filename() of the listing will point to the requested file.
		subject.list("/black.jpg", [&checked](auto dir, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(dir.contains("black.jpg"));
			REQUIRE(dir.filename() == "black.jpg");
			REQUIRE(dir.logical_path() == "/");
			checked = true;
		});
		REQUIRE(RunFor(checked));

		subject.logout([&checked](auto ec)
		{
			REQUIRE_FALSE(ec);
			checked = true;
		});
		REQUIRE(RunFor(checked));
	}
	SECTION("Password incorrect when login")
	{
		bool checked{false};
		subject.login("yungyung", "bunny", [&checked](auto err)
		{
			checked = true;
			REQUIRE(err == std::errc::permission_denied);
		});
		REQUIRE(RunFor(checked));
	}
	SECTION("Login with invalid user")
	{
		bool checked{false};
		subject.login("bad_guy", "bad_password", [&checked](auto err)
		{
			checked = true;
			REQUIRE(err == std::errc::permission_denied);
		});
		REQUIRE(RunFor(checked));
		REQUIRE_FALSE(subject.user().valid());

		// Anonymous user cannot access non-world-readable files.
		remove(std::filesystem::path{"/tmp/private_lena.avif"});
		subject.download_file("/private_lena.avif", "", "/tmp/private_lena.avif", [&checked](auto ec)
		{
			REQUIRE(ec == std::errc::no_such_file_or_directory);
			REQUIRE(std::filesystem::status("/tmp/private_lena.avif").type() == std::filesystem::file_type::not_found);
			checked = true;
		});
		REQUIRE(RunFor(checked));
	}
}
