/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 8/4/23.
//

#include "hrb/RequestHandler.hh"

#include "crypto/Random.hh"
#include "hrb/WebResources.hh"
#include "hrb/FileStorage.hh"
#include "hrb/DirectoryListing.hh"
#include "util/Cookie.hh"

#include "common/ServerEnvironment.hh"
#include "CheckResource.hh"

#include <catch2/catch_test_macros.hpp>
#include <optional>

using namespace hrb;
using namespace hrb::test;
using namespace std::chrono_literals;

TEST_CASE_METHOD(ServerFixture, "normal case for RequestHandler", "[normal]")
{
	WebResources lib{config().web_root()};
	RequestHandler subject{lib, redis(), config()};

	bool checked{false};
	auto check_success = [&checked](std::error_code ec)
	{
		REQUIRE_FALSE(ec);
		checked = true;
	};
	std::optional<Cookie> cookie;
	auto check_status = [&checked, &cookie](
		http::status expected_status = http::status::ok
	)
	{
		return [&checked, expected_status, &cookie](auto&& response)
		{
			REQUIRE(response.result() == expected_status);
			REQUIRE(response.version() == 11);

			if (response.count(http::field::set_cookie) > 0)
				cookie.emplace(response.at(http::field::set_cookie));
			else
				cookie.reset();
			checked = true;
		};
	};
	auto check_file = [&checked](
		const std::filesystem::path& expected_path,
		http::status expected_status = http::status::ok
	)
	{
		return [&checked, expected_path, expected_status](auto&& response)
		{
			REQUIRE(response.result() == expected_status);
			REQUIRE(response.version() == 11);
			REQUIRE(check_resource_content(expected_path, response));
			checked = true;
		};
	};

	SECTION("lib requests does not require a valid login")
	{
		EmptyRequest req;
		req.target("/.lib/hearty_rabbit.css");
		req.method(http::verb::get);

		// Pass a reference wrapper of a checker so that it won't be copied.
		subject.on_header(req, check_success);
		REQUIRE(RunFor(checked));
		subject.on_body(req, check_file(config().web_root()/"static/hearty_rabbit.css"));
		REQUIRE(RunFor(checked));
	}
	SECTION("non-exist lib requests without valid login")
	{
		EmptyRequest req;
		req.target("/.lib/no_such_file_or_directory");
		req.method(http::verb::get);

		// Pass a reference wrapper of a checker so that it won't be copied.
		subject.on_header(req, check_success);
		REQUIRE(RunFor(checked));
		subject.on_body(req, check_status(http::status::not_found));
		REQUIRE(RunFor(checked));
	}
	SECTION("successful login request")
	{
		StringRequest req;
		req.target("/?auth=login");
		req.method(http::verb::post);

		// Login HTTP request object
		SECTION("login correct")
		{
			req.set(http::field::content_type, "application/x-www-form-urlencoded");

			subject.on_header(req, check_success);
			REQUIRE(RunFor(checked));

			req.body() = "username=sumsum&password=bearbear";
			subject.on_body(req, check_status(http::status::no_content));
			REQUIRE(RunFor(checked));
			REQUIRE(cookie.has_value());

			// Set the cookie we got from login to the next request.
			EmptyRequest next;
			next.method(http::verb::get);
			next.set(http::field::cookie, cookie->str());
			next.target("/");

			subject.on_header(next, check_success);
			REQUIRE(RunFor(checked));

			// The cookie will be verified, and we will have a valid user stored in RequestHandler.
			REQUIRE(subject.user().username() == "sumsum");

			subject.on_body(next, check_status());
			REQUIRE(RunFor(checked));

			// lib requests
			EmptyRequest css;
			css.target("/.lib/hearty_rabbit.css");
			css.method(http::verb::get);

			// Pass a reference wrapper of a checker so that it won't be copied.
			subject.on_header(css, check_success);
			REQUIRE(RunFor(checked));
			subject.on_body(css, check_file(config().web_root()/"static/hearty_rabbit.css"));
			REQUIRE(RunFor(checked));

			// Logout
			EmptyRequest logout;
			logout.target("/?auth=logout");
			logout.method(http::verb::get);
			logout.set(http::field::cookie, cookie->str());

			subject.on_header(logout, check_success);
			REQUIRE(RunFor(checked));

			subject.on_body(next, check_status());
			REQUIRE(RunFor(checked));
			REQUIRE(cookie.has_value());
			REQUIRE_FALSE(UserID::parse_cookie(*cookie).has_value());
		}
		SECTION("incorrect login")
		{
			req.set(http::field::content_type, "application/x-www-form-urlencoded");
			req.body() = "username=sumsum&password=rabbit";
			subject.on_header(req, check_success);
			REQUIRE(RunFor(checked));

			subject.on_body(req, check_status(http::status::forbidden));
			REQUIRE(RunFor(checked));
			REQUIRE(cookie == std::nullopt);
		}
		SECTION("incorrect content type")
		{
			req.method(http::verb::post);
			req.set(http::field::content_type, "text/plain");
			req.body() = "hello world!";

			subject.on_header(req, check_success);
			REQUIRE(RunFor(checked));

			subject.on_body(req, check_status(http::status::bad_request));
			REQUIRE(RunFor(checked));
			REQUIRE(cookie == std::nullopt);
		}
	}
	SECTION("invalid logout")
	{
		UserID invalid{insecure_random<SessionID>(), "invalid"};

		// Login HTTP request object
		RequestIntent intent{RequestIntent::logout};
		EmptyRequest req;
		req.target(intent.target());
		req.method(intent.method());
		req.set(http::field::cookie, invalid.cookie().str());

		subject.on_header(req, check_success);
		REQUIRE(RunFor(checked));

		subject.on_body(req, check_status(http::status::bad_request));
		REQUIRE(RunFor(checked));
		REQUIRE(cookie.has_value());

		UserID session{*cookie, "don't care"};
		REQUIRE_FALSE(session.valid());
	}
}
