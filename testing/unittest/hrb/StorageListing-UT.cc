/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 8/6/23.
//

#include <opencv2/imgcodecs.hpp>

#include "hrb/DirectoryListing.hh"
#include "hrb/FileStorage.hh"
#include "hrb/StorageSync.hh"
#include "util/Escape.hh"
#include "util/DirectoryWatcher.hh"

#include "common/TestImages.hh"
#include "common/ServerEnvironment.hh"

#include <catch2/catch_test_macros.hpp>

#include <thread>

using namespace hrb;
using namespace std::chrono_literals;

TEST_CASE_METHOD(test::ServerFixture, "subdirectory as DirectoryEntry", "[normal]")
{
	auto base_path = env().prepare_base_path();
	auto path = base_path/"subdir";
	create_directories(path);

	std::error_code ec;
	DirectoryEntry subject{path, ec};
	REQUIRE_FALSE(ec);
	REQUIRE(subject.mime() == "inode/directory; charset=binary");

	// Nothing changed
	REQUIRE_FALSE(subject.full_update(path, ec));
	REQUIRE_FALSE(ec);
	REQUIRE(subject.mime() == "inode/directory; charset=binary");
}

TEST_CASE_METHOD(test::ServerFixture, "directory and file meta data", "[normal]")
{
	auto base_path = env().prepare_base_path();
	auto path = base_path/"lena.png";

	// Constructor does not calculate hash
	std::error_code ec;
	DirectoryEntry subject{path, ec};
	REQUIRE_FALSE(ec);
	REQUIRE(subject.mime() == "image/png");
	REQUIRE_FALSE(subject.hash().has_value());

	// See if the mime will be updated by full_update()
	subject.set_mime("image/jpeg");

	// full_update() will calculate hash
	REQUIRE(subject.full_update(path, ec));
	REQUIRE_FALSE(ec);
	REQUIRE(subject.hash().has_value());
	REQUIRE(subject.hash()->to_hex() == test::ServerEnvironment::file_hash(path));
	REQUIRE(subject.mime() == "image/png");

	// Nothing to update
	REQUIRE_FALSE(subject.full_update(path, ec));
	REQUIRE_FALSE(ec);

	// Update the file
	std::this_thread::sleep_for(10ms);
	auto old_hash = *subject.hash();
	auto old_modified = subject.modified();

	cv::imwrite(path, test::random_lena(), {cv::IMWRITE_JPEG_QUALITY, 40});
	REQUIRE(subject.full_update(path, ec));
	REQUIRE(subject.hash().value() != old_hash);
	REQUIRE(subject.modified() > old_modified);

	// Change the modified time without changing the file
	std::this_thread::sleep_for(10ms);
	old_modified = subject.modified();
	old_hash = *subject.hash();
	last_write_time(path, std::chrono::file_clock::now());
	REQUIRE(subject.full_update(path, ec));
	REQUIRE(subject.hash().value() == old_hash);
	REQUIRE(subject.modified() > old_modified);

	// Reset file hash
	subject.set_hash(std::nullopt);
	REQUIRE_FALSE(subject.hash().has_value());
}

TEST_CASE("round-trip from/to json/msgpack", "[normal]")
{
	DirectoryListing subject;
	subject.set_logical_path("/a/long/way");

	auto created = std::chrono::system_clock::now();

	std::error_code ec;
	DirectoryListing::FileHash dummy{};
	subject.add("file1.jpeg", "image/jpeg", std::chrono::system_clock::now(), std::filesystem::perms::none, dummy);
	subject.add("file2.avif", "image/avif", std::chrono::system_clock::now()+10s, std::filesystem::perms::none, dummy, created);
	REQUIRE_FALSE(ec);

	auto json = subject.to_json();
	INFO("Listing JSON is " << json.dump());
	REQUIRE(json.is_object());

	auto round_trip = json.get<DirectoryListing>();
	REQUIRE(round_trip.logical_path() == "/a/long/way/");
	REQUIRE(round_trip.filename() == "");
//	REQUIRE(round_trip.username() == "sumyung");

	auto msgpack = round_trip.at("file2.avif").to_msgpack();
	auto file2 = DirectoryEntry::from_msgpack(msgpack);
	REQUIRE(file2 == subject.at("file2.avif"));
	REQUIRE(file2.created() == created);
}

TEST_CASE("opencv tests", "[normal]")
{
	REQUIRE_NOTHROW(cv::imwrite("lena-rand.avif", test::random_lena(), {cv::IMWRITE_AVIF_QUALITY, 40}));
}

TEST_CASE("msgpack round-trip for DirectoryEntry", "[normal]")
{
	std::error_code ec;
	DirectoryEntry subject{test::images()/"lena.png", ec};
	subject.set_modified(std::chrono::time_point_cast<std::chrono::milliseconds>(subject.modified()));
	REQUIRE_FALSE(ec);

	auto msgpack = subject.to_msgpack();
	auto copy = DirectoryEntry::from_msgpack(std::move(msgpack));
	REQUIRE(copy == subject);
}

TEST_CASE_METHOD(test::ServerFixture, "list directory in FileStorage", "[normal]")
{
	auto db = redis();
	FileStorage subject{config(), *db};

	bool checked{false};

	SECTION("empty subdirectory")
	{
		create_directory(env().config().base_path() / "empty_subdirectory");
		subject.get_listing(
			"/empty_subdirectory/", [&checked](auto dir, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(dir.empty());
				REQUIRE(dir.logical_path() == "/empty_subdirectory/");
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Directory name without ending slash character '/'
		subject.get_listing(
			"/empty_subdirectory", [&checked](auto dir, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(dir.empty());
				REQUIRE(dir.logical_path() == "/empty_subdirectory/");
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Directory name without some relative paths
		subject.get_listing(
			"/empty_subdirectory/../empty_subdirectory/.", [&checked](auto dir, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(dir.empty());
				REQUIRE(dir.logical_path() == "/empty_subdirectory/");
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("file exist")
	{
		subject.get_listing(
			"black.jpg", [&checked](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.filename() == "black.jpg");

				auto black = listing.find("black.jpg");
				REQUIRE(black != listing.end());
				REQUIRE(black->second.mime() == "image/jpeg");
				REQUIRE(listing.logical_path() == "/");

				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Make sure the previous cache entry is outdated
		std::this_thread::sleep_for(5ms);

		// Copy one more file to make the cache outdated.
		copy_file(test::images() / "white.jpg", config().base_path() / "something.jpg");

		subject.get_listing(
			"/", [&checked](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.contains("something.jpg"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Request for something.jpg and its hash will be available next time
		DirectoryEntry::FileHash something{};
		subject.get_entry(
			"/something.jpg", "", [&checked, &something, this](auto&& path, auto&& entry, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(entry.hash().has_value());
				REQUIRE(file_hash(env().config().base_path() / "something.jpg") == entry.hash()->to_hex());
				something = *entry.hash();
				REQUIRE(exists(path));
				REQUIRE(entry.created().has_value());
				REQUIRE_THAT(fmt::to_string(*entry.created()), Catch::Matchers::StartsWith("1989-06-04 00:07:00"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Delete one file
		std::this_thread::sleep_for(5ms);
		remove(config().base_path() / "lena.png");
		subject.get_listing(
			"/", [&checked, &something](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.contains("something.jpg"));
				REQUIRE(listing.at("something.jpg").hash().value() == something);
				REQUIRE_FALSE(listing.contains("lena.png"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		// Modify one existing file
		std::this_thread::sleep_for(5ms);
		cv::imwrite(config().base_path()/"something.jpg", test::random_lena(), {cv::IMWRITE_JPEG_QUALITY, 40});
		subject.get_listing(
			"/", [&checked](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.contains("something.jpg"));
				REQUIRE_FALSE(listing.contains("lena.png"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		subject.get_entry(
			"/something.jpg", "", [&checked, &something](auto&& path, auto&& entry, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(entry.hash().value() != something);
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("look-up file")
	{
		env().copy_lena();
		subject.get_entry(
			"/two_lena.avif", "thumbnail.avif", [&checked](auto&& path, auto&& entry, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(entry.mime() == "image/avif");
				auto mat = cv::imread(path);
				REQUIRE(mat.rows == 512);
				REQUIRE(mat.cols == 512);
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("look-up non-exist rendition of existing file")
	{
		env().copy_lena();
		subject.get_entry(
			"/two_lena.avif", "no-such rendition", [&checked](auto&& path, auto&& entry, auto ec)
			{
				REQUIRE(ec == make_error_code(std::errc::no_such_file_or_directory));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("retrieve listing for file")
	{
		env().copy_lena();
		subject.get_listing(
			"/two_lena.avif", [&checked](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.filename() == "two_lena.avif");
				REQUIRE(listing.contains("two_lena.avif"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("retrieve directory listing for an existing file")
	{
		env().copy_lena();
		subject.get_listing(
			"/two_lena.avif/", [&checked](auto&& listing, auto ec)
			{
				REQUIRE(ec == std::errc::not_a_directory);
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("generate renditions")
	{
		env().copy_lena();
		auto physical_path = env().config().base_path() / "lena2.avif";
		std::vector<std::string> rends{"thumbnail.avif", "2048x2048.avif"};

		std::error_code ec;
		auto b2hash = blake2_hash(physical_path, ec);
		REQUIRE_FALSE(ec);

		auto b2hex = b2hash.to_hex();
		auto rend_path = env().config().blob_path() / b2hex.substr(0, 2) / b2hex;

		SECTION("generate_renditions() for single file")
		{
			subject.generate_renditions(physical_path, rends, [&checked, rend_path](auto ec)
			{
				REQUIRE_FALSE(ec);

				auto thumb = cv::imread(rend_path / "thumbnail.avif");
				REQUIRE(thumb.rows == 512);
				REQUIRE(thumb.cols == 512);

				auto def = cv::imread(rend_path / "2048x2048.avif");
				REQUIRE(def.rows <= 2048);
				REQUIRE(def.cols <= 2048);

				checked = true;
			});
			REQUIRE(RunFor(checked));
		}
		SECTION("generate_renditions() for root directory")
		{
			subject.generate_renditions(physical_path.parent_path(), rends, [&checked, rend_path](auto ec)
			{
				REQUIRE(ec == std::errc::is_a_directory);
				checked = true;
			});
			REQUIRE(RunFor(checked));
		}
		SECTION("generate_renditions() for existing directory")
		{
			auto subdir = env().config().base_path() / "some-subdir" / "";
			REQUIRE_FALSE(subdir.has_filename());
			create_directory(subdir);

			subject.generate_renditions(subdir, rends, [&checked, rend_path](auto ec)
			{
				REQUIRE(ec == std::errc::is_a_directory);
				checked = true;
			});
			REQUIRE(RunFor(checked));
		}
		SECTION("generate_renditions() for file that does not exist")
		{
			subject.generate_renditions("/dir/file-not-exist.jpg", rends, [&checked, rend_path](auto ec)
			{
				REQUIRE(ec == std::errc::no_such_file_or_directory);
				checked = true;
			});
			REQUIRE(RunFor(checked));
		}
	}
	SECTION("file does not exist")
	{
		subject.get_listing(
			"/no_such_file", [&checked](auto&& listing, auto ec)
			{
				REQUIRE(ec == std::errc::no_such_file_or_directory);
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));

		subject.get_entry(
			"/no_such_file_either", "", [&checked](auto&&, auto&&, auto ec)
			{
				REQUIRE(ec == std::errc::no_such_file_or_directory);
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("get entry with root directory")
	{
		subject.get_entry("/", "", [&checked](auto&&, DirectoryEntry&& entry, std::error_code ec)
		{
			REQUIRE(ec == std::errc::not_supported);
			REQUIRE(entry.is_directory());
			checked = true;
		});
		REQUIRE(RunFor(checked));
	}
	SECTION("get entry with non-exist directory")
	{
		subject.get_entry("/not-exist/", "", [&checked](auto&&, DirectoryEntry&&, std::error_code ec)
		{
			REQUIRE(ec == std::errc::no_such_file_or_directory);
			checked = true;
		});
		REQUIRE(RunFor(checked));
	}
	SECTION("invalid redis data type for directory")
	{
		// The "dir:/" key should be a hash, but we set it to a string here.
		db->sync_command(ioc, "SET {}dir:/ some_string", config().redis_prefix());
		subject.get_listing(
			"/", [&checked, this](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.contains("lena.png"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
	SECTION("invalid redis data type for file entry in directory")
	{
		// The "dir:/" key should be a hash, but we set it to a string here.
		db->sync_command(ioc, "DEL {}dir:/", config().redis_prefix());
		db->sync_command(ioc, "DEL {}max:mod:/", config().redis_prefix());
		db->sync_command(ioc, "HSET {}dir:/ invalid_msgpack.jpg some_invalid_msgpack", config().redis_prefix());
		REQUIRE(exists(config().base_path()/"black.jpg"));

		subject.get_listing(
			"/", [&checked, this](auto&& listing, auto ec)
			{
				REQUIRE_FALSE(ec);
				REQUIRE(listing.contains("black.jpg"));
				REQUIRE_FALSE(listing.contains("invalid_msgpack.jpg"));
				checked = true;
			}
		);
		REQUIRE(RunFor(checked));
	}
}

TEST_CASE("JPEG image metadata", "[normal]")
{
	std::error_code ec;
	DirectoryEntry meta{test::images()/"white.jpg", ec};
	REQUIRE_FALSE(ec);

	REQUIRE(meta.full_update(test::images()/"white.jpg", ec));
	REQUIRE_FALSE(ec);

	REQUIRE(meta.mime() == "image/jpeg");
	REQUIRE(meta.created().has_value());
	REQUIRE(meta.created()->time_since_epoch().count() > 0);
}

TEST_CASE("AVIF image metadata in DirectoryEntry", "[normal]")
{
	std::error_code ec;
	DirectoryEntry meta{test::images()/"ink_logo.avif", ec};
	REQUIRE_FALSE(ec);

	REQUIRE(meta.full_update(test::images()/"ink_logo.avif", ec));
	REQUIRE_FALSE(ec);

	REQUIRE(meta.mime() == "image/avif");
	REQUIRE(meta.created().has_value());
	REQUIRE(meta.created()->time_since_epoch().count() > 0);

	using namespace std::chrono;
	REQUIRE(fmt::to_string(time_point_cast<seconds>(*meta.created())) == "1989-06-04 00:07:00");
}

#if __has_include (<sys/inotify.h>)
TEST_CASE_METHOD(test::ServerFixture, "DirectoryWatcher events when moving directories", "[normal]")
{
	StorageSync sync{config(), ioc.get_executor()};
	DirectoryWatcher watcher{ioc.get_executor(), config().base_path(), sync};

	// Create a temp directory with some files
	test::TempDirectory tmp_dir{"src"};
	auto tmp_subdir = tmp_dir.path() / "subdir";
	create_directory(tmp_subdir);
	copy_file(test::images() / "white.jpg", tmp_subdir / "something.jpg");
	copy_file(test::images() / "lena.png", tmp_subdir / "lena.png");

	watcher.start();
	std::error_code ec;
	rename(tmp_subdir, config().base_path() / "subdir", ec);
	REQUIRE_FALSE(ec);

	REQUIRE(RunFor(
		[this]{
			// For some reason, lena.png does not have rendition generated.
			// Only something.jpg has rendition so the count is 1.
			return std::distance(
				std::filesystem::directory_iterator{config().blob_path()},
				std::filesystem::directory_iterator{}
			) >= 1;
		},
		10s
	));
	watcher.stop();
}
#endif
