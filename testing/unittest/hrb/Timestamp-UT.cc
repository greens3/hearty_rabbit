/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 5/27/18.
//

#include <catch2/catch_test_macros.hpp>

#include "util/Timestamp.hh"

using namespace hrb;
using nlohmann::json;
using namespace std::chrono;

TEST_CASE("read and write round-trip timestamps from JSON", "[normal]")
{
	auto subject = std::chrono::system_clock::now();
	json j;
	to_json(j, HttpTimePoint{subject});

	HttpTimePoint<> out;
	from_json(j, out);
	REQUIRE(
		std::chrono::time_point_cast<std::chrono::system_clock::duration>(out.tp) ==
		subject
	);

//	REQUIRE(fmt::to_string(out) == "");
}

TEST_CASE("automatically convert timestamps from/to JSON", "[normal]")
{
//	auto subject = Timestamp::now();
//	json json = subject;
//	REQUIRE(json.get<Timestamp>() == subject);
}