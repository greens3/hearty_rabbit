/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/10/23.
//

#include <catch2/catch_test_macros.hpp>

#include "hrb/RequestIntent.hh"

using namespace hrb;
using namespace boost::beast::http;

TEST_CASE("normal URL parsing", "[normal]")
{
	RequestIntent empty;
	REQUIRE(empty.path() == "/");
	REQUIRE(empty.action() == RequestIntent::view);
	REQUIRE(empty.method() == verb::get);
	REQUIRE(empty.target() == "/");
	REQUIRE(empty.rendition() == "");

	RequestIntent listing{"/somefile?list", verb::get};
	REQUIRE(listing.path() == "/somefile");
	REQUIRE(listing.action() == RequestIntent::list);
	REQUIRE(listing.target() == "/somefile?list");
	REQUIRE(listing.method() == verb::get);

	RequestIntent lena{"/lena.png?view", verb::get};
	REQUIRE(lena.action() == RequestIntent::view);
	REQUIRE(lena.path() == "/lena.png");
	REQUIRE(lena.target() == "/lena.png?view");
	REQUIRE(lena.method() == verb::get);

	RequestIntent lena_thumb{"/lena.png?content=thumbnail", verb::get};
	REQUIRE(lena_thumb.action() == RequestIntent::content);
	REQUIRE(lena_thumb.path() == "/lena.png");
	REQUIRE(lena_thumb.target() == "/lena.png?content=thumbnail");
	REQUIRE(lena_thumb.method() == verb::get);
	REQUIRE(lena_thumb.rendition() == "thumbnail");

	RequestIntent login{"/?auth=login", verb::post};
	REQUIRE(login.path() == "/");
	REQUIRE(login.action() == RequestIntent::login);
	REQUIRE(login.target() == "/?auth=login");
	REQUIRE(login.method() == verb::post);

	RequestIntent logout{"/?auth=logout", verb::get};
	REQUIRE(logout.path() == "/");
	REQUIRE(logout.action() == RequestIntent::logout);
	REQUIRE(logout.target() == "/?auth=logout");
	REQUIRE(logout.method() == verb::get);

	RequestIntent login_path{"/some/kind/of/path?auth=login", verb::post};
	REQUIRE(login_path.path() == "/some/kind/of/path");
	REQUIRE(login_path.action() == RequestIntent::login);
	REQUIRE(login_path.target() == "/some/kind/of/path?auth=login");
	REQUIRE(login_path.method() == verb::post);

	RequestIntent css{"/.lib/intent.js", verb::get};
	REQUIRE(css.path() == "intent.js");
	REQUIRE(css.action() == RequestIntent::lib);
	REQUIRE(css.target() == "/.lib/intent.js");
	REQUIRE(css.method() == verb::get);
	REQUIRE(css == RequestIntent{RequestIntent::lib, "intent.js"});

	RequestIntent css2{"/.lib/intent.js?", verb::get};
	REQUIRE(css2 == css);

	RequestIntent utf8_1{RequestIntent::content, "/台北/小記者/chris"};
	REQUIRE(utf8_1.path() == "/台北/小記者/chris");
	REQUIRE(utf8_1.target() == "/%E5%8F%B0%E5%8C%97/%E5%B0%8F%E8%A8%98%E8%80%85/chris");

	RequestIntent utf8_2{"/08%E6%9C%8818%E6%97%A5", verb::get};
	REQUIRE(utf8_2.path() == "/08月18日");

	RequestIntent utf8_3{"/%E4%B8%80%E5%B0%8F%E9%83%A8%E5%88%86?list", verb::get};
	REQUIRE(utf8_3.path() == "/一小部分");
	REQUIRE(utf8_3.action() == RequestIntent::list);
}
