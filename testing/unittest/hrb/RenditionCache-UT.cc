/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 3/3/18.
//

#include "hrb/RenditionCache.hh"
#include "hrb/UploadFile.hh"

#include "util/MMap.hh"
#include "util/Magic.hh"
#include "util/RenditionSetting.hh"

#include "common/TempDirectory.hh"
#include "common/TestImages.hh"
#include "config.hh"

#include <opencv2/imgcodecs.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace hrb;

class RenditionCacheUTFixture
{
protected:
	test::TempDirectory  m_blob_path{"RenditionCache-UT"};
};

TEST_CASE_METHOD(RenditionCacheUTFixture, "upload big upright image as BlobFile", "[normal]")
{
	auto master = hrb::test::images()/"up_f_upright.jpg";

	RenditionCache subject{m_blob_path.path(), master};

	RenditionSetting cfg;
	cfg.add("128x128.avif",   {128, 128}, "image/avif");    // default
	cfg.add("thumbnail.jpeg", {64, 64},   "image/jpeg");

	std::error_code ec;
	std::string rendition;
	auto rend128 = subject.rendition(rendition, cfg, std::string{constants::haarcascades_path()}, ec);
	REQUIRE(!ec);

	auto src = MMap::open(master, ec);
	REQUIRE_FALSE(ec);

	auto out128 = MMap::open(m_blob_path.path()/"128x128.avif", ec);
	REQUIRE_FALSE(ec);
	REQUIRE(out128.size() < src.size());
	REQUIRE(std::memcmp(out128.data(), src.data(), out128.size()) != 0);

	auto [setting, gen_mmap] = subject.rendition("thumbnail.jpeg", cfg, std::string{constants::haarcascades_path()}, ec).value();
	REQUIRE(Magic::instance().mime(gen_mmap) == "image/jpeg");
	REQUIRE(setting.dim.width() == 64);
	REQUIRE(setting.dim.height() == 64);
	REQUIRE(setting.mime == "image/jpeg");

	auto gen_jpeg = cv::imread(gen_mmap, cv::IMREAD_ANYCOLOR);
	REQUIRE(gen_jpeg.data);
	REQUIRE(!gen_jpeg.empty());
	REQUIRE(gen_jpeg.cols <= 64);
	REQUIRE(gen_jpeg.rows <= 64);
}

TEST_CASE_METHOD(RenditionCacheUTFixture, "upload big rot90 image as BlobFile", "[normal]")
{
	auto master = hrb::test::images()/"up_f_rot90.jpg";
	RenditionCache subject{m_blob_path.path(), master};

	RenditionSetting cfg;
	cfg.add("2048x2048.jpeg", {2048, 2048}, "image/jpeg");

	std::error_code ec;
	auto [setting, rotated] = subject.rendition("", cfg, std::string{constants::haarcascades_path()}, ec).value();
	REQUIRE_FALSE(ec);
	REQUIRE(rotated.filename() == "2048x2048.jpeg");
	REQUIRE(Magic::instance().mime(rotated) == "image/jpeg");

	auto gen_jpeg = cv::imread(rotated, cv::IMREAD_ANYCOLOR);
	REQUIRE(gen_jpeg.cols == 160);
	REQUIRE(gen_jpeg.rows == 192);
}

TEST_CASE_METHOD(RenditionCacheUTFixture, "upload lena.png as BlobFile", "[normal]")
{
	auto master = hrb::test::images()/"lena.png";

	RenditionCache subject{m_blob_path.path(), master};

	// generate smaller rendition
	RenditionSetting cfg;
	cfg.add("256x256.avif",   {256, 256}, "image/avif");

	std::error_code ec;
	auto [setting, rend] = subject.rendition("", cfg, std::string{constants::haarcascades_path()}, ec).value();
	REQUIRE(rend.filename() == "256x256.avif");

	REQUIRE(Magic::instance().mime(rend) == "image/avif");
	auto rend_mat = cv::imread(rend, cv::IMREAD_ANYCOLOR);;
	REQUIRE(rend_mat.rows == 256);
	REQUIRE(rend_mat.cols == 256);
}

/*
TEST_CASE_METHOD(RenditionCacheUTFixture, "upload image from camera as BlobFile", "[normal]")
{
	auto master = test::images()/"DSC_7926.JPG";
	if (fs::exists(master))
	{
		auto [tmp, src] = upload(master);
		std::error_code ec;
		BlobFile subject{m_blob_path, master};
//		REQUIRE(subject.original_datetime() != BlobFile::time_point{});

//		auto dt = subject.original_datetime();
//		auto tt = std::chrono::system_clock::to_time_t(dt);

//		struct std::tm tm{};
//		REQUIRE(::gmtime_r(&tt, &tm));
//
//		// Wed Jul 18 10:00:48 2018
//		INFO(asctime(&tm));
//
//		REQUIRE(tm.tm_year == 118);     // year 2018
//		REQUIRE(tm.tm_mon == 6);        // July
//		REQUIRE(tm.tm_mday == 18);
//		REQUIRE(tm.tm_hour == 10);
//		REQUIRE(tm.tm_min == 0);
//		REQUIRE(tm.tm_sec == 48);

	}
}
*/
/*
*/
