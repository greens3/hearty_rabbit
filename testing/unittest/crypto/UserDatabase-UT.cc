/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/27/18.
//

#include <catch2/catch_all.hpp>
#include "common/ServerEnvironment.hh"

#include "hrb/UserID.hh"
#include "crypto/UserDatabase.hh"
#include "crypto/Password.hh"
#include "crypto/Random.hh"
#include "util/Cookie.hh"
#include "util/Error.hh"
#include "util/Escape.hh"

using namespace hrb;
using namespace hrb::test;

TEST_CASE("UserID sanity checks", "[normal]")
{
	UserID anonymous;
	REQUIRE(anonymous.cookie().str() == "id=");
	REQUIRE_FALSE(anonymous.valid());

	UserID invalid_cookie{Cookie{}, "invalid-user"};
	REQUIRE_FALSE(invalid_cookie.valid());
	REQUIRE(invalid_cookie.username() == std::string{});
	REQUIRE(invalid_cookie == anonymous);
}

TEST_CASE_METHOD(ServerFixture, "Add new user to user database", "[normal]")
{
	auto redis_ = redis();
	UserDatabase subject{config(), *redis_};

	std::error_code ec;
	subject.add_user("sumsum", Password{"bearbear"}, ec);
	subject.add_user("yungyung", Password{"jipjip"}, ec);
	REQUIRE_FALSE(ec);

	bool checked{false};
	subject.verify_user("sumsum", Password{"bearbear"}, [&checked, subject](auto auth, auto ec) mutable
	{
		REQUIRE_FALSE(ec);
		REQUIRE(auth.username() == "sumsum");
		REQUIRE(auth.valid());
		REQUIRE(auth.session().has_value());

		subject.verify_session(*auth.session(), [&checked](auto auth, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(auth.username() == "sumsum");
			checked = true;
		});
	});
	REQUIRE(RunFor(checked));

	// Incorrect password
	subject.verify_user("sumsum", Password{"rabbit"}, [&checked](auto, auto ec)
	{
		REQUIRE(ec == std::errc::permission_denied);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	// User does not exist
	subject.verify_user("no_such_person", Password{"no_such_password"}, [&checked](auto, auto ec)
	{
		REQUIRE(ec == std::errc::permission_denied);
		checked = true;
	});
	REQUIRE(RunFor(checked));

	// Incorrect session ID
	auto bad_session = insecure_random<SessionID>();
	subject.verify_session(bad_session, [&checked](auto auth, auto ec)
	{
		REQUIRE(ec == std::errc::permission_denied);
		checked = true;
	});
	REQUIRE(RunFor(checked));
}

TEST_CASE("Add new user to shadow file", "[normal]")
{
	ShadowFile subject;
	subject.new_user("siuyung", Password{"rabbit"});
	subject.new_user("sumsum",  Password{"bearbear"});

	auto siuyung = subject.find("siuyuNg");
	REQUIRE(siuyung != subject.end());

	auto sumsum  = subject.find("SUMsum");
	REQUIRE(sumsum != subject.end());

	Password rabbit{"rabbit"};
	auto key1 = rabbit.derive_key(siuyung->salt.buffer(), siuyung->iteration, siuyung->hash_algorithm);
	REQUIRE(key1 == siuyung->password_hash);

	auto key2 = rabbit.derive_key(sumsum->salt.buffer(), sumsum->iteration, sumsum->hash_algorithm);
	REQUIRE(key2 != sumsum->password_hash);

	// Change password for sumsum
	Password pw2{"not_bearbear"};
	subject.new_user("sumsum", pw2);
	sumsum  = subject.find("suMSum");
	REQUIRE(sumsum != subject.end());
	auto key3 = pw2.derive_key(sumsum->salt.buffer(), sumsum->iteration, sumsum->hash_algorithm);
	REQUIRE(key3 == sumsum->password_hash);
}

TEST_CASE("User entry parsing in shadow file", "[normal]")
{
	const std::string salt_hex =
		"0123456789" "0123456789" "0123456789"
		"0123456789" "0123456789" "0123456789" "0123";
	const std::string hash_hex =
		"0123456789" "0123456789" "0123456789"
		"0123456789" "0123456789" "0123456789" "0123"
		"0123456789" "0123456789" "0123456789"
		"0123456789" "0123456789" "0123456789" "0123";

	auto line = "sumsum:" + salt_hex + ":" + hash_hex + ":5001:sha512";

	auto subject = ShadowFile::parse_entry(line);
	REQUIRE(subject.username == "sumsum");
	REQUIRE(subject.iteration == 5001UL);
	REQUIRE(subject.hash_algorithm == "sha512");
	REQUIRE(subject.password_hash == hex_to_array<ShadowFile::Hash{}.size()>(hash_hex).value());
	REQUIRE(subject.salt == hex_to_array<ShadowFile::Salt{}.size()>(salt_hex).value());
	REQUIRE(fmt::to_string(subject) == line);

	std::random_device rd;
	std::mt19937 gen{rd()};

	auto user2{subject};
	user2.username = "yungyung";
	std::shuffle(user2.password_hash.begin(), user2.password_hash.end(), gen);
	std::shuffle(user2.salt.begin(), user2.salt.end(), gen);
	user2.iteration = 1000;

	TempDirectory tmp_dir{"shadow_dir"};
	auto shadow_path = tmp_dir.path() / "shadow";

	ShadowFile file;
	file.append(subject);
	file.append(user2);

	std::error_code ec;
	file.save(shadow_path, ec);
	REQUIRE_FALSE(ec);

	ShadowFile loaded;
	loaded.load(shadow_path, ec);
	REQUIRE_FALSE(ec);
	REQUIRE(loaded.size() == file.size());
	REQUIRE(std::equal(loaded.begin(), loaded.end(), file.begin()));
}

TEST_CASE("Test password on_header", "[normal]")
{
	Password subject{"Hello"};
	REQUIRE(subject.get() == "Hello");
	REQUIRE(subject.size() == 5);
	REQUIRE(!subject.empty());

	// different salt must produce different keys
	auto key1 = subject.derive_key(boost::asio::buffer("1"), 100, "sha512");
	auto key2 = subject.derive_key(boost::asio::buffer("2"), 100, "sha512");
	REQUIRE(key1 != key2);

	subject.clear();
	REQUIRE(subject.empty());
	REQUIRE(subject.size() == 0);

	REQUIRE_THROWS_AS(subject.derive_key(boost::asio::buffer("salt"), 100, "sha512" ), std::exception);
}
