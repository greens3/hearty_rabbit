/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/12/23.
//

#include <catch2/catch_test_macros.hpp>

#include <array>

#include "crypto/Blake2.hh"
#include "crypto/Error.hh"

using namespace hrb;

TEST_CASE("test blake2 hash", "[normal]")
{
	Blake2 h;
	const char hello[] = "Hello, world!";
	h.update(std::data(hello), std::size(hello));

	auto hello_world = h.finalize();
	REQUIRE(hello_world == Blake2::Hash({
		193, 25, 14, 17, 90, 42, 30, 151, 96, 119, 184, 109, 165, 44, 104, 236, 12,
		208, 32, 102, 69, 157, 157, 78, 136, 135, 226, 69, 54, 230, 7, 100
	}));

	Blake2 h2;
	const char world[] = "World, Hello!";
	h2.update(std::data(world), std::size(world));
	h2.update(nullptr, 0);

	auto world_hello = h2.finalize();
	REQUIRE(hello_world != world_hello);
}

TEST_CASE("test OpenSSL error strings", "[normal]")
{
	// generate an error
	REQUIRE_FALSE(::EVP_MD_fetch(nullptr, "No such hash", nullptr));

	std::string error{OpenSSLException{}.what()};
	REQUIRE(error != "");
}