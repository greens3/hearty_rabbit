/*
	Copyright © 2020 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 2/1/2020.
//

#include <catch2/catch_test_macros.hpp>

#include "util/Timestamp.hh"
#include "util/Image.hh"
#include "util/MMap.hh"
#include "util/Magic.hh"

#include "common/TestImages.hh"

using namespace hrb;

TEST_CASE("read date time from black.jpg", "[normal]")
{
	std::error_code ec;
	auto black = MMap::open(test::images() / "black.jpg", ec);

	REQUIRE(!ec);

	Image subject{black.buffer(), Magic::instance().mime(black.buffer())};

	auto dt = subject.date_time();
	REQUIRE(fmt::to_string(HttpTimePoint{dt}) == "Thu, 02 Jan 2020 21:42:02.000 GMT");
}

TEST_CASE("AVIF image metadata", "[normal]")
{
	std::error_code ec;
	auto content = MMap::open(test::images() / "ink_logo.avif", ec);
	REQUIRE_FALSE(ec);

	Image meta{content.buffer(), Magic::instance().mime(content.buffer())};
	REQUIRE(meta.exif_version() == "0232");

	using namespace std::chrono;
	REQUIRE(fmt::to_string(time_point_cast<seconds>(meta.date_time())) == "1989-06-04 00:07:00");
}

TEST_CASE("mspaint.jpg image metadata", "[normal]")
{
	std::error_code ec;
	auto content = MMap::open(test::images() / "mspaint.jpg", ec);
	REQUIRE_FALSE(ec);

	Image meta{content.buffer(), Magic::instance().mime(content.buffer())};
	REQUIRE(meta.exif_version() == "0231");

	using namespace std::chrono;
	REQUIRE(fmt::to_string(time_point_cast<seconds>(meta.date_time())) == "2023-04-01 10:39:23");
}
