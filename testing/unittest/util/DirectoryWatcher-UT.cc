/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 7/14/23.
//

#include "util/DirectoryWatcher.hh"
#include "util/DirectoryEvents.hh"
#include "crypto/Random.hh"

#include <catch2/catch_all.hpp>

#include <deque>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <random>

using namespace hrb;

#if __has_include (<sys/inotify.h>)

struct MockDirectoryEvents : DirectoryEvents
{
	std::mutex m_lock;
	std::condition_variable m_cond_var;

	struct Op
	{
		std::filesystem::path path;
		std::string action;

		friend auto operator<=>(const Op&, const Op&) = default;
	};

	std::deque<Op> m_changes;

	std::deque<Op> fetch(std::size_t expected_count=1)
	{
		std::unique_lock ul{m_lock};

		// 10 seconds should be enough
		REQUIRE(m_cond_var.wait_for(
			ul,
			std::chrono::seconds{10},
			[this, expected_count]{return m_changes.size() == expected_count;}
		));

		auto result{std::move(m_changes)};
		ul.unlock();

		return result;
	}

	void queue(Op&& p)
	{
		std::unique_lock ul{m_lock};
		m_changes.push_back(std::move(p));
		ul.unlock();
		m_cond_var.notify_one();
	}

	void on_directory_added(std::filesystem::path&& dir) override
	{
		queue({std::move(dir), "create dir"});
	}

	void on_file_added(std::filesystem::path&& path) override
	{
		queue({std::move(path), "create file"});
	}

	void on_file_write_close(std::filesystem::path&& path) override
	{
	}

	void on_file_removed(std::filesystem::path&& path) override
	{
		queue({std::move(path), "delete file"});
	}

	void on_directory_removed(std::filesystem::path&& dir) override
	{
		queue({std::move(dir), "delete dir"});
	}
};

TEST_CASE("normal case for DirectoryWatcher", "[normal]")
{
	std::mt19937 gen{insecure_random<std::mt19937::result_type>()};

	MockDirectoryEvents events;
	using Op = MockDirectoryEvents::Op;

	boost::asio::io_context ioc;
	DirectoryWatcher subject{ioc.get_executor(), "/tmp", events};
	REQUIRE(subject.size() == 1);

	// Call start() to initiate async read before starting threads.
	// Otherwise, ioc.run() will return immediately because the queue inside ioc is empty.
	// In that case, we will need a work guard to prevent run() from returning.
	subject.start();

	std::vector<std::thread> workers;
	for (int i = 0; i< 5; ++i)
		workers.emplace_back([&ioc]{ioc.run();});

	SECTION("create tmp dir and delete")
	{
		char tmpdir[] = "/tmp/tempdirXXXXXX";
		mkdtemp(tmpdir);
		std::filesystem::create_directory(tmpdir);
		std::filesystem::remove(tmpdir);

		auto created = events.fetch(2);
		REQUIRE(created.size() == 2);
		REQUIRE(created[0] == Op{tmpdir, "create dir"});
		REQUIRE(created[1] == Op{tmpdir, "delete dir"});
	}
	SECTION("randomly create files and dirs")
	{
		std::vector<Op> ops;

		auto create_dir = [&]{
			char tmpdir[] = "/tmp/tempdirXXXXXX";
			mkdtemp(tmpdir);

			ops.push_back({tmpdir, "create dir"});
			std::filesystem::create_directory(tmpdir);
		};

		auto create_file = [&]{
			char tmpfile[] = "/tmp/tempfileXXXXXX";
			int file = ::mkstemp(tmpfile);
			REQUIRE(file > 0);
			ops.push_back({tmpfile, "create file"});

			std::string text{"hello, world!"};
			std::shuffle(text.begin(), text.end(), gen);

			REQUIRE(::write(file, text.data(), text.size()) == text.size());
			close(file);
		};

		// Randomly create some files and directories
	    std::uniform_int_distribution<> loop_count(6, 10);
	    std::uniform_int_distribution<> dir_count(3, 6);
	    std::uniform_int_distribution<> file_count(4, 6);

		int loops = loop_count(gen);
		for (int i = 0; i < loops; i++)
		{
			int dirs = dir_count(gen);
			for (int j=0; j < dirs; j++)
				create_dir();

			int files = file_count(gen);
			for (int j=0; j < files; j++)
				create_file();
		}

		auto created = events.fetch(ops.size());
		REQUIRE(ops.size() == created.size());

		std::sort(ops.begin(), ops.end());
		std::sort(created.begin(), created.end());
		REQUIRE(std::equal(ops.begin(), ops.end(), created.begin()));

		auto dir_created = std::count_if(ops.begin(), ops.end(), [](auto& op){return op.action == "create dir";});
		REQUIRE(subject.size() == dir_created+1);

		// Now remove the files and directories randomly
		std::shuffle(ops.begin(), ops.end(), gen);

		for (auto& op : ops)
		{
			if (op.action == "create file")
			{
				std::filesystem::remove(op.path);
				op.action = "delete file";
			}
			else if (op.action == "create dir")
			{
				std::filesystem::remove_all(op.path);
				op.action = "delete dir";
			}
		}

		auto deleted = events.fetch(ops.size());
		REQUIRE(ops.size() == deleted.size());

		std::sort(ops.begin(), ops.end());
		std::sort(deleted.begin(), deleted.end());
		REQUIRE(std::equal(ops.begin(), ops.end(), deleted.begin()));
	}

	// All files and directories created by the test case are deleted
	REQUIRE(subject.size() == 1);

	ioc.stop();
	subject.stop();

	for (auto& worker : workers)
		worker.join();
}

#endif
