/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 8/14/23.
//

#pragma once

#include "CommonFixture.hh"
#include "TempDirectory.hh"
#include "crypto/Blake2.hh"
#include "util/Configuration.hh"

#include "Sanali.hh"

#include <optional>

namespace hrb::test {

class ServerEnvironment
{
public:
	ServerEnvironment() = default;
	~ServerEnvironment() = default;

	const Configuration& config();
	std::shared_ptr<sanali::Connection> redis(boost::asio::io_context& ioc);

	static std::string file_hash(const std::filesystem::path& path);
	void copy_lena();

	std::filesystem::path prepare_base_path();
	std::filesystem::path prepare_blob_path();

private:
	std::optional<Configuration> m_cfg;
	std::optional<TempDirectory> m_base_path, m_blob_path;
};

class ServerFixture : public CommonFixture
{
protected:
	auto& config()      {return env().config();}
	auto  redis()       {return env().redis(ioc);}
	static auto file_hash(const std::filesystem::path& path)
	{
		return ServerEnvironment::file_hash(path);
	}
	ServerEnvironment& env() {return m_env ? *m_env : m_env.emplace();}

	static Blake2::Hash blake2_hash(const std::filesystem::path& file, std::error_code& ec);

private:
	std::optional<ServerEnvironment> m_env;
};

} // end of namespace hrb::test