/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 8/1/23.
//

#pragma once
#include <boost/asio/io_context.hpp>

#include <chrono>
#include <utility>

namespace hrb::test {

class CommonFixture
{
public:
	CommonFixture() = default;

protected:
	template <typename Condition> requires (requires(Condition cond){{cond()} -> std::convertible_to<bool>;})
	bool RunFor(Condition&& cond, std::chrono::seconds duration = std::chrono::seconds{10})
	{
		auto until = std::chrono::steady_clock::now() + duration;
		while (!cond() && ioc.run_one_until(until));
		ioc.restart();
		return cond();
	}

	bool RunFor(bool& cond, std::chrono::seconds duration = std::chrono::seconds{10})
	{
		auto until = std::chrono::steady_clock::now() + duration;
		while (!cond && ioc.run_one_until(until));
		ioc.restart();
		return std::exchange(cond, false);
	}

protected:
	boost::asio::io_context ioc;
};

} // end of namespace hrb::test