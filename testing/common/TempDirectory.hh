//
// Created by root on 11/27/23.
//

#pragma once

#include <filesystem>
#include <string>

namespace hrb::test {

class TempDirectory {
public:
	explicit TempDirectory(std::string prefix);

	~TempDirectory();

	TempDirectory(TempDirectory &&) = delete;

	auto &path() const { return m_path; }

private:
	std::filesystem::path m_path;
};

} // end of namespace
