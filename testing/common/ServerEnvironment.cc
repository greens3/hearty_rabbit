/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 8/3/23.
//

#include "ServerEnvironment.hh"
#include "TestImages.hh"

#include "crypto/Random.hh"
#include "crypto/Blake2.hh"

#include "util/MMap.hh"
#include "util/WebServerConfig.hh"

#include "config.hh"

#include <opencv2/imgcodecs.hpp>
#include <cstdlib>

namespace hrb::test {

const Configuration& ServerEnvironment::config()
{
	if (!m_cfg)
	{
		m_cfg.emplace(std::filesystem::path{CONFIG_FILE} / hrb::constants::config_filename());

		// Use a different prefix for all redis keys in UT
		m_cfg->redis_prefix("hrb-ut:");

		// use a randomize port
		auto https_port = m_cfg->web_server().listen_https().port();
		auto http_port = m_cfg->web_server().listen_http().port();
		while (https_port <= 1024 || https_port == m_cfg->web_server().listen_https().port())
			hrb::insecure_random(https_port);
		while (http_port <= 1024 || http_port == m_cfg->web_server().listen_http().port())
			hrb::insecure_random(http_port);
		m_cfg->change_listen_ports(https_port, http_port);

		// Refer to environment variables for redis
		auto env_host = std::getenv("HRB_REDIS_HOST");
		if (env_host)
			m_cfg->change_redis_host(env_host);

		auto env_port = std::getenv("HRB_REDIS_PORT");
		if (env_port)
			m_cfg->change_redis_port(env_port);

		prepare_base_path();
		prepare_blob_path();

		// put the shadow file in blob path
		m_cfg->shadow_file(m_cfg->blob_path()/"shadow");
	}
	return *m_cfg;
}

std::filesystem::path ServerEnvironment::prepare_base_path()
{
	// Construct a dummy base path for test cases by copying the test data to a new directory.
	m_base_path.emplace(std::filesystem::current_path()/"hearty_test_base");

	// Copy some test images to the new temp directory
	for (auto&& file : {"lena.png", "black.jpg", "mspaint.jpg"})
	{
		copy_file(images() / file, m_base_path->path() / file);
		last_write_time(m_base_path->path() / file, last_write_time(images() / file));
	}

	if (m_cfg)
		m_cfg->base_path(m_base_path->path());

	return m_base_path->path();
}

void ServerEnvironment::copy_lena()
{
	if (!m_base_path) prepare_base_path();
	assert(m_base_path);

	// Generate some random files
	cv::imwrite(m_base_path->path()/"lena1.jpg",  test::random_lena(), {cv::IMWRITE_JPEG_QUALITY, 40});
	cv::imwrite(m_base_path->path()/"lena2.avif", test::random_lena(), {cv::IMWRITE_AVIF_QUALITY, 40});
	cv::imwrite(m_base_path->path()/"lena3.webp", test::random_lena(), {cv::IMWRITE_WEBP_QUALITY, 100});
	cv::imwrite(m_base_path->path()/"private_lena.avif", test::random_lena(), {cv::IMWRITE_AVIF_QUALITY, 20});
	permissions(m_base_path->path()/"private_lena.avif", std::filesystem::perms::owner_read);

	auto two_lena = test::random_lena();
	two_lena.push_back(test::random_lena());
	cv::imwrite(m_base_path->path()/"two_lena.avif", two_lena, {cv::IMWRITE_AVIF_QUALITY, 20});
}

std::filesystem::path ServerEnvironment::prepare_blob_path()
{
	// Construct a dummy base path for test cases by copying the test data to a new directory.
	m_blob_path.emplace(std::filesystem::current_path()/"hearty_test_blob");
	if (m_cfg)
		m_cfg->blob_path(m_blob_path->path());
	return m_blob_path->path();
}

std::shared_ptr<sanali::Connection> ServerEnvironment::redis(boost::asio::io_context& ioc)
{
	auto redis = sanali::connect(ioc.get_executor(), config().redis_host(), config().redis_port());
	redis->set_logger([](const std::string& msg)
	{
		fmt::print("Message from sanali: {}\n", msg);
	});
	return redis;
}

std::string ServerEnvironment::file_hash(const std::filesystem::path& path)
{
	std::error_code mmap_ec;
	auto mmap = MMap::open(path, mmap_ec);

	std::string result;
	if (!mmap_ec)
	{
		Blake2 hash;
		hash.update(mmap.data(), mmap.size());

		result = hash.finalize().to_hex();
	}
	return result;
}

TempDirectory::TempDirectory(std::string prefix)
{
	prefix.append("XXXXXX");

	// Construct a dummy base path for test cases by copying the test data to a new directory.
	if (mkdtemp(prefix.data()) == nullptr)
		throw std::system_error(errno, std::generic_category());

	m_path = prefix;
	create_directories(m_path);
}

TempDirectory::~TempDirectory()
{
	if (!m_path.empty())
		remove_all(m_path);
}

Blake2::Hash ServerFixture::blake2_hash(const std::filesystem::path& file, std::error_code& ec)
{
	auto mmap = MMap::open(file, ec);
	if (ec)
		return {};

	Blake2 hash;
	hash.update(mmap.data(), mmap.size());
	return hash.finalize();
}

} // end of namespace hrb::test