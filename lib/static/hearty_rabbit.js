/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>

    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

const default_album = "(default album)";

// Default permission of newly uploaded files
let default_perm	= "private";
let toast_timer 	= null;
let last_moved_album = "";
const images_per_page = 50;

function assign_image_to_entry(entry, image_url, description)
{
	let img = entry.querySelector("img");
	img.src = image_url + ".jpeg";
	img.alt = description;
	entry.querySelector(".hrb-entry-avif").srcset = image_url + ".avif";
	entry.querySelector(".hrb-entry-webp").srcset = image_url + ".webp";
	entry.querySelector("span.text-overlay-box").textContent = description;

	img.addEventListener("error", ev =>
	{
		// Reload image if it fails
		console.log("Error when loading thumbnail", ev, image_url);
		img.src = "/.lib/loading.svg";
		img.src = image_url + ".jpeg";
	}, false);
}

function is_image(mime)
{
	return mime.indexOf("image/") !== -1;
}

function is_directory(mime)
{
	return mime === "inode/directory; charset=binary";
}

function add_element(filename, node_before)
{
	// assuming the element is already inserted to dir.elements
	const element = dir.elements[filename];

	// TODO: show non-image blobs
	if (!is_image(element.mime) && !is_directory(element.mime))
		return;

	// create one new entry for each blob
	const proto = document.getElementById("hrb-entry-prototype");
	const root  = document.getElementById("hrb-entry-root");

	if (node_before === undefined)
		node_before = proto;

	// Clone the prototype node and update its ID
	const entry = proto.cloneNode(true);
	entry.id = "hrb-entry-" + filename;
    entry.addEventListener("click", ev=>
	{
		ev.preventDefault();
		if (get_selected_blobs().length === 0)
		{
			if (is_image(element.mime))
				show_image(filename, true);
			else if (is_directory(element.mime))
			{
				fetch_dir(dir.logical_path + filename + '/');
			}
		}
		else
			toggle_select_blob(filename);
	}, false);

	// Assign blob URL to the image in the cloned node
	if (is_image(element.mime))
		assign_image_to_entry(entry, dir.logical_path + filename + "?content=thumbnail", filename);
	else
	{
		let img = entry.querySelector("img");
		img.src = "/.lib/folder.svg";
		entry.querySelector(".hrb-entry-avif").srcset = "/.lib/folder.svg";
		entry.querySelector(".hrb-entry-webp").srcset = "/.lib/folder.svg";

		const overlay = entry.querySelector("span.text-overlay-box");
		overlay.textContent = filename;
		overlay.addEventListener("click", evt =>
		{
			expand_dir(filename).then(()=>{});
			evt.stopPropagation();
		}, false);
		overlay.classList.add("zoom-in");
	}

	// Show the cloned node
	root.insertBefore(entry, node_before);

	// update perm icon after inserting the entry node to the document
	// otherwise update_perm_icon() can't find it
	update_permission_icons(filename);

	// select the image if the user clicks on the perm icon
    entry.querySelector(".perm-overlay-box").addEventListener("click", ev=>
	{
		toggle_select_blob(filename);
		ev.stopPropagation();
	}, false);

	// hide the message "there are no image here yet" because we just added one
	document.getElementById("message").style.display = "none";
}

function show_dialog(dialog_id)
{
	document.getElementById(dialog_id).style.display = "flex";
	document.body.style.overflow = "hidden";
}

function show_move_dialog()
{
	update_album_list();

	// fill the album with the last album the user just moved to
	if (last_moved_album)
		document.getElementById("dest-album-name").value = last_moved_album;

	show_dialog("move-blob-dialog");
}

function main()
{
	console.assert(window.dir);
	list_dir();

	window.onpopstate = ev =>
	{
		console.log("poping to = ", document.location.pathname);
		fetch_dir(document.location.pathname).then(()=>{});
	};

	// add all event handlers
	document.body.addEventListener("paste", paste);
	document.getElementById("nav-path-segment-prototype").addEventListener("click", ev =>
	{
		fetch_dir("/").then(()=>{});
	}, false);
	document.getElementById("logout-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		let req = new XMLHttpRequest();
		req.overrideMimeType("text/plain");
		req.open("GET", "/?auth=logout");
		req.onload = () =>
		{
			if (req.status === 200)
			{
				console.log("logout successful");
				delete dir.username;
				fetch_dir(document.location.pathname).then(()=>{});
			}
			else
			{
				console.log("cannot logout", req.status, req.responseText);
			}
		};
		req.send();
	});
	document.getElementById("file-upload").addEventListener("change", function()
	{
		for (let i = 0; i < this.files.length; i++)
			upload_file(this.files[i]);
	});
	document.addEventListener("dragenter", ev=>{ev.preventDefault();}, false);
	document.addEventListener("dragover",  ev=>{ev.preventDefault();}, false);
	document.addEventListener("drop", event =>
	{
		event.preventDefault();

		let files = event.target.files || event.dataTransfer.files;
		console.log("dropped", files.length, "files");

		// process all File objects
		// for (let i = 0, file; file == files[i]; i++)
		// 	upload_file(file);
	}, false);
	document.getElementById("login-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();

		// make sure the submit button is not disabled by last login
		document.querySelector("#login-dialog button").disabled = false;
		document.getElementById("login-username").disabled = false;
		document.getElementById("login-password").disabled = false;
		document.getElementById("login-message").textContent = "Please login:";
		show_dialog("login-dialog");
	}, false);
	document.getElementById("create-album-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		show_dialog("create-album-dialog");
	}, false);
	document.getElementById("move-image-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		show_move_dialog();
	}, false);
	document.getElementById("make-image-private-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		get_selected_blobs().forEach(blob => {set_blob_permission(blob, "private");});
		default_perm = "private";
		select_all_blobs(false);
	}, false);
	document.getElementById("make-image-shared-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		get_selected_blobs().forEach(blob => {set_blob_permission(blob, "shared");});
		default_perm = "shared";
		select_all_blobs(false);
	}, false);
	document.getElementById("make-image-public-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();
		get_selected_blobs().forEach(blob => {set_blob_permission(blob, "public");});
		default_perm = "public";
		select_all_blobs(false);
	}, false);
	document.getElementById("share-nav").addEventListener("click", ev =>
	{
		ev.preventDefault();

		post_blob("", "share=create").then((xhr) =>
		{
			console.log("share link", xhr.getResponseHeader("location"));
			show_dialog("share-link-dialog");
			document.getElementById("share-link-textbox").textContent =
				document.location.protocol + "//" + document.location.host + xhr.getResponseHeader("location");
		});
	}, false);
	document.getElementById("copy-shared-link").addEventListener("click", ev =>
	{
		console.log("copying");
		ev.preventDefault();
		let range = document.createRange();
		range.selectNodeContents(document.getElementById("share-link-textbox"));
		window.getSelection().addRange(range);
		document.execCommand("copy");
	}, false);
//	document.getElementById("delete-image").addEventListener("click", delete_image, false);
	document.getElementById("make-cover").addEventListener("click", ev =>
	{
		ev.preventDefault();
		let cover = document.getElementById("light-box").dataset.blob_id;
		post_blob("", "cover=" + cover).then(() =>
		{
			console.log("set cover to", cover);
			if (dir.meta == null)
				dir.meta = {cover: cover};
			else
				dir.meta.cover = cover;
			document.getElementById("make-cover").classList.add("highlighted");
		});
	}, false);
	document.getElementById("make-image-private").addEventListener("click", ev =>
	{
		ev.preventDefault();
		set_blob_permission(document.getElementById("light-box").dataset.blob_id, "private");
	}, false);
	document.getElementById("make-image-shared").addEventListener("click", ev =>
	{
		ev.preventDefault();
		set_blob_permission(document.getElementById("light-box").dataset.blob_id, "shared");
	}, false);
	document.getElementById("make-image-public").addEventListener("click", ev =>
	{
		ev.preventDefault();
		set_blob_permission(document.getElementById("light-box").dataset.blob_id, "public");
	}, false);
	document.getElementById("move-image").addEventListener("click", ev =>
	{
		ev.preventDefault();

		// save blob ID before closing light-box
		document.getElementById("move-blob-dialog").dataset.blob_id =
			document.getElementById("light-box").dataset.blob_id;
		close_overlays(false);

		show_move_dialog();
	}, false);
	document.getElementById("nav-to-parent").addEventListener("click", ev =>
	{
		let paths = dir.logical_path.split('/');
		if (paths.pop() === "")
			paths.pop();
		let next = paths.join('/');
		if (next === "")
			next = '/';
		fetch_dir(next);
	});
	document.querySelector("#create-album-dialog form").addEventListener("submit", ev =>
	{
		ev.preventDefault();
		const name = document.getElementById("new-album-name").value;

		let intent = Intent.current();
		intent.coll = name;
		intent.filename = "";
		ajax_navigate(intent);
	}, false);
	document.querySelector("#move-blob-dialog form").addEventListener("submit", ev =>
	{
		ev.preventDefault();

		if (document.getElementById("move-blob-dialog").dataset.blob_id != null)
		{
			move_image(
				get_blob_intent(document.getElementById("move-blob-dialog").dataset.blob_id),
				document.getElementById("dest-album-name").value
			);
		}
		else
		{
			let selected = get_selected_blobs();
			selected.forEach(blob_id =>
			{
				move_image(
					get_blob_intent(blob_id),
					document.getElementById("dest-album-name").value
				);
			});
		}
	}, false);
	document.querySelector("#login-dialog form").addEventListener("submit", async ev =>
	{
		ev.preventDefault();

		const promise = fetch(dir.logical_path + "?auth=login", {
			method: "POST",
			headers: {
				"Content-type": "application/x-www-form-urlencoded"
			},
			body:
				"username=" + document.getElementById("login-username").value +
				"&password=" + document.getElementById("login-password").value
		});

		// disable submit button
		document.querySelector("#login-dialog button").disabled = true;
		document.getElementById("login-username").disabled = true;
		document.getElementById("login-password").disabled = true;
		document.getElementById("login-message").textContent = "Please wait... Logging in..";

		const response = await promise;
		console.log("Login response", response.status);
		if (response.ok)
		{
			dir.username = document.getElementById("login-username").value;
			console.log("user", dir.username, "login successful");
			console.log(window.location.pathname);
			document.getElementById("login-password").value = "";

			fetch_dir(dir.logical_path).then(()=>{});
		}
		else
		{
			console.log("Login incorrect", response.status, response.statusText);
			document.getElementById("login-message").textContent = "Login incorrect! Please try again:";
			document.querySelector("#login-dialog button").disabled = false;
			document.getElementById("login-username").disabled = false;
			document.getElementById("login-password").disabled = false;
		}
	}, false);
	window.addEventListener("scroll", ()=>
	{
		const endOfPage = window.innerHeight + Math.round(window.scrollY) >= document.body.offsetHeight;
		if (endOfPage)
			show_more_thumbnails();
	});

	let close_btns = document.querySelectorAll(".close-overlays");
	for (let i = 0; i < close_btns.length; i++)
		close_btns[i].addEventListener(
			"click", ()=>
			{
				if (dir.filename != null)
					delete dir.filename;

				// navigate to collection list view
				history.pushState(null, "", dir.logical_path);

				close_overlays(true);
			}, false
		);
}

function sort_elements(elements)
{
	if (elements === undefined)
		elements = dir.elements;

	// Sort images by their timestamps before showing them
	let files = Object.keys(elements);
	files.sort((a,b) =>
	{
//		return dir.elements[a].modified - dir.elements[b].modified;
		return a.localeCompare(b);
	});
	return files;
}

function show_more_thumbnails()
{
	if (dir.elements)
	{
		const root = document.getElementById("hrb-entry-root");
		let entries = root.querySelectorAll("div.hrb-entry");
		const thumbnails_count = entries.length;

		// Remove all existing entries
		for (let i = 0; i < entries.length; i++)
		{
			if (entries[i] !== document.getElementById("hrb-entry-prototype"))
				entries[i].remove();
		}
		sort_elements().slice(0, thumbnails_count+images_per_page).forEach(e=>{add_element(e);});
	}
}

function display_logical_path()
{
	const root = document.getElementById("nav-path-segment");
	const proto = document.getElementById("nav-path-segment-prototype");
	let nodes = root.querySelectorAll("span.nav-path-segment");

	for (let i = 0; i < nodes.length; i++)
	{
		if (nodes[i] !== proto)
			nodes[i].remove();
	}

	let running = "/";
	dir.logical_path.split('/').forEach((segment)=>
	{
		if (segment !== '')
		{
			let segment_node = proto.cloneNode(true);
			segment_node.removeAttribute("id");
			segment_node.textContent = segment;

			running += segment;
			running += "/";

			const up_til_now = running.slice();
			segment_node.addEventListener("click", evt=>
			{
				console.log("clicked", up_til_now);
				dir.logical_path = up_til_now;
				console.log("clicked", dir.logical_path);
				fetch_dir(up_til_now);
			});

			root.appendChild(segment_node);
		}
	});
}

function list_dir()
{
	// clear all hrb-entries and load the new ones from window.dir
	const root = document.getElementById("hrb-entry-root");
	let entries = root.querySelectorAll("div.hrb-entry");
	for (let i = 0; i < entries.length; i++)
	{
		if (entries[i] !== document.getElementById("hrb-entry-prototype"))
			entries[i].remove();
	}
	document.getElementById("message").textContent = "There are no image here yet.";
	document.getElementById("message").style.display = "block";

	if (dir.elements)
	{
		// Sort images by their timestamps before showing them
		sort_elements().slice(0, images_per_page).forEach(e=>{add_element(e);});
	}
	display_logical_path();

	// User logged in
	if (dir.username && dir.username.length > 0)
	{
		// Display the user's name
		document.getElementById("show-username").textContent = dir.username;

		let to_show = document.querySelectorAll(".topnav .show-only-after-logged-in");
		for (let i = 0; i < to_show.length; i++)
			to_show[i].style.display = "block";
		let to_hide = document.querySelectorAll(".topnav .show-only-before-logged-in");
		for (let i = 0; i < to_hide.length; i++)
			to_hide[i].style.display = "none";
	}
	else
	{
		let to_hide = document.querySelectorAll(".topnav .show-only-after-logged-in");
		for (let i = 0; i < to_hide.length; i++)
			to_hide[i].style.display = "none";
		let to_show = document.querySelectorAll(".topnav .show-only-before-logged-in");
		for (let i = 0; i < to_show.length; i++)
			to_show[i].style.display = "block";
	}
	if (dir.error_message)
	{
		document.getElementById("message").style.display = "block";
		document.getElementById("message").textContent = dir.error_message;
	}

	if (!dir.filename && !dir.login_message)
	{
		close_overlays(false);
	}
	else if (dir.filename != null)
	{
		show_image(dir.filename, false);
	}

	update_selected_blobs();
	window.scrollTo(0,0);
}

async function expand_dir(subdir)
{
	const response = await fetch(dir.logical_path + subdir + "?list");
	const json = await response.json();

	// Replace directory node with its contents
	sort_elements(json.elements).forEach(filename =>
	{
		const subdir_filename = subdir + "/" + filename;
		console.log("adding item", subdir_filename, json.elements[filename]);
		dir.elements[subdir_filename] =  json.elements[filename];
	});
	show_more_thumbnails();

	// Remove old directory node
	let node = document.getElementById("hrb-entry-" + subdir);
	node.remove();
	delete dir.elements[subdir];
}

async function fetch_dir(target_logical_path)
{
	const response = await fetch( target_logical_path + "?list");
	if (response.ok)
	{
		window.dir = await response.json();
		history.pushState(null, null, dir.logical_path);
		list_dir();
	}
}

function close_overlays(push_state)
{
	document.getElementById("login-dialog").style.display = "none";
	document.getElementById("create-album-dialog").style.display = "none";
	document.getElementById("share-link-dialog").style.display = "none";
	document.getElementById("light-box").style.display = "none";
	document.getElementById("move-blob-dialog").style.display = "none";
	delete document.getElementById("light-box").dataset.filename;

	document.getElementById("light-box-image").src = "/.lib/loading.svg";
	document.querySelector("#light-box .light-box-avif").srcset = "/.lib/loading.svg";
	document.querySelector("#light-box .light-box-webp").srcset = "/.lib/loading.svg";

	// restore the scrollbar
	document.body.style.overflow = "auto";
}

function show_image(filename, push_state)
{
	const image_url = dir.logical_path + filename;
	if (push_state)
		history.pushState(null, filename, image_url + "?view");

	// hide edit buttons if we don't know their collection
	// because we don't know the URL to POST
	const hide_btns = [
		"make-image-private", "make-image-shared", "make-image-public", "move-image",
		"delete-image", "make-cover"
	];
	hide_btns.forEach(btn => {
		document.getElementById(btn).style.display = (dir.collection == null ? "none" : "inline");
	});

	// update "make-cover" button
	if (dir.meta && dir.meta.cover === filename)
		document.getElementById("make-cover").classList.add("highlighted");
	else
		document.getElementById("make-cover").classList.remove("highlighted");

	// show metadata
	get_image_meta(filename).then(meta =>
	{
		document.getElementById("meta-filename").textContent = filename;
		document.getElementById("meta-mime").textContent = meta.mime;

		let date_time = new Date((meta.hasOwnProperty('created') ? meta.created : meta.modified)/1000000);
		document.getElementById("meta-datetime").textContent = date_time.toLocaleString();
	});

	show_lightbox(image_url, filename);
	document.getElementById("light-box").dataset.filename = filename;
	update_permission_buttons(filename);
	select_all_blobs(false);
}

function update_permission_icons(blob_id)
{
	// update perm icon in hrb-entry
	const entry = document.getElementById("hrb-entry-" + blob_id);
	const element = dir.elements[blob_id];

	if (element != null && entry != null)
	{
		const perm_icon = entry.querySelector("i.perm-overlay-box.material-icons");
		if (element.selected != null && element.selected === true)
			perm_icon.textContent = "done";
		else if (element.perm === "public")
			perm_icon.textContent = "public";
		else if (element.perm === "shared")
			perm_icon.textContent = "people";
		else// if (element.perm === "private")
			perm_icon.textContent = "person";
	}
}

function update_permission_buttons(filename)
{
	const element = dir.elements[filename];

	// update permission buttons in lightbox
	if (element.perm === "private")
	{
		document.getElementById("make-image-private").classList.add("highlighted");
		document.getElementById("make-image-shared") .classList.remove("highlighted");
		document.getElementById("make-image-public") .classList.remove("highlighted");
	}
	else if (element.perm === "shared")
	{
		document.getElementById("make-image-private").classList.remove("highlighted");
		document.getElementById("make-image-shared") .classList.add("highlighted");
		document.getElementById("make-image-public") .classList.remove("highlighted");
	}
	else if (element.perm === "public")
	{
		document.getElementById("make-image-private").classList.remove("highlighted");
		document.getElementById("make-image-shared") .classList.remove("highlighted");
		document.getElementById("make-image-public") .classList.add("highlighted");
	}
}

function show_lightbox(url, filename)
{
	let image = document.getElementById("light-box-image");
	image.addEventListener("error", ev =>
	{
		// Reload image when error
		console.log("light-box-image error", ev);
		image.src = "/.lib/loading.svg";
		image.src = url + "?content=.jpeg";
	}, false);
	image.src = url + "?content=.jpeg";

	document.querySelector("#light-box .light-box-avif").srcset = url + "?content=.avif";
	document.querySelector("#light-box .light-box-webp").srcset = url + "?content=.webp";

	show_dialog("light-box");
}

// Called by lightbox's next/previous buttons
function next_image(offset)
{
	const sign = (offset > 0 ? 1 : -1);

	const current = document.getElementById("light-box").dataset.filename;
	if (current)
	{
		const blobs = sort_elements();
		const index = blobs.indexOf(current);
		for (let i = 0 ; i < blobs.length; ++i)
		{
			const next_index = (index + offset + sign*i + blobs.length) % blobs.length;
			const next = blobs[next_index];
			if (is_image(dir.elements[next].mime))
			{
				show_image(next, true);
				break;
			}
		}
	}
}

function paste(e)
{
	for (let i = 0 ; i < e.clipboardData.items.length ; i++)
	{
		let item = e.clipboardData.items[i];
		console.log("Item: ", item.type);

		if (is_image(item.type))
			queue_action({upload: item.getAsFile()});
    }
}

function get_image_meta(filename)
{
	return new Promise((resolve, reject) =>
	{
		let req = new XMLHttpRequest();
		req.open("GET", dir.logical_path + filename + "?meta");
		req.responseType = 'json';
		req.onload = () =>
		{
			// extract blob_id from the location
			if (req.status === 200)
			{
				let meta = req.response;

				// inject meta from dir.elements into the returned meta JSON
				const element = dir.elements[filename];
				if (element)
					meta = Object.assign(meta, element);

				resolve(meta);
			}
			else
			{
				reject(req.status);
			}
		};
		req.onerror = () =>
		{
			reject(req.status);
		};
		req.send();
	});
}

function hide_toast(message)
{
	if (message != null)
	{
		show_toast(message);
		toast_timer = setTimeout(() =>
		{
			document.getElementById("toast").style.display = "none";
			toast_timer = null;
		}, 10000);
	}
	else
		document.getElementById("toast").style.display = "none";
}

function show_toast(message)
{
	if (toast_timer != null)
	{
		clearTimeout(toast_timer);
		toast_timer = null;
	}
	document.getElementById("toast").textContent	= message;
	document.getElementById("toast").style.display	= "block";
}
