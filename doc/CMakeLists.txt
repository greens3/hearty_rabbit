find_package(Doxygen)

#Look for an executable called sphinx-build
find_program(
	SPHINX_EXECUTABLE
    NAMES sphinx-build
    DOC "Path to sphinx-build executable"
)

if (DOXYGEN_FOUND)
	set(DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/doxygen)
	set(DOXYGEN_INDEX_FILE ${DOXYGEN_OUTPUT_DIR}/xml/index.xml)
	
	configure_file(Doxyfile.in Doxyfile @ONLY)
	add_custom_command(OUTPUT ${DOXYGEN_INDEX_FILE}
		COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
		WORKING_DIRECTORY ${DOXYGEN_OUTPUT_DIR}
		DEPENDS ${LIB_SRC} Doxyfile.in doxygenextra.css
	)
	add_custom_target(DoxygenXML ALL DEPENDS ${DOXYGEN_INDEX_FILE})
	set_target_properties(DoxygenXML PROPERTIES EXCLUDE_FROM_ALL TRUE)

	if (SPHINX_EXECUTABLE)
		message(STATUS "Found sphinx: ${SPHINX_EXECUTABLE}")
		
		set(SPHINX_SOURCE ${CMAKE_CURRENT_SOURCE_DIR})
		set(SPHINX_BUILD ${CMAKE_CURRENT_BINARY_DIR}/sphinx)
		set(SPHINX_INDEX_FILE ${SPHINX_BUILD}/index.html)
		
		add_custom_target(
			Sphinx ALL
            COMMAND
			${SPHINX_EXECUTABLE} -b html
			-Dbreathe_projects.HeartyRabbit=${DOXYGEN_OUTPUT_DIR}/xml
			${SPHINX_SOURCE} ${SPHINX_BUILD}
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Generating documentation with Sphinx"
		)
	endif()
else()
	message(STATUS "No Doxygen found. Documentation won't be built")
endif()
