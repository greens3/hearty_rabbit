.. Hearty Rabbit documentation master file, created by
   sphinx-quickstart on Sun Oct 15 14:09:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hearty Rabbit's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Hearty Rabbit is a high-performance web-base image album implemented by asynchronous C++. It uses the Boost Beast library
to implement an asynchronous web server, and maintain a cache in Redis.

Design Principle
----------------

Data transparency:
   Hearty Rabbit does not store data in any database. All data are stored in image files.
   These files can be access directly. As a result, migration an album from Hearty Rabbit to
   other server is very easy: just copy the directory that contains the image files. Backing
   up and restoring data is also very easy. There is no need for a special backup application.
   Any file-base backup tool like ``tar`` or ``rsync`` would work.

Fully Asynchronous:
   Hearty Rabbit uses Boost ASIO as its framework. All operations are fully asynchronous.
   Hearty Rabbit can use all CPU cores available in the system to provide highest possible
   throughput supported by underlying the storage devices.

Low Resources Footprint:
   Hearty Rabbit does not require a lot of memory to run. It runs very well in a
   Raspberry Pi 4. If you have a separate box to run the Redis cache, there is no need
   to use a powerful machine like the Pi 4. It is designed to save hosting cost in cloud
   environments.

High Performance:
   On a Raspberry Pi 4, Hearty Rabbit can support about 1000 request per second: ::

      $ wrk -t12 -c400 -d30s https://<server>:4433/
      Running 30s test @ https://<server>:4433/
        12 threads and 400 connections
        Thread Stats   Avg      Stdev     Max   +/- Stdev
          Latency   230.82ms   58.92ms 732.63ms   85.57%
          Req/Sec   134.81     43.03   393.00     74.63%
        45661 requests in 30.10s, 439.03MB read
      Requests/sec:   1517.09
      Transfer/sec:     14.59MB

Architecture
============

Hearty Rabbit uses the file system as the data storage backend. On top of the storage backend, Hearty
Rabbit maintains two tiers of caching: rendition cache and Redis. The rendition cache stores down-sampled
copies of the images, for example, thumbnails. The Redis cache stores directory contents, as well as image
file meta-data.


API Documentation
=================

.. doxygennamespace:: hrb
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
